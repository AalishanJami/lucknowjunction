<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MUserBookings extends CI_Model
{
    public function fetch($id)
    {
        $query = $this->db->select('*')->from("eventbookinglist")->where("event_id", $id)->join('user', 'eventbookinglist.user_id=user.user_id','inner')->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('eventbookinglist',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('eventbookinglist',$data);
        return true;
    }

}