
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muserreviews extends CI_Model
{

    public function fetch()
    {
        $query = $this->db->select('*')->from("eventreviews")->join('events','eventreviews.event_id=events.event_id','inner')->get();

        return $query->result();
    }

    public function fetch_review($id)
    {
        $query = $this->db->select('*')->from("eventreviews")->where('review_id', $id)->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('review_id',$id);
        $this->db->update('eventreviews',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('review_id',$id);
        $this->db->update('eventreviews',$data);
        return true;
    }

    public function delete($id){
        $this->db->from('eventreviews');
        $this->db->where('review_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('review_id',$id);
        $this->db->update('review',$data);
        return true;
    }



    

}