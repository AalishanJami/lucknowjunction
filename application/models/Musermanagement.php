<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musermanagement extends CI_Model
{

    function insert($data)
    {
        $this->db->insert('user',$data);
        return true;
    }

    public function fetch()
    {
        $query = $this->db->select('*')->from("user")->get();

        return $query->result();
    }

    public function fetch_user($id)
    {
        $query = $this->db->select('*')->from("user")->where('user_id', $id)->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('user_id',$id);
        $this->db->update('user',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('user_id',$id);
        $this->db->update('user',$data);
        return true;
    }

    public function delete($id){
        $this->db->from('user');
        $this->db->where('user_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('user_id',$id);
        $this->db->update('user',$data);
        return true;
    }



    

}