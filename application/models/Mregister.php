<?php
/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/16/2019
 * Time: 3:48 PM
 */

class Mregister extends CI_Model
{
    function insert($data)
    {
        $this->db->insert('user',$data);
        return true;
    }

    function insertFb($data)
    {
    	$this->db->insert('user',$data);
    	$insert_id = $this->db->insert_id();
        return $insert_id;
    }
}