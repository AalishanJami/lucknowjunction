<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mchangepass extends CI_Model
{

    public function fetch($old)
    {
        $query = $this->db->select('*')->from("admin")->where('password', $old)->get();

        return $query->num_rows();
        
    }

    public function change($data, $username)
    {
        $this->db->where('username',$username);
        $this->db->update('admin',$data);
        
    }

}