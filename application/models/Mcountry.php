<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcountry extends CI_Model
{

    function insert($data)
    {
        $this->db->insert('country',$data);
        return true;
    }

    public function fetch()
    {
        $query = $this->db->select('*')->from("country")->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('country_id',$id);
        $this->db->update('country',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('country_id',$id);
        $this->db->update('country',$data);
        return true;
    }

    public function delete($id){
        $this->db->from('country');
        $this->db->where('country_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('country_id',$id);
        $this->db->update('country',$data);
        return true;
    }



    

}