<?php
/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/16/2019
 * Time: 4:09 PM
 */

class Mlogin extends CI_Model
{
    function emailExists($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user_email='$email'");
        $query = $this->db->get();
        return $query->result();
    }

    function adminAuth($email){
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where("username='$email'");
        $query = $this->db->get();
        return $query->result();
    }

    function addRecoveryToken($data){
       $this->db->insert('forgotpassword',$data);
        return true;
    }


    function validaterecoverpassword($email){
        $query = $this->db->select('*')->from('forgotpassword')->where("email",$email)->get();
        return $query->num_rows();
    }

    function validaterecoverpasswordtoken($token){
        $query = $this->db->select('*')->from('forgotpassword')->where("token",$token)->get();
        return $query->num_rows();
    }


    public function change($email, $data)
    {
        $this->db->where('user_email',$email);
        $this->db->update('user',$data);
        return true;
    }

}