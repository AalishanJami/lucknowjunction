<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meventreviews extends CI_Model
{

    public function fetch($id)
    {
        $query = $this->db->select('*')->from("eventreviews")->where("event_id", $id)->join('user', 'eventreviews.user_id=user.user_id','inner')->get();

        return $query->result();
    }

    public function fetch_user($id)
    {
        $query = $this->db->select('*')->from("feedback")->where('feedback_id', $id)->join('user', 'feedback.user_id=user.user_id','inner')->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('feedback',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('feedback',$data);
        return true;
    }

    public function delete($id){
        $this->db->from('feedback');
        $this->db->where('feedback_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('feedback',$data);
        return true;
    }

    
    function insert($data)
    {
        $this->db->insert('feedback_replies',$data);
        return true;
    }




    

}
