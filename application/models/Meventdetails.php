<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meventdetails extends CI_Model
{

    function getDetails($id){
    	$query = $this->db->select('*')->from("events")->where("event_id",$id)->join('city', 'events.city_id=city.city_id','inner')->join('country', 'events.country_id=country.country_id','inner')->join('state', 'events.state_id=state.state_id','inner')->join('category', 'events.category_id=category.category_id','inner')->get();

        return $query->result();
    }

    function getPrices($id){
		$query = $this->db->select('*')->from("eventspriceclasses")->where("event_id",$id)->get();

        return $query->result();
    }

    function getReviews($id){
    	$query = $this->db->select('*')->from("eventreviews")->where("event_id",$id)->get();

        return $query->num_rows();
    }

    function getRatings($id){
    	$query = $this->db->select('*')->from("eventratings")->where("event_id",$id)->get();

        return $query->num_rows();
    }
    function getRatingsUser($id, $userid){
        $array = array('event_id' => $id, 'user_id' => $userid);
    	$query = $this->db->select('*')->from("eventratings")->where($array)->where("user_id", $userid)->get();

        return $query->result();
    }

    function getBookings($id){
    	$query = $this->db->select('*')->from("eventbookinglist")->where("event_id",$id)->get();

        return $query->num_rows();
    }

    function saveRating($data){
        $this->db->insert('eventratings',$data);
        return true;
    }
    function saveReview($data){
        $this->db->insert('eventreviews',$data);
        return true;
    }
    function savePayment($data){
        $this->db->insert('eventbookinglist',$data);
        return true;
    }

    function getAdsSq(){
    $query =  $this->db->order_by('rand()')->limit(1)->from("ads")->where("ad_type", '1')->get();
    return $query->result();
    }

    function getAdsRec(){
   $query =  $this->db->order_by('rand()')->limit(1)->from("ads")->where("ad_type", '2')->get();
    return $query->result();
    }
}
