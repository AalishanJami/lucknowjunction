<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categoriesmodel extends CI_Model 
{
	function insert($data)
    {
        $this->db->insert('category',$data);
        return true;
    }

	 function fetch()
	 {
	 $query=$this->db->select('*')->from('category')->get();
	 return $query->result();
	 }
	  public function active($id, $data)
    {
        $this->db->where('category_id',$id);
        $this->db->update('category',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('category_id',$id);
        $this->db->update('category',$data);
        return true;
    }


    public function delete($id){
        $this->db->from('category');
        $this->db->where('category_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('category_id',$id);
        $this->db->update('category',$data);
        return true;
    }

 //    function displaycategById($id)
	// {
	//     $this->db->select('*');
 //        $this->db->where('category_id', $id);
 //        $q = $this->db->get('category');
 //        $result = $q->result_array();
 //        return $result;
	// }

	// function updatecategories($categoryname,$categorystatus,$id)
	// {
	//    if($categoryname !='' && $categorystatus !=''  ){

 //            // Update
 //            $value=array('category_name'=>$categoryname,'category_status'=>$categorystatus);
 //            $this->db->where('category_id',$id);
 //            $this->db->update('category',$value);
 //            echo "ok";
	// }
}
//}