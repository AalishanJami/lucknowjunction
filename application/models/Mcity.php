<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcity extends CI_Model
{

    function insert($data)
    {
        $this->db->insert('city',$data);
        return true;
    }

    public function fetch()
    {
        $query = $this->db->select('*')->from("city")->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('city_id',$id);
        $this->db->update('city',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('city_id',$id);
        $this->db->update('city',$data);
        return true;
    }

    public function delete($id){
        $this->db->from('city');
        $this->db->where('city_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('city_id',$id);
        $this->db->update('city',$data);
        return true;
    }



    

}