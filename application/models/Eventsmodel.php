<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Eventsmodel extends CI_Model {

  
  // Get all events list
   function displayrecords()
     {
     $query=$this->db->select('*')->from('events')->join('city', 'events.city_id=city.city_id','inner')->join('country', 'events.country_id=country.country_id','inner')->join('state', 'events.state_id=state.state_id','inner')->join('category', 'events.category_id=category.category_id','inner')->get();

     return $query->result();
     }
  
  public function fetch_user($id)
    {
        $query = $this->db->select('*')->from("events")->where('event_id', $id)->join('city', 'events.city_id=city.city_id','inner')->join('country', 'events.country_id=country.country_id','inner')->join('state', 'events.state_id=state.state_id','inner')->join('category', 'events.category_id=category.category_id','inner')->get();

        return $query->result();
    }



     public function active($id, $data)
    {
        $this->db->where('event_id',$id);
        $this->db->update('events',$data);
        return true;
    }

     public function sold($id, $data)
    {
        $this->db->where('event_id',$id);
        $this->db->update('events',$data);
        return true;
    }
     public function unsold($id, $data)
    {
        $this->db->where('event_id',$id);
        $this->db->update('events',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('event_id',$id);
        $this->db->update('events',$data);
        return true;
    }

 
    public function delete($id){
        $this->db->from('events');
        $this->db->where('event_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('event_id',$id);
        $this->db->update('events',$data);
        return true;
    }

    
     
// function updateEvent($postData,$id){

//         $name = trim($postData['txt_name']);
//         $email = trim($postData['txt_email']);
//         if($name !='' && $email !=''  ){

//             // Update
//             $value=array('name'=>$name,'email'=>$email);
//             $this->db->where('id',$id);
//             $this->db->update('users',$value)){

//         }
    }
