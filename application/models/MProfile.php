<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MProfile extends CI_Model
{

 public function fetch($id)
    {
   $this->db->select('*');
	 	 $this->db->from('user');
	 	// $this->db->join('events as f','e.user_id=f.user_id');
	 	  $this->db->where('user_id', $id );
	 	  $query = $this->db->get();
	
	      return $query->result();
	 
}
function update($id,$data)
    {

         $this->db->where('user_id', $id);
        $this->db->update('user',$data);

        return true;
    }
}