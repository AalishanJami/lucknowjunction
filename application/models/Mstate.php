<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstate extends CI_Model
{

    function insert($data)
    {
        $this->db->insert('state',$data);
        return true;
    }

    public function fetch()
    {
        $query = $this->db->select('*')->from("state")->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('state_id',$id);
        $this->db->update('state',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('state_id',$id);
        $this->db->update('state',$data);
        return true;
    }

    public function delete($id){
        $this->db->from('state');
        $this->db->where('state_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('state_id',$id);
        $this->db->update('state',$data);
        return true;
    }



    

}