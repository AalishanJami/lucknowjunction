<?php
/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/16/2019
 * Time: 3:48 PM
 */

class Mbecome extends CI_Model
{
    function insert($data)
    {
        $this->db->insert('events',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function insertPrices($data)
    {
        $this->db->insert('eventspriceclasses',$data);
        return true;
    }


    function getCountry(){

        $query = $this->db->select('*')->from("country")->where('status', '1')->get();

        return $query->result();
    }

    function getCity(){
        
        $query = $this->db->select('*')->from("city")->where('city_status', '1')->get();

        return $query->result();
    }

    function getState(){
        
        $query = $this->db->select('*')->from("state")->where('state_status', '1')->get();

        return $query->result();
    }

    function getCategory(){
        
        $query = $this->db->select('*')->from("category")->where('category_status', '1')->get();

        return $query->result();
    }

    function getEvent($id){
        
        $query = $this->db->select('*')->from("events")->where('event_id', $id)->get();

        return $query->result();
    }

    function udpatePrice($id, $data){
        $this->db->where('event_id',$id);
        $this->db->update('events',$data);
        return true;
    }
}