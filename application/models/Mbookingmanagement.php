<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mbookingmanagement extends CI_Model
{

    public function fetch()
    {
        $query = $this->db->select('*')->from("eventbookinglist")->join('events','eventbookinglist.event_bookinglistId=events.event_id','inner')->join('user', 'eventbookinglist.user_id=user.user_id','inner')->get();

        return $query->result();
        
    }

    public function fetch_review($id)
    {
        $query = $this->db->select('*')->from("eventbookinglist")->where('event_bookinglistId', $id)->get();

        return $query->result();
    }

    public function active($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('eventbookinglist',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('eventbookinglist',$data);
        return true;
    }

    public function delete($id){
        $this->db->from('eventbookinglist');
        $this->db->where('event_bookinglistId',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('event_bookinglistId',$id);
        $this->db->update('eventbookinglist',$data);
        return true;
    }



    

}