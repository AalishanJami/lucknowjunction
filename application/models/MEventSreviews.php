<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MeventSreviews extends CI_Model
{
    public function fetch($id)
    {
        $query = $this->db->select('*')->from("eventreviews")->where("event_id", $id)->join('user', 'eventreviews.user_id=user.user_id','inner')->get();

        return $query->result();
    }

}