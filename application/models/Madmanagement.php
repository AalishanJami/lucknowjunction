<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Madmanagement extends CI_Model 
{

	 function fetch()
	 {
	 $query=$this->db->select('*')->from('ads')->get();
	 return $query->result();
	 }
	  public function active($id, $data)
    {
        $this->db->where('ad_id',$id);
        $this->db->update('ads',$data);
        return true;
    }

    public function deactive($id, $data)
    {
        $this->db->where('ad_id',$id);
        $this->db->update('ads',$data);
        return true;
    }


    public function delete($id){
        $this->db->from('ads');
        $this->db->where('ad_id',$id);
        $this->db->delete();
        return true;
    }

    public function update($id, $data)
    {
        $this->db->where('ad_id',$id);
        $this->db->update('ads',$data);
        return true;
    }

     function insert($data)
    {
        $this->db->insert('ads',$data);
        return true;
    }

}
