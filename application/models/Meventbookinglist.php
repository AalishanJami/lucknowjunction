<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meventbookinglist extends CI_Model
{

    function getBookings($id)
    {
        $query = $this->db->select('*')->from("eventbookinglist")->join('events', 'eventbookinglist.event_id=events.event_id','inner')->where("user_id",$id)->get();

        return $query->result();
    }

}