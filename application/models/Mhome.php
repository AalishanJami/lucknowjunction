<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhome extends CI_Model
{

    public function search($cat, $con)
    {

    $this->db->select('*');
    $this->db->from('events');

    // $this->db->join('category', 'events.category_name = category.category_name');
    $this->db->like('category_name', $cat);

    $query = $this->db->get();
    //$result = $query->result_array();

    return $result;

    }

    function getCityandCat($city, $cat){
         $query = $this->db->select('*')->from("events")->where(array('category_id' => $cat, 'city_id' => $city))->limit(9,0)->get();

        return $query->result();
    }

     function getEventbyCategory($category){
        $query = $this->db->select('*')->from("events")->where('category_id', $category)->limit(9,0)->get();

        return $query->result();
    }

    function getEventbyNewYear(){
        $query = $this->db->select('*')->from("events")->where('category_id', '22')->limit(9,0)->get();

        return $query->result();
    }

    function getEventbyFD(){
        $query = $this->db->select('*')->from("events")->where('category_id', '22')->limit(9,0)->get();

        return $query->result();
    }

    function getAllEventsByCity($city){
        $query = $this->db->select('*')->from("events")->where('city_id', $city)->limit(9,0)->get();

        return $query->result();
    }


    function checkPricise($city){
        $query = $this->db->select('*')->from("city")->like('city_name', $city)->get();

        return $query->result();
    }

    function getSample($cond, $id){
        $query = $this->db->select('*')->from('events')->where($cond, $id)->get();

        return $query->result();
    }

    function getCategoryName($cond, $id){
        $query = $this->db->select('*')->from('category')->where($cond, $id)->get();

        return $query->result();
    }

function getCityPrecise($city){
        $query = $this->db->select('*')->from("events")->where('city_id', $city)->get();

        return $query->num_rows();
    }

 function getCity(){
        
        $query = $this->db->select('*')->from("city")->where('city_status', '1')->get();

        return $query->result();
    }

    function getCountry(){
        
        $query = $this->db->select('*')->from("events")->where('country_id', '1')->get();

        return $query->result();
    }

    function getNineEvents(){
        
        $query = $this->db->select('*')->from("events")->limit(9,0)->get();

        return $query->result();
    }


    function getCategory(){
        
        $query = $this->db->select('*')->from("category")->where('category_status', '1')->get();

        return $query->result();
    }

    function searchAll($category, $city, $name){
		$query = $this->db->select('*')->from("events")->like(array('category_id' => $category, 'city_id' => $city, 'event_name' => $name))->get();

        return $query->result();
    }
    
    function searchName($name){
    	$query = $this->db->select('*')->from("events")->like('event_name', $name)->get();

        return $query->result();
    }

    function searchCategory($category, $name){
    	$query = $this->db->select('*')->from("events")->like(array('category_id' => $category, 'event_name' => $name))->get();

        return $query->result();
    }

    function searchCity($city, $name){
    	$query = $this->db->select('*')->from("events")->like(array('city_id' => $city, 'event_name' => $name))->get();

        return $query->result();
    }

    function searchCat($city, $category){
    	$query = $this->db->select('*')->from("events")->like(array('city_id' => $city, 'category_id' => $category))->get();

        return $query->result();
    }

}