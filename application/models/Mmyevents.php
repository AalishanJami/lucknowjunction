<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmyevents extends CI_Model
{

    function getEvents($id){
    	$query = $this->db->select('*')->from("events")->where("organizer_id",$id)->join('city', 'events.city_id=city.city_id','inner')->join('country', 'events.country_id=country.country_id','inner')->join('state', 'events.state_id=state.state_id','inner')->join('category', 'events.category_id=category.category_id','inner')->get();

        return $query->result();
    }

    function getPrices($id){
		$query = $this->db->select('*')->from("eventspriceclasses")->where("event_id",$id)->get();

        return $query->result();
    }

    function delete($id){
    	 $this->db->where('event_id', $id);
   $this->db->delete('events'); 
    }

    function update($id, $data){
        $this->db->where('event_id',$id);
        $this->db->update('events',$data);
        return true;
    }

}
