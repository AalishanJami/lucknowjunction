<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdashboard extends CI_Model
{

    function getEvents()
    {
        $query = $this->db->select('*')->from("events")->get();

        return $query->num_rows();
    }
    function getUsers()
    {
        $query = $this->db->select('*')->from("user")->get();

        return $query->num_rows();
    }
    function getReviews()
    {
        $query = $this->db->select('*')->from("eventreviews")->get();

        return $query->num_rows();
    }
    function getCities()
    {
        $query = $this->db->select('*')->from("city")->get();

        return $query->num_rows();
    }

}