<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= --><div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Reply</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Reply</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top reviews gray ">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
                  <!-- Middle Content Area -->
                  <div class="col-md-8 col-xs-12 col-sm-12 news">
                      <!-- Middle Content Box -->
                  <div class="row">
                     <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="posts-masonry">
                        
                           <?php foreach($items as $item){?>
                            
                           <div class="col-md-12 ">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                   
                                 <span><?php echo $item->replies_content;?></span></span>
                                 <br><br>
                                  <span><strong>Date:</strong> &nbsp<?php echo strstr($item->date, ' ', true);?></span>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                  </div>
               
                     
            
                     </div>
                  </div>
                  <!-- Right Sidebar -->
                  
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
