<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=- -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Contact</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Feel Free To Contact Us</h1>
                     </div>
                     <li><a href="<?php echo base_url('index.php/Contact/replies') ?>">View Replies</a></li>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= --> 
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12 no-padding commentForm">
                     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <form method="post"  action="<?php echo base_url('index.php/Contact/insert') ?>">
                           <div class="row">
                              <div class="col-lg-6 col-md-6 col-xs-12">
                                 <div class="form-group">
                                    <input type="text" placeholder="Name" id="name" name="name" class="form-control" required>
                                 </div>
                                 <div class="form-group">
                                    <input type="email" placeholder="Email" id="email" name="email" class="form-control" required>
                                 </div>
                                 <div class="form-group">
                                    <input type="text" placeholder="Subject" id="subject" name="subject" class="form-control" required>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-xs-12">
                                 <div class="form-group">
                                    <textarea cols="12" rows="7" placeholder="Message..." id="message" name="message" class="form-control" required></textarea>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <input  value="Send Message" class="btn btn-theme" type="submit">
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="contactInfo">
                           <div class="singleContadds">
                              <i class="fa fa-map-marker"></i>
                              <p> Gomtinagar, Lucknow - 226010 </p>
                           </div>
                           <div class="singleContadds phone">
                              <i class="fa fa-phone"></i>
                              <p> +91 - 988 920 6089 - <span>Office</span> </p>
                            
                           </div>
                           <div class="singleContadds"> <i class="fa fa-envelope"></i> <a href="mailto:contact@scriptsbundle.com">contact@scriptsbundle.com</a> <a href="mailto:contact@scriptsbundle.com">contact@scriptsbundle.com</a> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Row End --> 
            </div>
            <!-- Main Container End --> 
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->