 <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Profile</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Profile</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top gray">
            <!-- Main Container -->
           

            <div class="container">
                 <?php
                              $i = 0;

                                  foreach($items as $item){ ?>
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
           
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <section class="search-result-item">
                        <a class="image-link" href="">
                           <?php if (!empty($this->session->userdata('site_url'))){?>
<img class="image center-block" alt="" src="<?php echo $item->user_img;?>" width="100" height="100"> 
                              <?php } else { ?>
<img class="image center-block" alt="" src="<?php echo base_url()?>uploads/<?php echo $item->user_img;?>"> 
                                 <?php } ?>
                           
                        </a>
                        <div class="search-result-item-body">
                           <div class="row">
                              <div class="col-md-5 col-sm-12 col-xs-12">
                                 <h4 class="search-result-item-heading"><?php echo $item->user_name;?></h4>
                                 <p class="info">
                                    <span><a><i class="fa fa-user "></i>View Profile </a></span>
                                    <!-- <span><a href="javascript:void(0)"><i class="fa fa-edit"></i>Edit Profile </a></span> -->
                                 </p>
                               
                              </div>
                              <!-- <div class="col-md-7 col-sm-12 col-xs-12">
                                 <div class="row ad-history">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                       <div class="user-stats">
                                          <h2>374</h2>
                                          <small>Ad Sold</small>
                                       </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                       <div class="user-stats">
                                          <h2>980</h2>
                                          <small>Total Listings</small>
                                       </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                       <div class="user-stats">
                                          <h2>413</h2>
                                          <small>Favourites Ads</small>
                                       </div>
                                    </div>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </section>
                     <div class="dashboard-menu-container">
                        <ul>
                           <li class="active">
                              <a href="profile.html">
                                 <div class="menu-name"> Profile </div>
                              </a>
                           </li>
                           <li>
                              <a href="<?php echo base_url('index.php/Login/logout') ?>">
                                 <div class="menu-name">Logout</div>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                     
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
               <div class="row margin-top-40">
                  <!-- Middle Content Area -->
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <!-- Row -->
                       
                     <div class="profile-section margin-bottom-20">
                        <div class="profile-tabs">
                           <ul class="nav nav-justified nav-tabs">
                              <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                              <li><a href="#edit" data-toggle="tab">Edit Profile</a></li>
                             
                           </ul>
                           <div class="tab-content">
                              <div class="profile-edit tab-pane fade in active" id="profile">
                                 <h2 class="heading-md">Manage your Name, ID and Email Addresses.</h2>
                                 <p>Below are the name and email addresses for your account.</p>
                                 <dl class="dl-horizontal">
                                    <dt><strong>Your name </strong></dt>
                                    <dd>
                                       <?php echo $item->user_name;?>
                                    </dd>
                                    <dt><strong>Email Address </strong></dt>
                                    <dd>
                                     <?php echo $item->user_email;?>
                                    </dd>
                                    <dt><strong>Phone Number </strong></dt>
                                    <dd>
                                      <?php echo $item->user_contact;?>
                                    </dd>
                                   
                                    <dt><strong>Facebook </strong></dt>
                                    <dd>
                                       <?php echo $item->user_fb;?>
                                    </dd>
                                     <dt><strong>Instagram </strong></dt>
                                    <dd>
                                       <?php echo $item->user_insta;?>
                                    </dd>
                                   
                                 </dl>
                              </div>
                              <div class="profile-edit tab-pane fade" id="edit">
                                 <h2 class="heading-md">Manage your Security Settings</h2>
                                 <p>Manage Your Account</p>
                                 <div class="clearfix"></div>
                                 <form method="post" action="<?php echo base_url('index.php/Profile/update/'.$item->user_id) ?>">
                                    <div class="row">
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                          <label>Your Name </label>
                                          <input type="text" class="form-control margin-bottom-20"
                                          value="<?php echo $item->user_name;?>" name="name"
                                          >
                                       </div>
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                          <label>Email Address <span class="color-red">*</span></label>
                                          <input type="text" class="form-control margin-bottom-20"
                                           value="<?php echo $item->user_email;?>" name="email"> 
                                       </div>
                                       <div class="col-md-12 col-sm-12 col-xs-12">  
                                          <label>Contact Number <span class="color-red">*</span></label>
                                          <input type="text" class="form-control margin-bottom-20"
                                           value="<?php echo $item->user_contact;?>" name="phone">
                                       </div>
                                       <div class="col-md-12 col-sm-12 col-xs-12">  
                                          <label>Your Facebook <span class="color-red">*</span></label>
                                          <input type="text" class="form-control margin-bottom-20"
                                           value="<?php echo $item->user_fb;?>" name="fb">
                                       </div>
                                         <div class="col-md-12 col-sm-12 col-xs-12">  
                                          <label>Your Instagram <span class="color-red">*</span></label>
                                          <input type="text" class="form-control margin-bottom-20"
                                           value="<?php echo $item->user_insta;?>" name="insta">
                                       </div>

                                    </div>
                                    <div class="row margin-bottom-20">
                                       <div class="form-group">
                                          <div class="col-md-9">
                                             <div class="input-group">
                                                <span class="input-group-btn">
                                                <span class="btn btn-default btn-file">
                                                Browse… <input type="file" id="imgInp">
                                                </span>
                                                </span>
                                                <input type="text" class="form-control" readonly>
                                             </div>
                                          </div>
                                          <div class="col-md-3">
                                               <?php if (!empty($this->session->userdata('site_url'))){?>
<img id="img-upload" height="200" width="200" src="<?php echo $this->session->userdata('img')?>" width="100" height="100"> 
                              <?php } else { ?>
<img id="img-upload" height="200" width="200" src="<?php echo base_url()?>uploads/<?php echo $item->user_img;?>"> 
                                 <?php } ?>
                           
                                            
                                          </div>
                                       </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                       
                                       <div class="col-md-4 col-sm-4 col-xs-12 text-right">
                                          <button type="submit" class="btn btn-theme btn-sm" name="submit">Update Profile</button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                            
                           </div>
                        </div>
                     </div>
                     <!-- Row End -->
                    
                
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
                <?php 
                               $i = $i++;
                                  } ?>
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->