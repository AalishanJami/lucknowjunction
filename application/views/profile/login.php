<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Login</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Sign In to your account </h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                     <!--  Form -->
                     <div class="form-grid">
                     
                         <?php echo form_open_multipart('Login/auth') ?>
<!--                         
                          <a class="btn btn-lg btn-block btn-social btn-facebook">
            					<span class="fa fa-facebook"></span> Sign in with Facebook
          				  </a>
                          
                          <a class="btn btn-lg btn-block btn-social btn-google">
            					<span class="fa fa-google"></span> Sign in with Facebook
          				  </a>
                          
                          <h2 class="no-span"><b>(OR)</b></h2> -->
                        
                           <div class="form-group">
                              <label>Email</label>
                              <input placeholder="Your Email" name="email" class="form-control" type="email">
                           </div>
                           <div class="form-group">
                              <label>Password</label>
                              <input placeholder="Your Password" name="password" class="form-control" type="password">
                           </div>

                           <div class="form-group">
                                                    <?php if ($this->session->flashdata('error_login')) { ?>
    <p id="demo" style="color:red">
        <?php echo $this->session->flashdata('error_login'); ?>
    </p>
<?php } ?>
                 
                     <p id="demo" style="color:red"></p>
                     </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-xs-12">
                                    <div class="skin-minimal">
                                       <ul class="list">
                                          <li>
                                           <a href="<?php echo base_url()?>index.php/Login/forgotPassword">
                                             <label for="minimal-checkbox-1">Forgot Password?</label>
                                           </a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <button class="btn btn-theme btn-lg btn-block">Login With Us</button>
                           <?php form_close() ?>
                           <br>
                           <a href="<?php echo $fb_auth_url ?>">
                           <div class="btn btn-theme btn-lg btn-block" style="background-color: #3b5998 ">
                          <h5 style="color: #fff">
                           Login with Facebook
                          </h5>
                         </div>
                           </a>
                           <br>
                           <a href="<?php echo $google_auth_url ?>">
                           <div class="btn btn-theme btn-lg btn-block" style="background-color: #DB4437 " >
                          <h5 style="color: #fff">
                           Login with Google
                          </h5>
                         </div>
                           </a>
                          <br>
                           <center><h3>OR</h3></center>
                           <br>
                          <a href="<?php echo base_url()?>index.php/Register">
                           <div class="btn btn-theme btn-lg btn-block">
                          <h5 style="color: #fff">
                           Register
                          </h5>
                         </div>
                           </a>
                           
                     </div>
                     <!-- Form -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

         <script>
          window.onload = function what(){
var url = window.location.href;

var captured = '/auth'= + exec(url)[1]; // Value is in [1] ('384' in our case)
var result = captured ? console.log(captured) : 'myDefaultValue';

// var changed = /change=([^&]+)/.exec(url)[1]; // Value is in [1] ('384' in our case)
// var result = changed ? console.log(captured) : 'myDefaultValue';

// if (captured){
// document.getElementById("demo").innerHtml = "Incorrect email or password";
// }

if (changed){
document.getElementById("demo").innerHTML = "Password changed, Login to continue";
}
}


  </script>