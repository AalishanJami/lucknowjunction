<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Registration</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Create Your Account</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Registration Form =-=-=-=-=-=-= -->
         
         <section class="section-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                     <!--  Form -->
                     <div class="form-grid">
                     <?php echo form_open_multipart('Register/add') ?>
                        
                          <!-- <a class="btn btn-lg btn-block btn-social btn-facebook">
            					<span class="fa fa-facebook"></span> Sign up with Facebook
          				  </a>
                          
                          <a class="btn btn-lg btn-block btn-social btn-google">
            					<span class="fa fa-google"></span> Sign up with Facebook
          				  </a>
                          
                          <h2 class="no-span"><b>(OR)</b></h2> -->
                          <div class="form-group">
                              <label>Name</label>
                              <input placeholder="Enter Your Name" name="name" class="form-control" type="text">
                           </div>
                           <div class="form-group">
                              <label>Contact Number</label>
                              <input placeholder="Enter Your Contact Number" name="phone" class="form-control" type="text">
                           </div>
                           <div class="form-group">
                              <label>Email</label>
                              <input placeholder="Enter Your Email"  name="email" class="form-control" type="email">
                           </div>
                           <div class="form-group">
                              <label>Password</label>
                              <input placeholder="Enter Your Password" name="password" class="form-control" type="password">
                           </div>
                           <div class="form-group">
                              <label>Facebook URL</label>
                              <input placeholder="Enter Your Facebook URL" name="fb" class="form-control" type="text">
                           </div>
                           <div class="form-group">
                              <label>Instagram URL</label>
                              <input placeholder="Enter Your Instagram URL" name="insta" class="form-control" type="text">
                           </div>
                           <div class="form-group">
                              <label>Profile Picture</label>
                              <input type="file" id="userfile" name="userfile"  class="form-control">
                           </div>
                          
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-xs-12 col-sm-7">
                                    <div class="skin-minimal">
                                       <ul class="list">
                                          <li>
                                             <input  type="checkbox" id="minimal-checkbox-1">
                                             <label for="minimal-checkbox-1">I agree to the <a href="#">Terms of Services</a></label>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-sm-5 text-right">
                                    <p class="help-block"><a data-target="#myModal" data-toggle="modal">Forgot password?</a>
                                    </p>
                                 </div>
                              </div>
                           </div>
                           <input type='submit' name="submit" value="register" class="btn btn-theme btn-lg btn-block">
                         
                           <?php form_close() ?>
                     </div>
                     <!-- Form -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
          <!-- =-=-=-=-=-=-= Registration Form End =-=-=-=-=-=-= -->

          <!-- =-=-=-=-=-=-= Forgot Password =-=-=-=-=-=-= -->
          <div class="custom-modal">
         <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header rte">
                     <h2 class="modal-title">Forgot Your Password ?</h2>
                  </div>
                  <form>
                     <div class="modal-body">
                        <div class="form-group">
                           <label>Email</label>
                           <input placeholder="Enter Your Email Adress" class="form-control" type="email">
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-success">Reset My Account</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>