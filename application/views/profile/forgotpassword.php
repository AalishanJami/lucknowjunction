<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Login</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Enter the email you used to register</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                     <!--  Form -->
                     <div class="form-grid">
                        <?php if ($this->session->flashdata('error_email')) { ?>
    <h1 style="font-size: 15px; color:red;float: right;">
        <?php echo $this->session->flashdata('error_email'); ?>
    </h1>
<?php } ?>
<?php if ($this->session->flashdata('success_email')) { ?>
    <h1 style="font-size: 15px; color:red;float: right;">
        <?php echo $this->session->flashdata('success_email'); ?>
    </h1>
<?php } ?>
                     
                         <?php echo form_open_multipart('Login/recover') ?>
                        
                           <div class="form-group">
                              <label>Email</label>
                              <input placeholder="Your Email" name="email" class="form-control" type="email">
                           </div>

                           <div class="form-group">
                     <p id="demo" style="color:red"></p>
                     </div>
                 
                           <button class="btn btn-theme btn-lg btn-block">Recover</button>
                           <?php form_close() ?>
                     </div>
                     <!-- Form -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

         <script>
  var url = window.location.href;
//   console.log(url);
//   var c = url.searchParams.get("auth");
//     console.log(c);
var captured = /auth=([^&]+)/.exec(url)[1]; // Value is in [1] ('384' in our case)
var result = captured ? console.log(captured) : 'myDefaultValue';

if (captured){
document.getElementById("demo").innerHTML = "Incorrect email or password, please try again!";
}
  </script>