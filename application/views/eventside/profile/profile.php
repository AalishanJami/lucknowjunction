<section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-9">
                      
                    <div class="card">
                        <div class="body">

                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                   
                                    <li style="margin-right: 100px;" role="presentation" class="active"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab">Click To View Event Details </a></li>
                                    <li role="presentation"><a href="#change_password_settings" aria-controls="settings" role="tab" data-toggle="tab">Click To View Pricing Details</a></li>
                                </ul>

                                <div class="tab-content">
                                    
                                    <div role="tabpanel" class="tab-pane fade in" id="profile_settings">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Event Name</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" readonly class="form-control" id="NameSurname" name="NameSurname" placeholder="Name Surname" value="<?php echo $events[0]->event_name; ?>" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="InputExperience" class="col-sm-2 control-label">Date</label>

                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                         <input type="text" readonly class="form-control" id="NameSurname" name="NameSurname" placeholder="Name Surname" value="<?php echo $events[0]->event_date; ?>" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="InputSkills" class="col-sm-2 control-label">Venue</label>

                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="InputSkills" name="InputSkills" placeholder="Skills" value="<?php echo $events[0]->event_chiefGuest; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="InputSkills" class="col-sm-2 control-label">Country</label>

                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="InputSkills" name="InputSkills" placeholder="Skills" value="<?php echo $events[0]->country_name; ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                <label for="InputSkills" class="col-sm-2 control-label">City</label>

                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="InputSkills" name="InputSkills" placeholder="Skills" value="<?php echo $events[0]->city_name; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label for="InputSkills" class="col-sm-2 control-label">State</label>

                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="InputSkills" name="InputSkills" placeholder="Skills" value="<?php echo $events[0]->state_name; ?>">
                                                    </div>
                                                </div>
                                            </div>



                                            </div>
                                           
                                        </form>
                                    </div>
                                     <?php $i = 1; foreach($prices as $price){ ?>
                                    <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label for="OldPassword" class="col-sm-3 control-label">Ticket Class</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="text" readonly class="form-control" id="OldPassword" name="OldPassword" placeholder="Old Password" value="<?php echo $price->class_name;?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NewPassword" class="col-sm-3 control-label">Ticket Price(INR)</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="text" readonly class="form-control" id="NewPassword" name="NewPassword" placeholder="New Password" value="<?php echo $price->class_price;?>">
                                                    </div>
                                                </div>
                                            </div>
                                          
                                          
                                        </form>
                                    </div>
                                     <?php $i = $i +1;} ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>