<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="font-size: 24px; font-weight: bold;">User Reviews</h2>
        </div><br><br>

        <div class="row clearfix">
              <?php 
            $i = 1;
              foreach($items as $item){?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="card profile-card">
                    <div class="profile-header">&nbsp;</div>
                    <div class="profile-body">
                        <div class="image-area">
                            <img class="image-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $item->user_img;?>" width="200"/>
                        </div>
                        <div class="content-area">
                            <h3><?php echo $item->user_name;?> </h3>
                            <p>Date Posted: <?php echo $item->review_date;?> </p>
                            <p></p>
                        </div>
                    </div>
                    <div class="profile-footer">
                        <ul>
                            <p><span style="font-weight: bold;">Review</span> &nbsp&nbsp " <?php echo $item->event_review;?>"</p><br>

                             </ul>
                    </div>
                </div>
            </div>
            <?php $i = $i + 1; 
                            } ?>
           
            
           </div>
    </div>
    </div>

</section>