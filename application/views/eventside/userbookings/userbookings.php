<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="font-size: 24px; font-weight: bold;">User's Bookings</h2>
        </div><br><br>
        <div class="row clearfix">
        
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?php 
                 $i = 1;
              foreach($items as $item){?>
                <div class="card profile-card">
                    <div class="profile-header">&nbsp;</div>
                    <div class="profile-body">
                        <div class="image-area">
                             <img class="image-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $item->user_img;?>" width="200"/>
                        </div>
                        <div class="content-area">
                            <h3><?php echo $item->user_name;?></h3>
                            <p><?php echo $item->user_email;?></p>
                            <p><?php echo $item->user_fb;?></p>
                            <p><?php echo $item->user_contact;?></p>
                            <p></p>
                        </div>
                    </div>
                    <div class="profile-footer">
                        <ul>
                        
                            <!-- <p><span style="font-weight: bold;">Review</span> &nbsp&nbsp""</p><br> -->

                                    
                            <div style="float: right;">
                            
                               <a
                                            href="<?php echo base_url('index.php/eventside/UserBookings/active/'.$item->event_bookinglistId) ?>"><img
                                                src="<?php echo base_url(); ?>admin/images/tick.jpg"
                                                height="15"></a><strong style="letter-spacing: 10px;"> |</strong><a
                                            href="<?php echo base_url('index.php/eventside/UserBookings/deactive/'.$item->event_bookinglistId) ?>"><img
                                                src="<?php echo base_url(); ?>admin/images/cross.jpg" height="13"></a>
                        </ul><br>
                    </div>
                </div>

          <?php $i = $i + 1; 
                            } ?>
            </div>
            
            
         
            
            
            
    </div>
    </div>
    </div>

</section>