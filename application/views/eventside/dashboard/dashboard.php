<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="info-box bg-pink hover-expand-effect" style="height: 350px;">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">User Bookings</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $bookings?>" data-speed="1000" data-fresh-interval="20" style="margin-top: 30px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="info-box bg-cyan hover-expand-effect" style="height: 350px;">
                        <div class="icon">
                            <i class="material-icons">rate_review</i>
                        </div>
                        <div class="content">
                            <div class="text">User Reviews</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $reviews?>" data-speed="1000" data-fresh-interval="20" style="margin-top: 30px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->