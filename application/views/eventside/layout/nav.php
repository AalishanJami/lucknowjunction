<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url(); ?>admin/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('name') ?></div>
                    <div class="email"><?php echo $this->session->userdata('email') ?></div>
                    <!-- <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?php echo base_url('index.php/eventside/Profile') ?>"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo base_url('index.php/admin/Signin') ?>"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/eventside/Dashboard/index/'.$this->session->userdata('event_id')) ?>">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/eventside/UserReviews') ?>">
                            <i class="material-icons">rate_review</i>
                            <span>User Reviews</span>
                        </a>
                    </li>
                     <li>
                        <a href="<?php echo base_url('index.php/eventside/UserBookings') ?>">
                            <i class="material-icons">book</i>
                            <span>User Bookings</span>
                        </a>
                    </li>
                     <li> 
                                <a href="<?php echo base_url('index.php/eventside/Profile') ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profile</span>
                                </a>
                            </li>
                          
                   
                                  </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>