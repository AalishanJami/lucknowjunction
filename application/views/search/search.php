<style>
    .date {
        width: 70px; height: 80px; 
        background: #fcfcfc; 
        background: linear-gradient(top, #fcfcfc 0%,#dad8d8 100%); 
        background: -moz-linear-gradient(top, #fcfcfc 0%, #dad8d8 100%); 
        background: -webkit-linear-gradient(top, #fcfcfc 0%,#dad8d8 100%); 
        border: 1px solid #d2d2d2;
        border-radius: 10px;
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        box-shadow: 0px 0px 15px rgba(0,0,0,0.1);
        -moz-box-shadow: 0px 0px 15px rgba(0,0,0,0.1);
        -webkit-box-shadow: 0px 0px 15px rgba(0,0,0,0.1);

    }
    .date p {
        font-family: Helvetica, sans-serif; 
        font-size: January0px; text-align: center; color: #9e9e9e; 
    }
    .date p span {
        background: #d10000; 
        background: linear-gradient(top, #d10000 0%, #7a0909 100%);
        background: -moz-linear-gradient(top, #d10000 0%, #7a0909 100%);
        background: -webkit-linear-gradient(top, #d10000 0%, #7a0909 100%);
        font-size: 23px; font-weight: bold; color: #fff; text-transform: uppercase;     
        display: block;
        border-top: 3px solid #a13838;
        border-radius: 0 0 10px 10px;
        -moz-border-radius: 0 0 10px 10px;
        -webkit-border-radius: 0 0 10px 10px;
        padding: 6px 0 6px 0;
    }

</style>

<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class="breadcrumb-link">
                        <ul>
                           <li><a href="#">Search Results</a></li>
                           <li><a class="active" href="#">Filters</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Filter Events</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->

      <!-- =-=-=-=-=-=-= Search Bar =-=-=-=-=-=-= -->
         <div class="search-bar">
            <div class="section-search search-style-2">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="clearfix">
                        <?php echo form_open_multipart('home/search', array('id' => 'search')); ?>
                              <div class="search-form pull-left">
                                 <div class="search-form-inner pull-left">
                                    <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
                                       <div class="form-group">
                                       <label>Category:
                                       <select name="category" form="search" class="model form-control">
                                        <option value="0">Select Category</option>
                                        <?php foreach ($category as $item){ ?>
                                        <option value="<?php echo $item->category_id; ?>">
                                            <?php echo $item->category_name; ?></option>
                                        <?php } ?>
                                    </select>
                                       </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
                                       <div class="form-group">
                                       <label>City:
                                       <select name="city" form="search" class="model form-control">
                                        <option value="0">Select City</option>
                                        <?php foreach ($city as $item){ ?>
                                        <option value="<?php echo $item->city_id; ?>"><?php echo $item->city_name; ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                       </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
                                       <div class="form-group">
                                         <label>Name:
                                          <input type="text" name="name" class="form-control"
                                        placeholder="Enter event name" />
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group pull-right">
                                    <button type="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                 </div>
                              </div>
                              <?php form_close() ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--================================../SEARCH STYLE 2================================-->
         </div>
         <!-- =-=-=-=-=-=-= Search Bar End =-=-=-=-=-=-= -->

       <!-- =-=-=-=-=-=-= Ads Archieve =-=-=-=-=-=-= --> 
         <section class="custom-padding">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="clearfix"></div>
                  <!-- Heading Area -->
                  <div class="heading-panel">
                     <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h1>Filter<span class="heading-color"> Events</span></h1>
                        <!-- Short Description -->
                        </div>
                  </div>
                  <!-- Middle Content Box -->
                  <div class="row">
                     <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="">
                        <?php if($results !=0){
foreach($results as $item){ ?>
                        
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img height="200" width="200" alt="" src="<?php echo base_url()?>/uploads/<?php echo $item->event_photo ?>">
                                    <!-- Ad Status -->
                         <div class="short-description">

                                <div class="row">
                                    <div class="col-xs-3">
                                     <div class="date">
                                        <p><?php echo array_map('trim', explode(' ', date('d', strtotime($item->event_date))))[0]; ?><span><?php echo array_map('trim', explode(' ', date('M', strtotime($item->event_date))))[0]; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                       <h3><a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>"><?php echo  $out = strlen($item->event_name) > 30 ? substr($item->event_name,0,50)."..." : $item->event_name; ?></a></h3>
                                       <span><strong>Time: </strong><?php echo $item->event_time; ?></span>
                                       <br>
                                       <?php if($item->is_pricing == "1"){ ?>
                                           <span><strong>Prices from: <strong><a>INR. <?php echo $item->base_price; ?></a></span>
                                           <?php } else { ?>
                                            <span><strong>Type: </strong>Free Event</span>
                                        <?php } ?>

                                    </div>
                                </div>     
                            </div>
                                    
                                 </div>
                                 <!-- Ad Img End -->
                                
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                        <?php }} ?>
                        <?php if($results ==0){ ?>
                        <h3>No results found </h3>
                        <?php } ?>
                        </div>
                     </div>
                  </div>
                  <!-- Middle Content Box End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archieve End =-=-=-=-=-=-= -->