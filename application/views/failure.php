 <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
 <style>
    .rate {
        float: left;
        height: 46px;
        padding: 0 10px;
    }
    .rate:not(:checked) > input {
        position:absolute;
        top:-9999px;
    }
    .rate:not(:checked) > label {
        float:right;
        width:1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:30px;
        color:#ccc;
    }
    .rate:not(:checked) > label:before {
        content: '★ ';
    }
    .rate > input:checked ~ label {
        color: #ffc700;    
    }
    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;  
    }
    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }
</style>


<div class="page-header-area-2 gray">
 <div class="container">
     <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="small-breadcrumb">
                 <div class=" breadcrumb-link">
                     <ul>
                         <li><a href="<?php echo base_url();?>">Home Page</a></li>
                         <li><a class="active" href="#">Checkout</a></li>
                     </ul>
                 </div>
                 <div class="header-page">
                     <h1>Checkout</h1>
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
 <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
 <section class="section-padding no-top gray">
     <!-- Main Container -->
     <div class="container">
         <!-- Row -->
         <div class="row">
             <!-- Middle Content Area -->
            
                <!-- Middle Content Area  End -->
            </div>
            <!-- Row End -->
            <div class="row margin-top-40">
             <!-- Middle Content Area -->
             <div class="col-md-12 col-xs-12 col-sm-12">
                 <!-- Row -->
                 <div class="profile-section margin-bottom-20">
                     <div class="profile-tabs">
                      
                         <div class="tab-content">
                        
                                 
        		<div class="container">
        			<h3> Your Payment Failed, Please try again later</h3>
        		</div>

                              </div>
                       
                          </div>
                      </div>
                  </div>
                  <!-- Row End -->
              </div>
              <!-- Middle Content Area  End -->
          </div>
      </div>
      <!-- Main Container End -->
  </section>
  <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

  