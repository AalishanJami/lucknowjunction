<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Foods & Drinks Events</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Upcoming Foods & Drinks Events</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
         <!-- =-=-=-=-=-=-= Ads Archieve =-=-=-=-=-=-= --> 
         <section class="custom-padding">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="clearfix"></div>
                  <!-- Heading Area -->
                  <div class="heading-panel">
                     <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h1>Top Foods & Drinks<span class="heading-color"> Events</span></h1>
                        <!-- Short Description -->
                        </div>
                  </div>
                  <!-- Middle Content Box -->
                  <div class="row">
                     <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="posts-masonry">
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/2.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/2.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">Porsche 911 Carrera 2017 </a></h3>
                                    <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                    <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/1.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/4.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2017 Audi A4 quattro Premium</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/3.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/6.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2014 Ford Shelby GT500 Coupe</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/4.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/2.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">BMW I8 1.5 Auto 4X4 2dr </a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/5.png">
                                    <!-- Ad Status --><span class="ad-status"> Featured </span>
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/1.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Brand New </a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/6.png">
                                    <!-- Ad Status --><span class="ad-status"> Featured </span>
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/1.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">McLaren F1 Sports Car</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/2.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/2.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">Porsche 911 Carrera 2017 </a></h3>
                                    <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                    <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/1.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/4.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2017 Audi A4 quattro Premium</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/3.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/6.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2014 Ford Shelby GT500 Coupe</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/2.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/2.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">Porsche 911 Carrera 2017 </a></h3>
                                    <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                    <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/1.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/4.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2017 Audi A4 quattro Premium</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/3.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/6.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2014 Ford Shelby GT500 Coupe</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/2.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/2.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">Porsche 911 Carrera 2017 </a></h3>
                                    <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                    <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/1.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/4.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2017 Audi A4 quattro Premium</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img class="img-responsive" alt="" src="<?php echo base_url(); ?>assets/images/posting/3.png">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/6.png" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    <!-- View Details --><a href="#" class="view-details">View Details</a>
                                    <!-- Additional Info -->
                                    <div class="additional-information">
                                       <p>Transmission : Automatic</p>
                                       <p>Engine Capacity : 1799 cc</p>
                                       <p>Engine Type : Petrol</p>
                                       <p>Mileage : 103,198 km</p>
                                       <p>Registered City : London</p>
                                    </div>
                                    <!-- Additional Info End-->
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">2014 Ford Shelby GT500 Coupe</a></h3>
                                   <!-- Location -->
                                 <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                   <span><strong>Nov 17</strong></span><br><br>
                                 <span class="ad-price">INR 200</span>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                           
                        </div>
                     </div>
                  </div>
                  <!-- Middle Content Box End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archieve End =-=-=-=-=-=-= -->