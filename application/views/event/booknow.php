 <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
 <style>
    .rate {
        float: left;
        height: 46px;
        padding: 0 10px;
    }
    .rate:not(:checked) > input {
        position:absolute;
        top:-9999px;
    }
    .rate:not(:checked) > label {
        float:right;
        width:1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:30px;
        color:#ccc;
    }
    .rate:not(:checked) > label:before {
        content: '★ ';
    }
    .rate > input:checked ~ label {
        color: #ffc700;    
    }
    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;  
    }
    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }
</style>


<div class="page-header-area-2 gray">
 <div class="container">
     <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="small-breadcrumb">
                 <div class=" breadcrumb-link">
                     <ul>
                         <li><a href="<?php echo base_url();?>">Home Page</a></li>
                         <li><a class="active" href="#">Event Details</a></li>
                     </ul>
                 </div>
                 <div class="header-page">
                     <h1>Event Details</h1>
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
 <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
 <section class="section-padding no-top gray">
     <!-- Main Container -->
     <div class="container">
         <!-- Row -->
         <div class="row">
             <!-- Middle Content Area -->
             <div class="col-md-12 col-xs-12 col-sm-12">
                 <section class="search-result-item">
                     <a class="image-link" href="#"><img class="image center-block" alt=""
                         src="<?php echo base_url(); ?>uploads/<?php echo $events[0]->event_photo;?>"> </a>
                         <div class="search-result-item-body">
                             <div class="row">
                                 <div class="col-md-5 col-sm-12 col-xs-12">
                                     <h4 class="search-result-item-heading"><a
                                         href="#"><?php echo $events[0]->event_name;?></a></h4>
                                     <!-- <p class="info">
                                    <span><a href="profile.html"><i class="fa fa-user "></i>Profile </a></span>
                                    <span><a href="javascript:void(0)"><i class="fa fa-edit"></i>Edit Profile </a></span>
                                 </p>
                                 <p class="description">You last logged in at: 14-01-2017 6:40 AM [ USA time (GMT + 6:00hrs)</p> -->
                                 <span class="label label-warning">Verified</span>
                                 <span class="label label-success">Category</span>
                                 <br>
 
                                 </div>

                                 <div class="col-md-7 col-sm-12 col-xs-12">
                                     <div class="row ad-history">
                                         <div class="col-md-4 col-sm-4 col-xs-12">
                                             <div class="user-stats">
                                                 <h2><?php echo $bookings ?></h2>
                                                 <small>Bookings</small>
                                             </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-12">
                                             <div class="user-stats">
                                                 <h2><?php echo $ratings ?></h2>
                                                 <small>Rating</small>
                                             </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-12">
                                             <div class="user-stats">
                                                 <h2><?php echo $reviews ?></h2>
                                                 <small>Reviews</small>
                                             </div>
                                         </div>

                                     </div>
                                 </div>

                             </div>

                      

                     </section>
                     <!-- <div class="dashboard-menu-container">
                        <ul>
                           <li class="active">
                              <a href="profile.html">
                                 <div class="menu-name"> Profile </div>
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <div class="menu-name">Logout</div>
                              </a>
                           </li>
                        </ul>
                    </div> -->
                </div>
                <!-- Middle Content Area  End -->
            </div>
            <!-- Row End -->
            <div class="row margin-top-40">
             <!-- Middle Content Area -->
             <div class="col-md-12 col-xs-12 col-sm-12">
                 <!-- Row -->
                 <div class="profile-section margin-bottom-20">
                     <div class="profile-tabs">
                      
                         <div class="tab-content">
                        
                                     <h2 class="heading-md">Book Now</h2>
                                     <br>
                                     <div class="row">
                                         <div class="col-sm-12 col-md-6 margin-bottom-20">
                                             <label>Available classes for the event: &nbsp;
                                                 <?php echo $events[0]->event_price;?></label>
                                             </div>
                                         </div>
                                         <?php echo form_open_multipart('EventDetails/payment', array('id' => 'event', 'class'=> 'submit-form')) ?>
                                             <div class="form-group">
                                                <label>Select Ticket Class </label>
                                                <select name="class" form="event" class="model form-control">
                                                    <option value="0">Ticket Classes</option>
                                                    <?php foreach($prices as $price){ ?>
                                                        <option value="<?php echo $price->class_price; ?>"> <?php echo $price->class_name?>:  INR. <?php echo $price->class_price;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                              <input name="event" type="hidden" value="<?php echo $events[0]->event_id?>">
                                          </div>

                                          <input type="submit" class="btn btn-theme ">
                                   <?php form_close() ?>


                              </div>
                       
                          </div>
                      </div>
                  </div>
                  <!-- Row End -->
              </div>
              <!-- Middle Content Area  End -->
          </div>
      </div>
      <!-- Main Container End -->
  </section>
  <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

  <script type="text/javascript">
      function submitForm(){
        var form = document.getElementById('event');
        form.submit();
      }
  </script>
