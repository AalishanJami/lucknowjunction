<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Events Directory</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Events Directory</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Background Rotator =-=-=-=-=-=-= -->
      <div class="background-rotator">
         <!-- slider start-->
         <div class="owl-carousel owl-theme background-rotator-slider">
            <!-- Slide -->
            <!-- Slide -->
        <div class="item linear-overlay"><img src="<?php echo base_url(); ?>assets/images/slider/1.JPG" alt=""></div>
        <!-- Slide -->
        <div class="item linear-overlay"><img src="<?php echo base_url(); ?>assets/images/slider/2.JPG" alt=""></div>
        <!-- Slide -->
        <div class="item linear-overlay"><img src="<?php echo base_url(); ?>assets/images/slider/3.JPG" alt=""></div>
         </div>

         <div class="search-section" style="margin-top: -150px;">
            <!-- Find search section -->
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <!-- Heading -->
                     <div class="content">
                        <div class="heading-caption">
                           <h1>Discover Events Happening in Your City</h1>
                           <p style="font-size: 20px;">200M+ Events &nbsp | &nbsp 30,000 Cities &nbsp | &nbsp 4M People Exploring Events every month</p>
                        </div>
                        <div class="search-form">
                        <?php echo form_open_multipart('Home/search', array('id' => 'search', 'class'=> 'submit-form')); ?>
                              <div class="row">
                                 <div class="col-md-4 col-xs-12 col-sm-4">
                                    <!-- Category -->
                                    <select name="category" form="search" class="model form-control">
                                        <option value="0">Select Category</option>
                                        <?php foreach ($category as $item){ ?>
                                        <option value="<?php echo $item->category_id; ?>">
                                            <?php echo $item->category_name; ?></option>
                                        <?php } ?>
                                    </select>
                                 </div>
                                 <!-- Input Field -->
                                 <div class="col-md-4 col-xs-12 col-sm-4">
                                 <input type="text" name="name" class="form-control"
                                        placeholder="Enter event name" />
                                 </div>
                                 <!-- Input Field -->
                                 <!-- Search Button -->
                                 <div class="col-md-4 col-xs-12 col-sm-4">
                                    <button type="submit" class="btn btn-theme btn-block">Search <i class="fa fa-search" aria-hidden="true"></i></button>
                                 </div>
                              </div>
                              <?php form_close() ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- /.Find search section-->
      </div>
      <!-- =-=-=-=-=-=-= Background Rotator End =-=-=-=-=-=-= -->