
<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
<style>
	.rate {
		float: left;
		height: 46px;
		padding: 0 10px;
	}
	.rate:not(:checked) > input {
		position:absolute;
		top:-9999px;
	}
	.rate:not(:checked) > label {
		float:right;
		width:1em;
		overflow:hidden;
		white-space:nowrap;
		cursor:pointer;
		font-size:30px;
		color:#ccc;
	}
	.rate:not(:checked) > label:before {
		content: '★ ';
	}
	.rate > input:checked ~ label {
		color: #ffc700;    
	}
	.rate:not(:checked) > label:hover,
	.rate:not(:checked) > label:hover ~ label {
		color: #deb217;  
	}
	.rate > input:checked + label:hover,
	.rate > input:checked + label:hover ~ label,
	.rate > input:checked ~ label:hover,
	.rate > input:checked ~ label:hover ~ label,
	.rate > label:hover ~ input:checked ~ label {
		color: #c59b08;
	}
</style>


<div class="page-header-area-2 gray">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="small-breadcrumb">
					<div class=" breadcrumb-link">
						<ul>
							<li><a href="<?php echo base_url();?>">Home Page</a></li>
							<li><a class="active" href="#">Event Details</a></li>
						</ul>
					</div>
					<div class="header-page">
						<h1>Select Ticket Class</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
	<!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
	<section class="section-padding no-top gray">
		<!-- Main Container -->
		<div class="container">
			<!-- Row -->
			<div class="row">
				<!-- Middle Content Area -->
				<div class="col-md-12 col-xs-12 col-sm-12">
					<section class="search-result-item">
						<a class="image-link" href="#"><img class="image center-block" alt=""
							src="<?php echo base_url(); ?>uploads/<?php echo $events[0]->event_photo;?>"> </a>
							<div class="search-result-item-body">
								<div class="row">
									<div class="col-md-5 col-sm-12 col-xs-12">
										<h4 class="search-result-item-heading"><a
											href="#"><?php echo $events[0]->event_name;?></a></h4>
                                     <!-- <p class="info">
                                    <span><a href="profile.html"><i class="fa fa-user "></i>Profile </a></span>
                                    <span><a href="javascript:void(0)"><i class="fa fa-edit"></i>Edit Profile </a></span>
                                 </p>
                                 <p class="description">You last logged in at: 14-01-2017 6:40 AM [ USA time (GMT + 6:00hrs)</p> -->
                                 <span class="label label-warning">Verified</span>
                                 <span class="label label-success">Category</span>
                                 <br>
                                 
                                 </div>

                                 <div class="col-md-7 col-sm-12 col-xs-12">
                                 	<div class="row ad-history">
                                 		<div class="col-md-4 col-sm-4 col-xs-12">
                                 			<div class="user-stats">
                                 				<h2><?php echo $bookings ?></h2>
                                 				<small>Bookings</small>
                                 			</div>
                                 		</div>
                                 		<div class="col-md-4 col-sm-4 col-xs-12">
                                 			<div class="user-stats">
                                 				<h2><?php echo $ratings ?></h2>
                                 				<small>Rating</small>
                                 			</div>
                                 		</div>
                                 		<div class="col-md-4 col-sm-4 col-xs-12">
                                 			<div class="user-stats">
                                 				<h2><?php echo $reviews ?></h2>
                                 				<small>Reviews</small>
                                 			</div>
                                 		</div>

                                 	</div>
                                 </div>
                             </div>
                         </div>
                     </section>
                     
                 </div>
                 <!-- Middle Content Area  End -->
             </div>
             <!-- Row End -->
             <div class="row margin-top-40">
             	<!-- Middle Content Area -->
             	<div class="col-md-12 col-xs-12 col-sm-12">
             		<!-- Row -->
             		<div class="profile-section margin-bottom-20">
             			<div class="profile-tabs">
             				<ul class="nav nav-justified nav-tabs">
             					<li class="active"><a href="#bookings" data-toggle="tab">Payment</a></li>
             				</ul>
             				
             						
             						<br>
             						<div class="row">
             							<br>
             							<?php echo form_open_multipart('EventDetails/check') ?>
             							<div class="form-group">
             								<label>Name</label>
             								<input name="firstname" placeholder="Your first name" class="form-control" value="<?php echo $this->session->userdata('name') ?>">
             							</div>
             							<div class="form-group">
             								<label>Email</label>
             								<input name="email" placeholder="Your email address" class="form-control" type="email" value="<?php echo $this->session->userdata('email') ?>">
             							</div>
             							<div class="form-group">
             								<label>Phone</label>
             								<input name="phone" placeholder="Your phone number" class="form-control" type="tel" value="">
             							</div>
             								<input name="productinfo" class="form-control" type="hidden" value="<?php echo $events[0]->event_id?>,<?php echo $this->session->userdata('id');?>">

             								<input name="amount" class="form-control" type="hidden" value="<?php echo $amount?>">

             								<input type="submit" class="btn btn-theme " value="Proceed">
             							<?php form_close() ?>
             						</div>
             					</div>
             				</div>
             			</div>
             			<!-- Row End -->
             		</div>
             		<!-- Middle Content Area  End -->
             	</div>
             </div>
             <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

         <script type="text/javascript">
         	function submitForm(){
         		var form = document.getElementById('event');
         		form.submit();
         	}
         </script>
