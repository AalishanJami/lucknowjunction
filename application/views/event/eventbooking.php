 <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Event Bookings</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Event Bookings</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-12 col-lg-12 col-sx-12">
                     <!-- Row -->
                     <ul class="list-unstyled">
                        <?php foreach($bookings as $booking){ ?>
                        <li>
                           <div class="well ad-listing clearfix">
                              <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                 <!-- Image Box -->
                                 <div class="img-box">
                                    <img src="<?php echo base_url(); ?>uploads/<?php echo $booking->event_photo ?>" height="200" alt="">
                                    <!-- <div class="total-images"><strong>8</strong> photos </div> -->
                                    <!-- <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div> -->
                                 </div>
                                 <!-- Ad Status --><span class="ad-status"> Featured </span>
                                 <!-- User Preview -->
                                 <!-- <div class="user-preview">
                                    <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/2.jpg" class="avatar avatar-small" alt=""> </a>
                                 </div> -->
                              </div>
                              <div class="col-md-9 col-sm-7 col-xs-12">
                                 <!-- Ad Content-->
                                 <div class="row">

                                    <div class="content-area">
                                       <div class="col-md-9 col-sm-12 col-xs-12">
                                          <!-- Ad Title -->
                                          <h3><a href="<?php echo base_url('index.php/EventDetails/index/'.$booking->event_id);?>"><?php echo $booking->event_name ?></a></h3>
                                          <!-- Ad Meta Info -->
                                          <ul class="ad-meta-info">
                                             <li><span>Time: </span><?php echo $booking->event_time ?></a> </li>
                                             <li><span>Date: </span><?php echo $booking->event_date ?> </li>
                                          </ul>
                                          <!-- Ad Description-->
                                          <div class="ad-details">
                                             <span>Ticket type:</span>
                                             <span><?php if($booking->is_pricing == "1"){?>Priced<?php } else{ ?> Free <?php } ?></span>
                                          </div>

                                          <div class="ad-details">
                                             <span>Ticket Count:</span>
                                             <span>1</span>
                                          </div>
                                          <?php if($booking->is_pricing == "1"){?>
                                          <div class="ad-details">
                                             <span>Amount Paid:</span>
                                             <span><?php echo $booking->amount_paid; ?></span>
                                          </div>
                                       <?php } ?>
                                       </div>
                                       <div class="col-md-3 col-xs-12 col-sm-12">
                                          <!-- Ad Stats -->
                                          <div class="short-info">
                                             <div class="ad-stats hidden-xs"><span>Status  : </span><?php if($booking->booking_status =="1"){
                                                echo "Active";
                                             } else {
                                                echo "Not Active";
                                             }?></div>
                                             
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Ad Content End -->
                              </div>
                           </div>
                        </li>
                        <?php } ?>
                     </ul>
                    </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
         <!-- Product Preview Popup -->
      <div class="quick-view-modal modalopen" id="ad-preview" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog modal-lg ad-modal">
            <button class="close close-btn popup-cls" aria-label="Close" data-dismiss="modal" type="button"> <i class="fa-times fa"></i> </button>
            <div class="modal-content single-product">
               <div class="diblock">
                  <div class="col-lg-7 col-sm-12 col-xs-12">
                     <div class="clearfix"></div>
                     <div id="single-slider" class="flexslider">
                        <ul class="slides">
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/1.jpg" /></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/2.jpg" /></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/3.jpg" /></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/4.jpg" /></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/5.jpg" /></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/6.jpg" /></li>
                        </ul>
                     </div>
                     <div id="carousel" class="flexslider">
                        <ul class="slides">
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/1_thumb.jpg"></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/2_thumb.jpg"></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/3_thumb.jpg"> </li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/4_thumb.jpg"></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/5_thumb.jpg"></li>
                           <li><img alt="" src="<?php echo base_url(); ?>assets/images/single-page/6_thumb.jpg"></li>
                        </ul>
                     </div>
                  </div>
                  <div class=" col-sm-12 col-lg-5 col-xs-12">
                     <div class="summary entry-summary">
                        <div class="ad-preview-details">
                           <a href="#">
                              <h4>Honda Civic Sports Edition 2017</h4>
                           </a>
                           <div class="overview-price">
                              <span>$36.000</span>
                           </div>
                           <div class="clearfix"></div>
                           <p>Tattooed fashion axe Blue Bottle butcher tilde. Pitchfork leggings meh Odd Future.Drinking vinegar. </p>
                           <ul class="ad-preview-info col-md-6 col-sm-6">
                              <li>
                                 <span>Fabrication:</span>
                                 <p>2013/2014</p>
                              </li>
                              <li>
                                 <span>Speed:</span>
                                 <p>160p/h</p>
                              </li>
                              <li>
                                 <span>Mileage:</span>
                                 <p>30.000km - 40.000km</p>
                              </li>
                              <li>
                                 <span>Fuel:</span>
                                 <p>Petrol</p>
                              </li>
                           </ul>
                           <ul class="ad-preview-info col-md-6 col-sm-6">
                              <li>
                                 <span>Color:</span>
                                 <p>Black</p>
                              </li>
                              <li>
                                 <span>Transmition:</span>
                                 <p>Automatic</p>
                              </li>
                              <li>
                                 <span>Dors:</span>
                                 <p>4/5</p>
                              </li>
                              <li>
                                 <span>Engine:</span>
                                 <p>2500cm3</p>
                              </li>
                           </ul>
                           <button class="btn btn-theme btn-block" type="submit">Contact Dealer</button>
                        </div>
                     </div>
                     <!-- .summary -->
                  </div>
               </div>
            </div>
         </div>
      </div>