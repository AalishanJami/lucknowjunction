<style>
    .date {
        width: 70px; height: 80px; 
        background: #fcfcfc; 
        background: linear-gradient(top, #fcfcfc 0%,#dad8d8 100%); 
        background: -moz-linear-gradient(top, #fcfcfc 0%, #dad8d8 100%); 
        background: -webkit-linear-gradient(top, #fcfcfc 0%,#dad8d8 100%); 
        border: 1px solid #d2d2d2;
        border-radius: 10px;
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        box-shadow: 0px 0px 15px rgba(0,0,0,0.1);
        -moz-box-shadow: 0px 0px 15px rgba(0,0,0,0.1);
        -webkit-box-shadow: 0px 0px 15px rgba(0,0,0,0.1);

    }
    .date p {
        font-family: Helvetica, sans-serif; 
        font-size: January0px; text-align: center; color: #9e9e9e; 
    }
    .date p span {
        background: #d10000; 
        background: linear-gradient(top, #d10000 0%, #7a0909 100%);
        background: -moz-linear-gradient(top, #d10000 0%, #7a0909 100%);
        background: -webkit-linear-gradient(top, #d10000 0%, #7a0909 100%);
        font-size: 23px; font-weight: bold; color: #fff; text-transform: uppercase;     
        display: block;
        border-top: 3px solid #a13838;
        border-radius: 0 0 10px 10px;
        -moz-border-radius: 0 0 10px 10px;
        -webkit-border-radius: 0 0 10px 10px;
        padding: 6px 0 6px 0;
    }

</style>

<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
<div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                  
                     <?php if($type == 'category'){?>

                     <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Categories</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1><?php echo $name[0]->category_name ?></h1>
                     </div>
                     <?php }  
                     if($type != 'category'){ ?>
                        <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Events</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Events</h1>
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php if($type=='category'){ ?>
      <div class="main-content-area clearfix">
            <!-- =-=-=-=-=-=-= Trending Ads =-=-=-=-=-=-= -->
            <section class="custom-padding white">
                <!-- Main Container -->
                <div class="container">
                    <!-- Row -->
                    <div class="row">
                        <!-- Heading Area -->
                        <div class="heading-panel">
                            <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                                <!-- Main Title -->
                                <h1><span class="heading-color" style="font-size: 18px">“<?php echo $name[0]->category_desc ?>” </span></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
     <?php } ?>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
         <!-- =-=-=-=-=-=-= Ads Archieve =-=-=-=-=-=-= --> 
         <section class="custom-padding">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="clearfix"></div>
                  <!-- Heading Area -->
                
                  <!-- Middle Content Box -->
                  <div class="row">
                     <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="">
                        <?php foreach($all as $item){ ?>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img height="200" width="200" alt="" src="<?php echo base_url(); ?>/uploads/<?php echo $item->event_photo?>">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>"> <img src="<?php echo base_url(); ?>uploads/<?php echo $item->event_photo; ?>" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    
                                 </div>
                                 <!-- Ad Img End -->
                                <div class="short-description">

                                <div class="row">
                                    <div class="col-xs-3">
                                     <div class="date">
                                        <p><?php echo array_map('trim', explode(' ', date('d', strtotime($item->event_date))))[0]; ?><span><?php echo array_map('trim', explode(' ', date('M', strtotime($item->event_date))))[0]; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                       <h3><a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>"><?php echo  $out = strlen($item->event_name) > 30 ? substr($item->event_name,0,50)."..." : $item->event_name; ?></a></h3>
                                       <span><strong>Time: </strong><?php echo $item->event_time; ?></span>
                                       <br>
                                       <?php if($item->is_pricing == "1"){ ?>
                                           <span><strong>Prices from: <strong><a>INR. <?php echo $item->base_price; ?></a></span>
                                           <?php } else { ?>
                                            <span><strong>Type: </strong>Free Event</span>
                                        <?php } ?>

                                    </div>
                                </div>     
                            </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                        <?php } ?>
                        </div>
                     </div>
                  </div>
                  <!-- Middle Content Box End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archieve End =-=-=-=-=-=-= -->