
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v5.0&appId=2048106295447319&autoLogAppEvents=1"></script>

<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
<style>
  #fb-share-button {
    background: #3b5998;
    border-radius: 3px;
    font-weight: 600;
    padding: 5px 8px;
    display: inline-block;
    position: static;
  }

  #fb-share-button:hover {
    cursor: pointer;
    background: #213A6F
  }

  #fb-share-button svg {
    width: 18px;
    fill: white;
    vertical-align: middle;
    border-radius: 2px
  }

  #fb-share-button span {
    vertical-align: middle;
    color: white;
    font-size: 14px;
    padding: 0 3px
  }
  .rate {
    float: left;
    height: 46px;
    padding: 0 10px;
  }
  .rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
  }
  .rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
  }
  .rate:not(:checked) > label:before {
    content: '★ ';
  }
  .rate > input:checked ~ label {
    color: #ffc700;    
  }
  .rate:not(:checked) > label:hover,
  .rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
  }
  .rate > input:checked + label:hover,
  .rate > input:checked + label:hover ~ label,
  .rate > input:checked ~ label:hover,
  .rate > input:checked ~ label:hover ~ label,
  .rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
  }
</style>

<div class="page-header-area-2 gray">
 <div class="container">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="small-breadcrumb">
         <div class=" breadcrumb-link">
           <ul>
             <li><a href="<?php echo base_url();?>">Home Page</a></li>
             <li><a class="active" href="#">Event Details</a></li>
           </ul>
         </div>
         <div class="header-page">
           <h1>Event Details</h1>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>

  <div class="modal fade" id="created" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <p>Booking Created</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

  <?php if ($this->session->flashdata('success')) { ?>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                        <script>
$( document ).ready(function() {
    $('#created').modal('show')  
});
</script>
<?php } ?>

 <div class="modal fade" id="book" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Provide the details to confirm your booking</h4>
        </div>
        <div class="modal-body">
            <?php echo form_open_multipart('EventDetails/freeBooking', array('id' => 'freebook', 'class'=> 'submit-form')) ?>
                                             <div class="form-group">
                                                <label>Select Ticket Count</label>
                                               <input type="number" name="ticketcount">
                                            </div>
                                            <div class="form-group">
                                              <input name="id" type="hidden" value="<?php echo $events[0]->event_id?>">
                                          </div>

                                          <input type="submit" class="btn btn-theme ">
                                   <?php form_close() ?>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
 <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
 <section class="section-padding no-top gray">
   <!-- Main Container -->
   <div class="container">
     <!-- Row -->
     <div class="row">
       <!-- Middle Content Area -->
       <div class="col-md-12 col-xs-12 col-sm-12">
         <section class="search-result-item">
           <a class="image-link" href="#"><img class="image center-block" alt=""
             src="<?php echo base_url(); ?>uploads/<?php echo $events[0]->event_photo;?>"> </a>
             <div class="search-result-item-body">
               <div class="row">
                 <div class="col-md-5 col-sm-12 col-xs-12">
                   <h4 class="search-result-item-heading"><a
                     href="#"><?php echo $events[0]->event_name;?></a></h4>
                                     <!-- <p class="info">
                                    <span><a href="profile.html"><i class="fa fa-user "></i>Profile </a></span>
                                    <span><a href="javascript:void(0)"><i class="fa fa-edit"></i>Edit Profile </a></span>
                                 </p>
                                 <p class="description">You last logged in at: 14-01-2017 6:40 AM [ USA time (GMT + 6:00hrs)</p> -->
                                 <span class="label label-warning">Verified</span>
                                 <span class="label label-success">Category</span>
                                 <br>
                                 <?php if(!empty($this->session->userdata('id'))){
                                  if(count((array)$ratinguser) > 0){ ?>
                                   <div class="rate">
                                    <h4 style="margin-top: 6px">Your Rating: <?php echo strval($ratinguser[0]->rating) ?> </h4>
                                  </div>
                                  <?php
                                } else{
                                  ?>


                                  <?php echo form_open_multipart('EventDetails/addRating/'.$events[0]->event_id, array('id' => 'myForm')) ?>
                                  <div class="rate">
                                    <input type="radio" id="star5" name="rate" value="5" onclick="document.getElementById('myForm').submit();"/>
                                    <label for="star5" title="text" >5 stars</label>
                                    <input type="radio" id="star4" name="rate"  onclick="document.getElementById('myForm').submit();" value="4" />
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" onclick="document.getElementById('myForm').submit();" name="rate" value="3" />
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" onclick="document.getElementById('myForm').submit();" name="rate" value="2" />
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" onclick="document.getElementById('myForm').submit();" name="rate" value="1" />
                                    <label for="star1" title="text">1 star</label>
                                  </div>

                                  <?php form_close() ?>

                                <?php }} ?>
                              </div>

                              <div class="col-md-7 col-sm-12 col-xs-12">
                               <div class="row ad-history">
                                 <div class="col-md-4 col-sm-4 col-xs-12">
                                   <div class="user-stats">
                                     <h2><?php echo $bookings ?></h2>
                                     <small>Bookings</small>
                                   </div>
                                 </div>
                                 <div class="col-md-4 col-sm-4 col-xs-12">
                                   <div class="user-stats">
                                     <h2><?php echo $ratings ?></h2>
                                     <small>Rating</small>
                                   </div>
                                 </div>
                                 <div class="col-md-4 col-sm-4 col-xs-12">
                                   <div class="user-stats">
                                     <h2><?php echo $reviews ?></h2>
                                     <small>Reviews</small>
                                   </div>
                                 </div>

                               </div>
                             </div>

                           </div>
                           <?php if($events[0]->is_pricing== "1"){?>
                             <a href="<?php echo base_url();?>index.php/EventDetails/booknow/<?php echo $events[0]->event_id?>">
                               <div id="fb-share-button" style="background-color:red">
                              <span>Book Now</span>
                            </div>
                             </a>
                             
                          <?php } else{ ?>
                             <a data-target="#book" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#book">
                               <div id="fb-share-button" style="background-color:red">
                              <span>Book Now</span>
                            </div>
                             </a>
                          <?php } ?>
                          <a href="https://www.facebook.com/sharer.php?u=<?php echo base_url('index.php/EventDetails/index/'.$events[0]->event_id)?>">
                              <div id="fb-share-button">
                                <svg viewBox="0 0 12 12" preserveAspectRatio="xMidYMid meet">
                                  <path class="svg-icon-path" d="M9.1,0.1V2H8C7.6,2,7.3,2.1,7.1,2.3C7,2.4,6.9,2.7,6.9,3v1.4H9L8.8,6.5H6.9V12H4.7V6.5H2.9V4.4h1.8V2.8 c0-0.9,0.3-1.6,0.7-2.1C6,0.2,6.6,0,7.5,0C8.2,0,8.7,0,9.1,0.1z"></path>
                                </svg>
                                <span>Share</span>
                              </div>
                            </a>
                          <a href="https://twitter.com/share?url=<?php echo base_url('index.php/EventDetails/index/'.$events[0]->event_id)?>">

                            <div id="fb-share-button" style="background-color:#1da1f2">
                              <i class="fa fa-twitter" style="font-size:20px;color:#fff; margin-top:2px"></i>
                              <span>Share</span>
                            </div>
                          </a>

                        </section>

                      </div>
                      <!-- Middle Content Area  End -->
                    </div>

                    <div class="row margin-top-40">
                      <div class="main-content-area clearfix">
                       <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
                       <section class="section-padding no-top gray">
                         <!-- Main Container -->
                         <div class="container">
                           <!-- Row -->
                           <div class="row">
                             <!-- Middle Content Area -->
                             <div class="col-md-12 col-xs-12 col-sm-12">
                               <section class="search-result-item">
                                 <img src="<?php echo base_url();?>uploads/<?php echo $adrec[0]->ad_img?>" height="200" width="100%" s/>

                               </section>

                             </div>
                             <!-- Middle Content Area  End -->
                           </div>
                         </div>
                         <!-- Row End -->
                         <div class="row margin-top-40">
                           <!-- Middle Content Area -->
                           <div class="col-md-12 col-xs-12 col-sm-12">
                             <!-- Row -->
                             <div class="profile-section margin-bottom-20">
                               <div class="profile-tabs">
                                 <ul class="nav nav-justified nav-tabs">
                                   <li class="active"><a href="#profile" data-toggle="tab">Details</a></li>
                                   <!-- <li><a href="#edit" data-toggle="tab">Contact</a></li> -->
                                   <li><a href="#payment" data-toggle="tab">Pricing</a></li>
                                   <!-- <li><a href="#settings" data-toggle="tab">Book now </a></li> -->
                                   <li><a href="#reviews" data-toggle="tab">Review</a></li>

                                 </ul>
                                 <div class="tab-content">
                                   <div class="profile-edit tab-pane fade in active" id="profile">
                                     <h2 class="heading-md">Event Details:</h2>
                                     <!-- <p>Below are the name and email addresses on file for your account.</p> -->
                                     <br>
                                     <dl class="dl-horizontal">
                                       <dt><strong>Event Name </strong></dt>
                                       <dd>
                                         <?php echo $events[0]->event_name;?>
                                       </dd>
                                       <dt><strong>Description</strong></dt>
                                       <dd>
                                         <?php echo $events[0]->event_description;?>
                                       </dd>
                                       <dt><strong>Date </strong></dt>
                                       <dd>
                                         <?php echo $events[0]->event_date;?>
                                       </dd>
                                       <dt><strong>Time </strong></dt>
                                       <dd>
                                         <?php echo $events[0]->event_time;?>
                                       </dd>
                                       <dt><strong>Venue </strong></dt>     
                                       <dd>
                                         <?php echo $events[0]->event_chiefGuest;?>
                                       </dd>
                                       <dt><strong>Country </strong></dt>
                                       <dd>
                                         <?php echo $events[0]->country_name;?>
                                       </dd>
                                       <dt><strong>State </strong></dt>
                                       <dd>
                                         <?php echo $events[0]->state_name;?>
                                       </dd>
                                       <dt><strong>City </strong></dt>
                                       <dd>
                                         <?php echo $events[0]->city_name;?>
                                       </dd>
                                     </dl>

                                   </div>
                                 <!-- <div class="profile-edit tab-pane fade" id="edit">
                                 <h2 class="heading-md">Manage your Security Settings</h2>
                                 <p>Manage Your Account</p>
                                 <div class="clearfix"></div>
                                

                               </div> -->
                               <div class="profile-edit tab-pane fade" id="payment">
                                 <h2 class="heading-md">Ticket Classes & Amount</h2>
                                 <br>
                                 <div class="row">
                                   <div class="col-sm-12 col-md-6 margin-bottom-20">
                                     <label>Available classes for the event: &nbsp;
                                       <?php echo $events[0]->event_price;?></label>
                                     </div>
                                   </div>
                                   <br>
                                   <form action="#" id="sky-form" class="sky-form" novalidate>
                                     <?php $i = 1; foreach($prices as $price){ ?>
                                       <dl class="dl-horizontal">

                                         <dt><strong>Ticket Class:</strong></dt>
                                         <dd>
                                           <?php echo $price->class_name;?>
                                         </dd>
                                         <dt><strong>Ticket Price:</strong></dt>
                                         <dd>
                                           INR. <?php echo $price->class_price;?>
                                         </dd>
                                       </dl>
                                       <?php $i = $i +1;} ?>
                                     </form>
                                   </div>

                                   <div class="profile-edit tab-pane fade" id="reviews">
                                     <h2 class="heading-md">Your Review</h2>
                                     <p>Write your comments on <?php echo $events[0]->event_name ?></p>
                                     <br>
                                     <?php echo form_open_multipart('EventDetails/review/'.$events[0]->event_id) ?>
                                     <div class="skin-minimal">
                                      <textarea  cols="100" name="review" placeholder="   Your review... "rows="5"></textarea>
                                    </div>
                                    <div class="button-group margin-top-20">
                                     <button type="submit" class="btn btn-sm btn-theme">Save Changes</button>
                                   </div>
                                   <?php form_close() ?>
                                 </div>


                               </div>
                             </div>
                           </div>

                           <!-- Row End -->
                         </div>

                         <!-- Middle Content Area  End -->
                       </div>

                     </div>
                     <!-- Main Container End -->
                   </section>
                   <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

                   <script type="text/javascript">
                    function submitForm(){
                      var form = document.getElementById('event');
                      form.submit();


                    }
                  </script>
