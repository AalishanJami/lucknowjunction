<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
<div class="page-header-area-2 gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="small-breadcrumb">
                    <div class="breadcrumb-link">
                        <ul>
                             <li><a href="<?php echo base_url();?>">Home Page</a></li>
                            <li><a class="active" href="#">Become Event</a></li>
                        </ul>
                    </div>
                    <div class="header-page">
                        <h1>Enter Ticket Prices for your Event</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
    <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
    <section class="section-padding no-top gray ">
        <!-- Main Container -->
        <div class="container">
            <!-- Row -->

            <div class="row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <!-- end post-padding -->
                    <div class="post-ad-form postdetails">
                        <?php echo form_open_multipart('BecomeEvent/addClasses', array('id' => 'event', 'class'=> 'submit-form')); ?>

                        <?php for($i= 0; $i < $event[0]->event_price; $i++){ ?>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <label class="control-label">Ticket Class Name # <?php echo $i + 1 ?><span
                                        style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="name[<?php echo $i?>]"
                                    placeholder="e.g. Standard" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <label class="control-label">Ticket Class Price # <?php echo $i + 1 ?><span
                                        style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="price[<?php echo $i?>]"
                                    placeholder="e.g. 1000" required>
                            </div>
                        </div>
                        <?php } ?>

                        <input type="hidden" name="total" value="<?php echo $event[0]->event_price;?>">
                        <input type="hidden" name="event_id" value="<?php echo $event[0]->event_id;?>">


                        <input type="submit" class="btn btn-theme ">
                        <?php form_close() ?>
                    </div>
                    <!-- end post-ad-form-->
                </div>
                <!-- end col -->

                <!-- Middle Content Area  End -->
                <!-- end col -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Main Container End -->
    </section>
    <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->