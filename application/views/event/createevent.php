<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Create Event</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Publish Your Event</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top gray ">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                     <!-- end post-padding -->
                     <div class="post-ad-form postdetails">
                        <form  class="submit-form">
                           <!-- Title  -->
                           <div class="row">
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 <label class="control-label">Event Name <span style="color: red;">*</span></label>
                                 <input type="text" class="form-control" placeholder="Enter the name of your event" required>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <label class="control-label">Start Date <span style="color: red;">*</span></label>
                                 <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="Enter start date of event" required>
                              </div>
                           <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <label class="control-label">Time</label>
                                 <input type="text" min="09:00" max="18:00" required class="form-control" onfocus="(this.type='time')" placeholder="Enter end date of event" value="12:00 AM">
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <label class="control-label">End Date (optional)</label>
                                 <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="Enter end date of event" >
                              </div>
                           <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <label class="control-label">Time</label>
                                 <input type="text" min="09:00" max="18:00" required class="form-control" onfocus="(this.type='time')" placeholder="Enter end date of event" value="12:00 AM">
                              </div>
                           </div>
                              <!-- Price  -->
                              <div class="row">
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 <label class="control-label">Location Name <span style="color: red;">*</span></label>
                                 <input type="text" class="form-control" placeholder="Start typing location name for suggestions" required>
                              </div>
                           </div>
                           <!-- end row -->
                              <div class="row">
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 <label class="control-label">Event Description <span style="color: red;">*</span></label>
                                 <input type="text" class="form-control" style="height: 200px;" required>
                              </div>
                           </div>
                              
                           <button class="btn btn-theme pull-right">Create</button>
                        </form>
                     </div>
                     <!-- end post-ad-form-->
                  </div>
                  <!-- end col -->
                  
                  <!-- Middle Content Area  End --><!-- end col -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->