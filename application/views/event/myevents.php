<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
<div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">My Events</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>My Events</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
         <!-- =-=-=-=-=-=-= Ads Archieve =-=-=-=-=-=-= --> 
         <section class="custom-padding">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="clearfix"></div>
                  <!-- Heading Area -->
                  <div class="heading-panel">
                     <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h1>My<span class="heading-color"> Events</span></h1>
                        <!-- Short Description -->
                        </div>
                  </div>
                  <!-- Middle Content Box -->
         
                  <div class="row">
                     <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="posts-masonry">
                        <?php foreach($all as $item){ ?>
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="category-grid-box" style="height: 300; width:200" height="300" width="200">
                                 <!-- Ad Img -->
                                 <div class="category-grid-img">
                                    <img height="200" width="200" alt="" src="<?php echo base_url(); ?>uploads/<?php echo $item->event_photo; ?>">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <div class="user-preview">
                                       <a href="<?php echo base_url('index.php/eventside/Dashboard/index/'.$item->event_id) ?>"> <img src="<?php echo base_url(); ?>uploads/<?php echo $item->event_photo; ?>" class="avatar avatar-small" alt=""> </a>
                                    </div>
                                    
                                 </div>
                                 <!-- Ad Img End -->
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="<?php echo base_url('index.php/eventside/Dashboard/index/'.$item->event_id) ?>"><?php echo  $out = strlen($item->event_name) > 50 ? substr($item->event_name,0,50)."..." : $item->event_name; ?></a></h3>
                                    <a href="<?php echo base_url('index.php/MyEvents/delete/'.$item->event_id) ?>">
                                    <button class="btn btn-default">Delete</button>
                                 </a>
                                 <a href="<?php echo base_url('index.php/MyEvents/edit/'.$item->event_id) ?>">
                                    <button class="btn btn-default">Edit</button>
                                 </a>
                                 </div>
                                 
                              </div>
                              <!-- Ad Box End -->
                           </div>
                        <?php } ?>
                        </div>
                     </div>
                  </div>
                  <!-- Middle Content Box End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archieve End =-=-=-=-=-=-= -->