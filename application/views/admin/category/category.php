<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="font-size: 24px; font-weight: bold;">Category Management</h2>
            <?php if ($this->session->flashdata('success')) { ?>
    <h1 style="font-size: 15px; color:green;float: right;" >
        <?php echo $this->session->flashdata('success'); ?>
    </h1>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <h1 style="font-size: 15px; color:red;float: right;">
        <?php echo $this->session->flashdata('error'); ?>
    </h1>
<?php } ?>
        </div>
        <!-- Input --><br>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <h2 class="card-inside-title" style="font-size: 20px;">Category</h2><br>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                 <?php echo form_open_multipart('admin/Category/add') ?>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control"
                                                placeholder="Enter Category" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                          <textarea name="desc" cols="40" rows="5"  class="form-control"placeholder="Enter Description"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-line">
                                           <input type="file" id="userfile" name="userfile"  class="form-control">
                                        </div>
                                    </div>

                                    <div style="float: right;">
                                        <input type="submit" value="Create"  name="save" 
                                            class="btn btn-danger"
                                            style="border-radius: 10px; font-size: 18px; font-weight: bold;">
                                    </div>
                                </form>
                                <?php form_close() ?>
                            </div>
                        </div>
                        <!--#END# Switch Button -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content" style="margin-top: -30px;">
    <div class="container-fluid">
        <!-- Basic Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 style="font-weight: bold; font-size: 24px;">
                            Category List
                        </h2>
                    </div>
                    <div class="body table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Categories</th>
                                    <th>Status</th>
                                    <th>Change Status</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                                <?php 
                                $i = 1;
                                foreach($items as $item){?>
                                <form method="post" action="<?php echo base_url('index.php/admin/Category/update/'.$item->category_id) ?>">
                                <tr>
                                    <th scope="row"><?php echo strval($i); ?></th>
                                    <td><span id='name[<?php echo strval($item->category_id)?>]' ><?php echo $item->category_name;?></span>
                                    <input type="hidden" name="edit" id="edit_text[<?php echo strval($item->category_id)?>]" placeholder="Enter Category" 
                                    style="border-radius:5px; border:1px solid grey; height:30px; padding:8px;margin-left:-70px;"
                                    />
                                    <input id="edit_submit[<?php echo strval($item->category_id)?>]"class="btn btn-danger" type="hidden"  />
                                    </td>
                                    <td><?php if($item->category_status == "1"){
                                        echo "Enabled";
                                    } else{
                                        echo "Disabled";
                                    } ?></td>
                                    <td><a
                                            href="<?php echo base_url('index.php/admin/Category/active/'.$item->category_id) ?>"><img
                                                src="<?php echo base_url(); ?>admin/images/tick.jpg"
                                                height="15"></a><strong style="letter-spacing: 10px;"> |</strong><a
                                            href="<?php echo base_url('index.php/admin/Category/deactive/'.$item->category_id) ?>"><img
                                                src="<?php echo base_url(); ?>admin/images/cross.jpg" height="13"></a>
                                    </td>
                                    <td>
                                        <a onclick="display(<?php echo strval($item->category_id)?>)" ><span
                                                style="font-weight: bold; color: #9c27b0; font-size: 16px;">Edit</span></a>
                                        <span style="font-weight: bold; margin-left: 10px;">|</span>
                                        <a
                                            href="<?php echo base_url('index.php/admin/Category/delete/'.$item->category_id) ?>"><span
                                                style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;">Delete</span></a>
                                    </td>
                                </tr>
                                </form>
                                <?php $i = $i + 1; 
                            } ?>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Table -->

    </div>
</section>

<script>
function display(id) {
  var edit_text = document.getElementById("edit_text["+id+"]");
  edit_text.type = "text";
  var edit_button = document.getElementById("edit_submit["+id+"]");
  edit_button.type = "submit";      
  var text = document.getElementById("name["+id+"]");
  text.style.visibility = "hidden";     
  console.log(id)
  
  
}
function hide() {
  var edit_text = document.getElementById("edit_text");
  edit_text.type = "hidden";
  var edit_button = document.getElementById("edit_submit");
  edit_button.type = "hidden";      
  var text = document.getElementById("name");
  text.style.visibility = "visible"; 
      
}
</script>