<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="font-size: 24px; font-weight: bold;">User Feedback Management</h2>
        </div><br><br>
       
        <div class="row clearfix">
        <?php foreach($items as $item){ ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="card profile-card">
                    <div class="profile-header">&nbsp;</div>
                    <div class="profile-body">
                        <div class="image-area">
                            <img src="<?php echo base_url(); ?>admin/images/user-lg.jpg"
                                alt="AdminBSB - Profile Image" />
                        </div>
                        <div class="content-area">
                            <h3><?php echo $item->user_name; ?></h3>
                            <p><?php echo $item->user_email; ?></p>
                            <p><?php echo $item->user_contact; ?></p>
                        </div>
                    </div>
                    <div class="profile-footer">
                        <ul>
                            <p><span style="font-weight: bold;">Review</span> &nbsp&nbsp"<?php echo $item->feedback_msg; ?>"</p><br>
                            <a href="<?php echo base_url('index.php/admin/UserFeedbackManagement/reply/'.$item->feedback_id) ?>"><span
                                    style="font-weight: bold; color: #9c27b0; font-size: 16px;">Reply</span></a>
                            <span style="font-weight: bold; margin-left: 10px;">|</span>
                            <a href="<?php echo base_url('index.php/admin/UserFeedbackManagement/delete/'.$item->feedback_id) ?>"><span
                                    style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;">Delete</span></a>
                        </ul>
                    </div>
                </div>
            </div>
           <?php } ?>

        </div>
    </div>
    </div>
    </div>

</section>