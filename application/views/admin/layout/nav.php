<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url(); ?>admin/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('admin_name') ?></div>
                    <div class="email"><?php echo $this->session->userdata('admin_username') ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo base_url('index.php/admin/Dashboard/logout') ?>"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/admin/Dashboard') ?>">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">add_location</i>
                            <span>Location Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="<?php echo base_url('index.php/admin/location/Country') ?>">Country</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/location/State') ?>">State</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/location/City') ?>">City</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">event</i>
                            <span>Event Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="<?php echo base_url('index.php/admin/eventmanagement/EventManagement') ?>">Event Profile</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/eventmanagement/EventRequest') ?>">Event Request</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/admin/Category') ?>">
                            <i class="material-icons">assignment</i>
                            <span>Category Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/admin/user/UserManagement') ?>">
                            <i class="material-icons">people</i>
                            <span>User Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/admin/user/UserReviews') ?>">
                            <i class="material-icons">rate_review</i>
                            <span>Review Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/admin/AdManagement') ?>">
                            <i class="material-icons">content_copy</i>
                            <span>Ad Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/admin/BookingManagement') ?>">
                            <i class="material-icons">book</i>
                            <span>Booking Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/admin/UserFeedbackManagement') ?>">
                            <i class="material-icons">feedback</i>
                            <span>User Feedback Management</span>
                        </a>
                    </li>
                    <!-- <li> 
                                <a href="<?php echo base_url('index.php/admin/Profile') ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profile</span>
                                </a>
                            </li>
                            -->
                     <li>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">settings</i>
                            <span>Settings</span>
                        </a>
                        <ul class="ml-menu">
                            <!-- <li>
                                <a href="<?php echo base_url('index.php/admin/settings/CGeneralsettings') ?>">
                                    <span>General Settings</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/settings/CCMS') ?>">
                                    <span>CMS</span>
                                </a>
                            </li> -->
                            <li>
                                <a href="<?php echo base_url('index.php/admin/settings/Changepass') ?>">
                                    <span>Change Password</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                   
                                  </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>