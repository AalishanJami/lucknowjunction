<section class="content" style="width: 100%;">
        <div class="container-fluid">
            <div class="row clearfix">    
                <div class="col-xs-12 col-sm-9">
                    <div class="card">
                        <div class="body">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" style="font-weight: bold; font-size: 24px;">Change Password</li><br><br><br>
                                </ul>
                                    <div role="tabpanel" class="tab-pane fade in" id="change_password_settings" style="margin-top: 20px;">
                                    <?php echo form_open_multipart('admin/settings/Changepass/change', array('class' => 'form-horizontal')); ?>
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label for="OldPassword" class="col-sm-3 control-label">Old Password</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="password" class="form-control" id="OldPassword" name="old" placeholder="Old Password" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="NewPassword" class="col-sm-3 control-label">New Password</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="password" class="form-control" id="NewPassword" name="new" placeholder="New Password" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <p id="demo" style="color:red"></p>
                                            <!-- <div class="form-group">
                                                <label for="NewPasswordConfirm" class="col-sm-3 control-label">New Password (Confirm)</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="password" class="form-control" id="NewPasswordConfirm" name="NewPasswordConfirm" placeholder="New Password (Confirm)" required>
                                                    </div>
                                                </div>
                                            </div> -->

                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <input type="submit" value="change" class="btn btn-danger">
                                                </div>
                                            </div>
                                       <?php form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <script>
  var url = window.location.href;
//   console.log(url);
//   var c = url.searchParams.get("auth");
//     console.log(c);
var captured = /auth=([^&]+)/.exec(url)[1]; // Value is in [1] ('384' in our case)
var result = captured ? console.log(captured) : 'myDefaultValue';

if (captured){
document.getElementById("demo").innerHTML = "Incorrect email or password, please try again!";
}
  </script>