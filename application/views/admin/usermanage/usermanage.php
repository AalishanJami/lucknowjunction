<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="font-size: 24px; font-weight: bold;">User Management</h2>
        </div><br><br>
        <div class="row clearfix">
          <?php foreach($items as $item){?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card profile-card">
                        <div class="profile-header">&nbsp;</div>
                        <div class="profile-body">
                            <div class="image-area">
                                <img src="<?php echo base_url(); ?>admin/images/user-lg.jpg"
                                    alt="AdminBSB - Profile Image" />
                            </div>
                            <div class="content-area">
                                <h3><?php echo $item->user_name;?></h3>
                                <p><?php echo $item->user_email;?></p>
                                <p><?php echo $item->user_contact;?></p>
                            </div>
                        </div>
                        <div class="profile-footer">
                            <ul>
                                <a href="<?php echo base_url('index.php/admin/user/UserManagement/loadedit/'.$item->user_id) ?>"><span
                                        style="font-weight: bold; color: #9c27b0; font-size: 16px;">Edit</span></a>
                                <span style="font-weight: bold; margin-left: 10px;">|</span>
                                <a href="<?php echo base_url('index.php/admin/user/UserManagement/delete/'.$item->user_id) ?>"><span
                                        style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;">Delete</span></a>
                                        <span style="font-weight: bold; margin-left: 10px;">|</span>
                                <span
                                        style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;"><?php if($item->is_Active == "1"){ echo "Enabled";}else{echo "Disabled";}?></span>
                                <div style="float: right;">
                                    <a href="<?php echo base_url('index.php/admin/user/UserManagement/active/'.$item->user_id) ?>"><img src="<?php echo base_url(); ?>admin/images/tick.jpg"
                                            height="15"></a><strong style="letter-spacing: 10px;"> |</strong><a
                                        href="<?php echo base_url('index.php/admin/user/UserManagement/deactive/'.$item->user_id) ?>"><img src="<?php echo base_url(); ?>admin/images/cross.jpg"
                                            height="13"></a></div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          <?php } ?>
        </div>
    </div>
    </div>