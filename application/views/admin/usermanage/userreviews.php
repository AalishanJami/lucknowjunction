<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="font-size: 24px; font-weight: bold;">Review Management</h2>
        </div><br><br>
        <div class="row clearfix">
        <?php foreach($items as $item){ ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="card profile-card">
                    <div class="profile-header">&nbsp;</div>
                    <div class="profile-body">
                        <div class="image-area">
                            <img src="<?php echo base_url(); ?>admin/images/user-lg.jpg"
                                alt="AdminBSB - Profile Image" />
                        </div>
                        <div class="content-area">
                            <h3>Event: <?php echo  $out = strlen($item->event_name) > 50 ? substr($item->event_name,0,50)."..." : $item->event_name ?></h3>
                            <p>Date: <?php echo $item->event_date ?></p>
                            <p><?php if($item->event_status == "1"){echo "Enabled";}else{echo "Disabled";}?></p>
                        </div>
                    </div>
                    <div class="profile-footer">
                        <ul>
                            <p><span style="font-weight: bold;">Review</span> &nbsp&nbsp"<?php echo $item->event_review;?>"</p><br>

                            <a href="<?php echo base_url('index.php/admin/user/UserReviews/delete/'.$item->review_id) ?>"><span
                                    style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;">Delete</span></a>
                                    <span style="font-weight: bold; margin-left: 10px;">|</span>
                                <span
                                        style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;"><?php if($item->isActive == "1"){echo "Review: Enabled";}else{echo "Review: Disabled";}?></span>
                            <div style="float: right;">
                            
                                <a href="<?php echo base_url('index.php/admin/user/UserReviews/active/'.$item->review_id) ?>"><img src="<?php echo base_url(); ?>admin/images/tick.jpg"
                                        height="15"></a><strong style="letter-spacing: 10px;"> |</strong><a
                                    href="<?php echo base_url('index.php/admin/user/UserReviews/deactive/'.$item->review_id) ?>"><img src="<?php echo base_url(); ?>admin/images/cross.jpg" height="13"></a>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } ?>
    </div>
    </div>
    </div>

</section>