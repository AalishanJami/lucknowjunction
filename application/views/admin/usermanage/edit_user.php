<section class="content" style="width: 100%;">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-9">
                    <div class="card">
                        <div class="body">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab" style="font-weight: bold; font-size: 24px;">Put Your Details</a></li>
                                </ul>

                                    <div role="tabpanel" class="tab-pane fade in" id="profile_settings" style="margin-top: 20px;">
                                        <form class="form-horizontal" method="post" action="<?php echo base_url('index.php/admin/user/UserManagement/update/'.$items[0]->user_id) ?>">
                                            <div class="form-group">
                                                <label for="NameSurname" value="name" class="col-sm-2 control-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="NameSurname" name="name" placeholder="Enter Name" value="<?php echo $items[0]->user_name?>" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Email</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="NameSurname" name="email" value="<?php echo $items[0]->user_email?>" placeholder="Enter Email" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Phone</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="NameSurname" name="phone" placeholder="Enter Phone Number" value="<?php echo $items[0]->user_contact?>" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Facebook link</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="NameSurname" name="fb" value="<?php echo $items[0]->user_fb?>" placeholder="Enter Facebook link" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Instagram link</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="NameSurname" name="insta" placeholder="Enter Instagram link" value="<?php echo $items[0]->user_insta?>" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-danger">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>