<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                      <h2 style="font-size: 24px; font-weight: bold;">Event Management</h2>
                       </div><br><br>
                                 <div class="row clearfix">
                       <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="card">
                             <?php
                              $i = 0;

                                  foreach($items as $event){ ?>

                             <div class="card profile-card">
                        
                                <div class="profile-header">&nbsp;</div>
                                   <div class="profile-body">
                                       <div class="image-area">
                                           <img src="<?php echo base_url(); ?>admin/images/user-lg.jpg" alt="AdminBSB - Profile Image" />
                                      </div>
                                       
                                       <div class="content-area">
                                           <h3><?php echo  $out = strlen($event->event_name) > 50 ? substr($event->event_name,0,50)."..." : $event->event_name;?></h3>
                                           <p style="color: red;font-weight: bold">Venue: <?php echo $event->event_chiefGuest;?></p>
                                           <p style="color: blue;font-weight: normal;">Location: <?php echo $event->city_name;?></p>
                                            <p style="color: green;font-weight: bold">Status: <?php if ($event->event_status == "1"){echo "enabled";} else{echo "disabled";}?></p> </p>
                                        </div>
                                    </div>
                        <div class="profile-footer">
                            <ul>
                                   <a href="<?php echo base_url('index.php/admin/EditForm/loadedit/'.$event->event_id) ?>"><span style="font-weight: bold; color: #9c27b0; font-size: 16px;">Edit</span></a>
                                 <span style="font-weight: bold; margin-left: 10px;">|</span>
                                <a href="<?php echo base_url('index.php/admin/EditForm//delete/'.$event->event_id) ?>"><span style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;">Delete</span></a>
                                <?php if($event->is_sold == "1"){ ?>
                                  <span style="font-weight: bold; margin-left: 10px;">|</span>
                                <a href="<?php echo base_url('index.php/admin/EditForm//unsold/'.$event->event_id) ?>"><span style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;">Mark Un-Sold</span></a>
                                <?php } else{ ?>
                                <span style="font-weight: bold; margin-left: 10px;">|</span>
                                <a href="<?php echo base_url('index.php/admin/EditForm//sold/'.$event->event_id) ?>"><span style="font-weight: bold; color: #9c27b0; font-size: 16px; margin-left: 10px;">Mark Sold</span></a>
                                <?php } ?>
                                 <div style="float: right;">
                                  <a href="<?php echo base_url('index.php/admin/eventmanagement/EventManagement/active/'.$event->event_id)?>"><img src="<?php echo base_url(); ?>admin/images/tick.jpg" height="15"></a><strong style="letter-spacing: 10px;"> |</strong><a href="<?php echo base_url('index.php/admin/eventmanagement/EventManagement/deactive/'.$event->event_id) ?>"><img src="<?php echo base_url(); ?>admin/images/cross.jpg" height="13"></a>
                          
                            </ul>
                        </div>

                                <?php 
                               $i = $i++;
                                  } ?>
                                </div>
                                         </div>
                               </div>
                           </div>
                      </div>
                   </div>       
                </div>     
              </div>
              </div>
            </div>
        </div>

</section>

    