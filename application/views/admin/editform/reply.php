<section class="content" style="width: 100%;">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-9">
                    <div class="card">
                        <div class="body">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab" style="font-weight: bold; font-size: 24px;">Feedback Reply to <?php echo $items[0]->user_name ?></a></li>
                                </ul>

                                    <div role="tabpanel" class="tab-pane fade in" id="profile_settings" style="margin-top: 20px;">
                                        
                                        <form class="form-horizontal" method="post" action="<?php echo base_url('index.php/admin/UserFeedbackManagement/add') ?>">
                                            <div class="form-group">
                                             <label for="eventname" class="col-sm-2 control-label">Reply</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <textarea  class="form-control"  name="reply" 
                                                        placeholder="Write your reply ..." rows="4" required></textarea>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="feedbackid" value="<?php echo $items[0]->feedback_id ?>" />
                                                <input type="hidden" name="user_id" value="<?php echo $items[0]->user_id ?>" />
                                            </div><br>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                   <button type="submit" class="btn btn-danger">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                        
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>