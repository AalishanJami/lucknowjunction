<section class="content" style="width: 100%;">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-9">
                    <div class="card">
                        <div class="body">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab" style="font-weight: bold; font-size: 24px;">Event Details</a></li>
                                </ul>

                                    <div role="tabpanel" class="tab-pane fade in" id="profile_settings" style="margin-top: 20px;">
                                        
                                        <form class="form-horizontal" method="post" action="<?php echo base_url('index.php/admin/EditForm/update/'.$items[0]->event_id) ?>">
                                            <div class="form-group">
                                             <label for="eventname" class="col-sm-2 control-label">Event Name</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="eventname" name="eventname" 
                                                        value="<?php echo $items[0]->event_name?>"
                                                        placeholder="Enter Your Name" required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="eventlocation" class="col-sm-2 control-label">Location</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="eventlocation" placeholder="Enter location" 
                                                         value="<?php echo $items[0]->city_name?>"
                                                        required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="chiefguest" class="col-sm-2 control-label">Venue</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="chiefguest" placeholder="Enter ChiefGuest"
                                                        value="<?php echo $items[0]->event_chiefGuest?>"
                                                         required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="date" class="col-sm-2 control-label">Date</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="date" class="form-control" id="date" 

                                                        name="date" placeholder="Enter date"
                                                        value="<?php echo $items[0]->event_date?>"
                                                         required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="time" class="col-sm-2 control-label">Time</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="time" class="form-control" id="Email" name="time" placeholder="Enter time" 
                                                        value="<?php echo $items[0]->event_time?>"
                                                        required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <label for="description" class="col-sm-2 control-label">Description</label>

                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="InputSkills" name="description" placeholder="Enter description"
                                                        value="<?php echo $items[0]->event_description?>"
                                                         required>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                   <button type="submit" class="btn btn-danger">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                        
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>