<style>
    .date {
        width: 70px; height: 80px; 
        background: #fcfcfc; 
        background: linear-gradient(top, #fcfcfc 0%,#dad8d8 100%); 
        background: -moz-linear-gradient(top, #fcfcfc 0%, #dad8d8 100%); 
        background: -webkit-linear-gradient(top, #fcfcfc 0%,#dad8d8 100%); 
        border: 1px solid #d2d2d2;
        border-radius: 10px;
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        box-shadow: 0px 0px 15px rgba(0,0,0,0.1);
        -moz-box-shadow: 0px 0px 15px rgba(0,0,0,0.1);
        -webkit-box-shadow: 0px 0px 15px rgba(0,0,0,0.1);

    }
    .date p {
        font-family: Helvetica, sans-serif; 
        font-size: January0px; text-align: center; color: #9e9e9e; 
    }
    .date p span {
        background: #d10000; 
        background: linear-gradient(top, #d10000 0%, #7a0909 100%);
        background: -moz-linear-gradient(top, #d10000 0%, #7a0909 100%);
        background: -webkit-linear-gradient(top, #d10000 0%, #7a0909 100%);
        font-size: 23px; font-weight: bold; color: #fff; text-transform: uppercase;     
        display: block;
        border-top: 3px solid #a13838;
        border-radius: 0 0 10px 10px;
        -moz-border-radius: 0 0 10px 10px;
        -webkit-border-radius: 0 0 10px 10px;
        padding: 6px 0 6px 0;
    }
@media (max-width: 575px) {
   .headerf {
    height: 850px;
  }
}
@media (max-width: 991px) {
  .headerf {
    height: 850px;
  }
}
@media (max-width: 767px) {
   .headerf {
    height: 850px;
  }
}
@media (max-width: 575px) {
   .searcha {
    margin-top: -600px;
  }
}
@media (max-width: 991px) {
  .searcha {
    margin-top: -600px;
  }
}
@media (max-width: 767px) {
   .searcha {
    margin-top: -600px;
  }
}
@media (max-width: 575px) {
   .font_sizea {
    font-size: 22px;
    font-weight: bold;
  }
}
@media (max-width: 991px) {
  .font_sizea {
    font-size: 22px;
    font-weight: bold;
  }
}
@media (max-width: 767px) {
   .font_sizea {
    font-size: 22px;
    font-weight: bold;
  }
}
@media (max-width: 575px) {
   .boxa {
    margin-top: -25px;
  }
}
@media (max-width: 991px) {
  .boxa {
    margin-top: -25px;
  }
}
@media (max-width: 767px) {
   .boxa {
    margin-top: -25px;
  }
}
@media (max-width: 575px) {
   .bottomtotopa {
    height: 350px;
  }
}
@media (max-width: 991px) {
  .bottomtotopa {
    height: 350px;
  }
}
@media (max-width: 767px) {
   .bottomtotopa {
    height: 350px;
  }
}
@media (max-width: 575px) {
   .searchbox {
    height: 70px;
    margin-top: -90px;
  }
}
@media (max-width: 991px) {
  .searchbox {
    height: 70px;
    margin-top: -90px;
  }
}
@media (max-width: 767px) {
   .searchbox {
    height: 70px;
    margin-top: -90px;
  }
}
@media (max-width: 575px) {
   .ab {
    margin-top: 50px;
  }
}
@media (max-width: 991px) {
  .ab {
    margin-top: 50px;
  }
}
@media (max-width: 767px) {
   .ab {
    margin-top: 50px;
  }
}
@media (max-width: 575px) {
   .slider {
    overflow: hidden;
    background: white;
  }
}
@media (max-width: 991px) {
  .slider {
    overflow: hidden;
    background: white;
  }
}
@media (max-width: 767px) {
   .slider {
    overflow: hidden;
    background: white;
  }
}
@media (max-width: 575px) {
   .slider1 {
    margin-top: -105px;  }
}
@media (max-width: 991px) {
  .slider1 {
   margin-top: -105px;
  }
}
@media (max-width: 767px) {
   .slider1 {
    margin-top: -105px;  }
}
@media (max-width: 575px) {
   .slider2 {
    margin-top: -130px;  }
}
@media (max-width: 991px) {
  .slider2 {
   margin-top: -130px;
  }
}
@media (max-width: 767px) {
   .slider2 {
    margin-top: -130px;  }
}
@media (max-width: 575px) {
   .text:before {
   content: "Discover Events Happening in Your Location";
   font-size: 23px;
   font-weight: bold;
   color: black;
}
}
@media (max-width: 991px) {
  .text:before {
   content: "Discover Events Happening in Your Location";
   font-size: 23px;
   font-weight: bold;
   color: black;
}
}
@media (max-width: 767px) {
   .text:before {
   content: "Discover Events Happening in Your Location";
   font-size: 23px;
   font-weight: bold;
   color: black;
}
}
</style>
<center><ul>
   <li class="text"></li>
</ul></center>
<div class="background-rotator bottomtotopa">
    <!-- slider start-->
    <div class="owl-carousel owl-theme background-rotator-slider headerf" style="margin-top: -80px;">
        <!-- Slide -->
        <div class="item linear-overlay slider"><img src="<?php echo base_url(); ?>assets/images/slider/1.JPG" alt=""></div>
        <!-- Slide -->
        <div class="item linear-overlay slider"><img src="<?php echo base_url(); ?>assets/images/slider/2.JPG" alt=""></div>
        <!-- Slide -->
        <div class="item linear-overlay slider"><img src="<?php echo base_url(); ?>assets/images/slider/3.JPG" alt=""></div>
    </div>


    <div class="search-section" style="margin-top: -220px;">
        <!-- Find search section -->
        <div class="container slider1">
            <div class="row">
                <div class="col-md-12 searcha">
                    <!-- Heading -->
                    <div class="content">
                        <div class="heading-caption">
                            <h2 style="font-weight: bold; color: white;" class="font_sizea">Discover Events Happening in Your Location</h2>
                            <p style="font-size: 17px; font-weight: bolder; margin-top: 20px;"><?php echo $eventcount; ?> Events &nbsp | &nbsp <?php echo $cities; ?> Locations &nbsp | &nbsp <?php echo $users; ?> People
                            Exploring Events every month</p>
                           
                        </div>
                        <div class="search-form boxa searchbox">
                            <?php echo form_open_multipart('Home/index', array('id' => 'search', 'class'=> 'submit-form')); ?>

                            <div class="row">

                                <div class="col-md-4 col-xs-12 col-sm-3 ab">
                                    <!-- Category -->
                                    <select name="city" form="search" id="city"  onchange="javascript: if(this.value != '0') this.form.submit(); else alert('Please select a city');"  class="model form-control">
                                        <option value="0">Select Location</option>

                                        <!--   <option value="90000" id="loc" selected><?php echo $cityname ?></option> -->
                                        <?php foreach ($city as $item){
                                            if($city_selected == $item->city_name){ ?>
                                              <option value="<?php echo $item->city_name; ?>" selected><?php echo $item->city_name; ?>
                                          </option>  
                                      <?php  }
                                      else { ?>
                                       <option value="<?php echo $item->city_name; ?>"><?php echo $item->city_name; ?>
                                   </option> 
                               <?php }
                               ?>
                           <?php } ?>
                       </select>
                                </div>

                                <!-- Input Field -->
                                <div class="col-md-4 col-xs-12 col-sm-3">
                                    <select name="category" id="category" form="search"   onchange="javascript: if(this.value != '0') this.form.submit(); else alert('Please select a category');"  class="model form-control">
                                        <option value="0" >All Events</option>
                                        <?php foreach ($category as $item){ 
                                         if($category_selected == $item->category_id){ ?>
                                             <option value="<?php echo $item->category_id; ?>" selected>
                                                <?php echo $item->category_name; ?></option>
                                            <?php  }
                                            else { ?>
                                              <option value="<?php echo $item->category_id; ?>">
                                                <?php echo $item->category_name; ?></option>
                                            <?php }
                                            ?>
                                        <?php } ?>
                                    </select>
                   </div>
                   <!--  <input type="hidden" name="cityname" id="inploc" value="" />
                   --> <!-- Input Field -->
                                <!-- <div class="col-md-3 col-xs-12 col-sm-3">
                                    <input type="hidden" name="name" class="form-control"
                                    placeholder="Enter event name" />
                                </div>
                                 --><!-- Search Button -->
                                 <div class="col-md-3 col-xs-12 col-sm-3">
                                    <button type="submit" class="btn btn-theme btn-block" style="text-transform: capitalize; font-weight: bold; font-size: 16px;">Explore <i class="fa fa-search"
                                        aria-hidden="true"></i></button>

                                    </div>
                                </div>
                                <?php form_close() ?>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- /.Find search section-->
    </div>


    <!-- =-=-=-=-=-=-= Background Rotator End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->

    <div class="main-content-area clearfix slider2">
        <!-- =-=-=-=-=-=-= Trending Ads =-=-=-=-=-=-= -->
        <section class="custom-padding white">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1><span class="heading-color">New Year </span>Events in <?php echo $cityname ?></h1>
                        </div>
                    </div>
                    <!-- Middle Content Box -->
                    <!-- Nav tabs -->


                    <div class="recent-tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#all" role="tab" data-toggle="tab"
                                aria-expanded="true">All</a></li>
                        <!-- <li role="presentation" class=""><a href="#usedcars" role="tab" data-toggle="tab"
                                aria-expanded="true">Today</a></li>
                        <li role="presentation"><a href="#newcars" role="tab" data-toggle="tab"
                                aria-expanded="true">Tomorrow</a></li>
                        <li role="presentation"><a href="#usedcars" role="tab" data-toggle="tab"
                            aria-expanded="true">This Weekend</a></li> -->
                        </ul>
                    </div>
                    
                    <!-- Recently Listed New Cars -->

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="all">

                          <?php foreach($eventsnewyear as $item){?>

                           <div class="col-md-4 col-xs-12 col-sm-6" style="">
                            <div class="category-grid-box">
                                <!-- Ad Img -->
                                <a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>">
                                    <div class="category-grid-img">
                                        <img height="200" width="200" alt=""
                                        src="<?php echo base_url(); ?>uploads/<?php echo $item->event_photo ?>">
                                        <!-- Ad Status -->
                                        <!-- User Review -->


                                    </div>
                                </a>
                                <!-- Ad Img End -->
                                <div class="short-description">

                                    <div class="row">
                                        <div class="col-xs-3">
                                           <div class="date">
                                            <p><?php echo array_map('trim', explode(' ', date('d', strtotime($item->event_date))))[0]; ?><span><?php echo array_map('trim', explode(' ', date('M', strtotime($item->event_date))))[0]; ?></p>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                         <h3><a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>"><?php echo  $out = strlen($item->event_name) > 30 ? substr($item->event_name,0,50)."..." : $item->event_name; ?></a></h3>
                                         <span><strong>Time: </strong><?php echo $item->event_time; ?></span>
                                         <br>
                                         <?php if($item->is_pricing == "1"){ ?>
                                             <span><strong>Prices from: <strong><a>INR. <?php echo $item->base_price; ?></a></span>
                                             <?php } else { ?>
                                                <span><strong>Type: </strong>Free Event</span>
                                            <?php } ?>

                                        </div>
                                    </div>     
                                </div>
                            </div>

                        </div>

                    <?php } ?>

                </div>
                <!-- Recently Listed Used Cars -->
                <!-- Main Container End -->
        </section>

    

           <section class="custom-padding white">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1><span class="heading-color">Popular </span>Events in <?php echo $cityname ?></h1>
                        </div>
                    </div>
                    <!-- Middle Content Box -->
                    <!-- Nav tabs -->


                    <div class="recent-tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#all" role="tab" data-toggle="tab"
                                aria-expanded="true">All</a></li>
                        <!-- <li role="presentation" class=""><a href="#usedcars" role="tab" data-toggle="tab"
                                aria-expanded="true">Today</a></li>
                        <li role="presentation"><a href="#newcars" role="tab" data-toggle="tab"
                                aria-expanded="true">Tomorrow</a></li>
                        <li role="presentation"><a href="#usedcars" role="tab" data-toggle="tab"
                            aria-expanded="true">This Weekend</a></li> -->
                        </ul>
                    </div>
                    
                    <!-- Recently Listed New Cars -->

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="all">

                          <?php foreach($events as $item){?>

                           <div class="col-md-4 col-xs-12 col-sm-6" style="">
                            <div class="category-grid-box">
                                <!-- Ad Img -->
                                <a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>">
                                    <div class="category-grid-img">
                                        <img height="200" width="200" alt=""
                                        src="<?php echo base_url(); ?>uploads/<?php echo $item->event_photo ?>">
                                        <!-- Ad Status -->
                                        <!-- User Review -->


                                    </div>
                                </a>
                                <!-- Ad Img End -->
                                <div class="short-description">

                                    <div class="row">
                                        <div class="col-xs-3">
                                           <div class="date">
                                            <p><?php echo array_map('trim', explode(' ', date('d', strtotime($item->event_date))))[0]; ?><span><?php echo array_map('trim', explode(' ', date('M', strtotime($item->event_date))))[0]; ?></p>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                         <h3><a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>"><?php echo  $out = strlen($item->event_name) > 30 ? substr($item->event_name,0,50)."..." : $item->event_name; ?></a></h3>
                                         <span><strong>Time: </strong><?php echo $item->event_time; ?></span>
                                         <br>
                                         <?php if($item->is_pricing == "1"){ ?>
                                             <span><strong>Prices from: <strong><a>INR. <?php echo $item->base_price; ?></a></span>
                                             <?php } else { ?>
                                                <span><strong>Type: </strong>Free Event</span>
                                            <?php } ?>

                                        </div>
                                    </div>     
                                </div>
                            </div>

                        </div>

                    <?php } ?>

                </div>
                <!-- Recently Listed Used Cars -->
                <!-- Main Container End -->
        </section>
            <!-- =-=-=-=-=-=-= Trending Ads End =-=-=-=-=-=-= -->
    <section class="custom-padding white">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1><span class="heading-color">F & B </span>Events in <?php echo $cityname ?></h1>
                        </div>
                    </div>
                    <!-- Middle Content Box -->
                    <!-- Nav tabs -->


                    <div class="recent-tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#all" role="tab" data-toggle="tab"
                                aria-expanded="true">All</a></li>
                        <!-- <li role="presentation" class=""><a href="#usedcars" role="tab" data-toggle="tab"
                                aria-expanded="true">Today</a></li>
                        <li role="presentation"><a href="#newcars" role="tab" data-toggle="tab"
                                aria-expanded="true">Tomorrow</a></li>
                        <li role="presentation"><a href="#usedcars" role="tab" data-toggle="tab"
                            aria-expanded="true">This Weekend</a></li> -->
                        </ul>
                    </div>
                    
                    <!-- Recently Listed New Cars -->

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="all">

                          <?php foreach($eventsfd as $item){?>

                           <div class="col-md-4 col-xs-12 col-sm-6" style="">
                            <div class="category-grid-box">
                                <!-- Ad Img -->
                                <a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>">
                                    <div class="category-grid-img">
                                        <img height="200" width="200" alt=""
                                        src="<?php echo base_url(); ?>uploads/<?php echo $item->event_photo ?>">
                                        <!-- Ad Status -->
                                        <!-- User Review -->


                                    </div>
                                </a>
                                <!-- Ad Img End -->
                                <div class="short-description">

                                    <div class="row">
                                        <div class="col-xs-3">
                                           <div class="date">
                                            <p><?php echo array_map('trim', explode(' ', date('d', strtotime($item->event_date))))[0]; ?><span><?php echo array_map('trim', explode(' ', date('M', strtotime($item->event_date))))[0]; ?></p>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                         <h3><a title="" href="<?php echo base_url('index.php/EventDetails/index/'.$item->event_id) ?>"><?php echo  $out = strlen($item->event_name) > 30 ? substr($item->event_name,0,50)."..." : $item->event_name; ?></a></h3>
                                         <span><strong>Time: </strong><?php echo $item->event_time; ?></span>
                                         <br>
                                         <?php if($item->is_pricing == "1"){ ?>
                                             <span><strong>Prices from: <strong><a>INR. <?php echo $item->base_price; ?></a></span>
                                             <?php } else { ?>
                                                <span><strong>Type: </strong>Free Event</span>
                                            <?php } ?>

                                        </div>
                                    </div>     
                                </div>
                            </div>

                        </div>

                    <?php } ?>

                </div>
                <!-- Recently Listed Used Cars -->
                <!-- Main Container End -->
        </section>
            <!-- =-=-=-=-=-=-= Clients  =-=-=-=-=-=-= -->
            <section class="custom-padding">
                <!-- Main Container -->
                <div class="container">
                    <!-- Row -->
                    <div class="row">
                        <div class="clearfix"></div>
                        <!-- Heading Area -->
                        <div class="heading-panel">
                            <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                                <!-- Main Title -->
                                <h1>Things to do in and around <span class="heading-color">  <?php echo $cityname ?></span></h1>
                                <!-- Short Description -->
                            </div>
                        </div>
                        <!-- Middle Content Box -->
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <div class="posts-masonry">
                                    <?php foreach($category as $item){ ?>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <!-- Ad Box -->
                                            <div class="category-grid-box" style="height: 280px">
                                                <!-- Ad Img -->
                                                <a href="<?php echo base_url('index.php/Events/index/category/'.$item->category_id) ?>">
                                                    <div class="category-grid-img">
                                                        <img height="200" width="200" alt=""
                                                        src="<?php echo base_url(); ?>uploads/<?php echo $item->category_img ?>">
                                                        <!-- Ad Status -->
                                                        <!-- User Review -->
                                                    </div>
                                                </a>
                                                <!-- Ad Img End -->
                                                <div class="short-description">
                                                    <!-- Ad Category -->
                                                    <!-- Ad Title -->
                                                    <!-- <h3><a title="" href="<?php echo base_url('index.php/EventDetails') ?>">Porsche 911 Carrera 2017 </a></h3> -->
                                                    <!-- Price -->
                                                    <a href="<?php echo base_url('index.php/Events/index/category/'.$item->category_id) ?>">
                                                        <div class="price"><?php echo $item->category_name; ?></div>
                                                    </a>
                                                </div>

                                            </div>
                                            <!-- Ad Box End -->
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!-- Middle Content Box End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Main Container End -->
            </section>



            <!-- =-=-=-=-=-=-= Clients  =-=-=-=-=-=-= -->
            <div class="happy-clients-area gray">
                <div class="container">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1>Explore Events<span class="heading-color"> across India</span></h1>
                        </div>
                    </div>
                    <div class="row clients-space">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="client-brand-list">

                               <div class="col-md-4 col-xs-12 col-sm-6">
                                  <div class="category-grid-box" style="height: 90px">
                                    <!-- Ad Img -->
                                    <a title="" href="<?php echo base_url('index.php/ComingSoon') ?>">
                                        <div class="category-grid-img">
                                            <img height="90" width="200" alt=""
                                            src="<?php echo base_url(); ?>assets/images/b.jfif">
                                            <!-- Ad Status -->
                                            <!-- User Review -->
                                            <h2 style="position: absolute; left: 80px; top: 12px; color: white; font-weight: bold; font-size: 35px;">Allahabad</h2>

                                        </div>
                                    </a>
                                    <!-- Ad Img End -->
                                    <div class="short-description">

                                        <div class="row">


                                            <center>
                                             <h3 style="font-size: 30px;"><a title="" href="<?php echo base_url('index.php/ComingSoon') ?>">Allahabad</a></h3>
                                         </center>


                                     </div>     
                                 </div>
                             </div>

                         </div>
                         <div class="col-md-4 col-xs-12 col-sm-6" style="">
                          <div class="category-grid-box" style="height: 90px">
                            <!-- Ad Img -->
                            <a title="" href="<?php echo base_url('index.php/ComingSoon') ?>">
                                <div class="category-grid-img">
                                    <img height="90" width="200" alt=""
                                    src="<?php echo base_url(); ?>assets/images/a.jpg">
                                    <!-- Ad Status -->
                                    <!-- User Review -->
                                    <h2 style="position: absolute; left: 100px; top: 12px; color: white; font-weight: bold; font-size: 36px;">Kanpur</h2>

                                </div>
                            </a>
                            <!-- Ad Img End -->
                            <div class="short-description">

                                <div class="row">
                                    <center>
                                     <h3 style="font-size: 30px;"><a title="" href="<?php echo base_url('index.php/ComingSoon') ?>">Kanpur</a></h3>
                                 </center>
                             </div>     
                         </div>
                     </div>

                 </div>
                 <div class="col-md-4 col-xs-12 col-sm-6" style="">
                  <div class="category-grid-box" style="height: 90px">
                    <!-- Ad Img -->
                    <a title="" href="<?php echo base_url('index.php/ComingSoon') ?>">
                        <div class="category-grid-img">
                            <img height="90" width="200" alt=""
                            src="<?php echo base_url(); ?>assets/images/slider/3.JPG">
                            <!-- Ad Status -->
                            <!-- User Review -->
                            <h2 style="position: absolute; left: 100px; top: 15px; color: white; font-weight: bold; font-size: 33px;">Varanasi</h2>

                        </div>
                    </a>
                    <!-- Ad Img End -->
                    <div class="short-description">

                        <div class="row">


                           <center>
                             <h3 style="font-size: 30px;"><a title="" href="<?php echo base_url('index.php/ComingSoon') ?>">Varanasi</a></h3>
                         </center>
                     </div>     
                 </div>
             </div>

         </div>

         <!-- Listing Ad Grid -->
         <div class="clearfix"></div>
         <div class="text-center">
                            <!-- <div class="load-more-btn">
                                <a href="#"><button class="btn btn-lg btn-theme" type="button"> View All Countries <i
                                            class="fa fa-refresh"></i> </button>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Clients  End =-=-=-=-=-=-= -->

<!-- <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAu8sC3fYgn_Vp2W3u-sGDkpXHDua6-Uac&libraries=places'></script>

<script type="text/javascript">
    window.onload = function() {
        var geocoder = new google.maps.Geocoder();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
        }
        function successFunction(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            codeLatLng(lat, lng)
        }
        function errorFunction() {
        }
        function initialize() {
            geocoder = new google.maps.Geocoder();
        }
        function codeLatLng(lat, lng) {
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': latlng}, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                var city = results[1].address_components[5].long_name;
                if (results[1]) {
                  for (var i = 0; i < results[0].address_components.length; i++) {
                    for (var b = 0; b < results[0].address_components[i].types.length; b++) {
                      if (results[0].address_components[i].types[b] == 'country') {
                        country = results[0].address_components[i];
                        break;
                    }
                    if (results[0].address_components[i].types[b] == 'administrative_area_level_1') {
                        state = results[0].address_components[i];
                        break;
                    }
                    if (results[0].address_components[i].types[b] == 'administrative_area_level_2') {
                        city = results[0].address_components[i];
                                // console.log(city);
                                break;
                            }
                        }
                    }
                    var d = new Date();
                    d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
                    var expires = 'expires=';
                    document.cookie =  'currentcity=' + city.long_name + "; " + 
                    expires;
                    console.log(city.long_name);}}});}
            if(getCookie("lat")===null){
              if (navigator.geolocation) {
                console.log("here on first line");
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
            console.log("entering show position");
            function showPosition(position) {
              setCookie("lat", position.coords.latitude, 1);
              console.log("going to reload");
              console.log(getCookie("lat"));
              window.setTimeout(function(){ window.location = window.location.href; },1000);
  // window.location.reload(true);
}
}
};

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
</script> -->
<!-- <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
<script type='text/javascript'>
$(function() {
    $('#category').change(function() {
        this.form.submit();
    });
    $('#city').change(function() {
        this.form.submit();
    });
});
</script> -->

<script type="text/javascript">
    function DoSubmit(sel)
    {
       if(sel.val()!='0') this.form.submit();
   }
</script>