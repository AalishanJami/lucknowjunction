<!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
         <footer class="footer-bg" style="background: #000000;">
            <!-- Footer Content -->
            <div class="footer-top">
               <div class="container">
                  <div class="row">
                     <div class="col-md-3  col-sm-6 col-xs-12">
                        <!-- Info Widget -->
                        <div class="widget">
                           <div class="logo"> <img alt="" src="<?php echo base_url(); ?>assets/images/lucknow.png" height="100"> </div>
                           <p style="text-align: justify;">Lucknow Junction is a digital platform that serves you with the convenience of arranging all the tickets, passes, bookings at single place. Whether it’s about booking some event in the city or about getting passes for your favorite restaurant. The platform is totally helpful to you.
</p>
                           
                        </div>
                        <!-- Info Widget Exit -->
                     </div>
                     <div class="col-md-2  col-sm-6 col-xs-12" style="margin-left: 80px;">
                        <!-- Follow Us -->
                        <div class="widget my-quicklinks">
                           <!-- <h5 style="font-size: 14px; font-weight: bold;">Most Search City</h5>
                           <ul >
                              <li><a href="#">About Us</a></li>
                              <li><a href="#">Faqs</a></li>
                              <li><a href="#">Packages</a></li>
                              <li><a href="#">Contact Us</a></li>
                           </ul> -->
                        </div>
                        <!-- Follow Us End -->
                     </div>
                     <div class="col-md-2  col-sm-6 col-xs-12">
                        <!-- Follow Us -->
                        <div class="widget my-quicklinks">
                           <h5 style="font-size: 14px; font-weight: bold;color:#ffffff">Our Cities</h5>
                           <ul >
                              <li><a href="#">Lucknow</a></li>
                              <li><a href="#">Allahabad</a></li>
                              <li><a href="#">Kanpur</a></li>
                           </ul>
                        </div>
                        <!-- Follow Us End -->
                     </div>
                     <div class="col-md-2  col-sm-6 col-xs-12">
                        <!-- Follow Us -->
                        <div class="widget my-quicklinks">
                           <!-- <h5 style="font-size: 14px; font-weight: bold;">Most Search Events</h5>
                           <ul >
                              <li><a href="#">About Us</a></li>
                              <li><a href="#">Faqs</a></li>
                              <li><a href="#">Packages</a></li>
                              <li><a href="#">Contact Us</a></li>
                           </ul> -->
                        </div>
                        <!-- Follow Us End -->
                     </div>
                     <div class="col-md-2  col-sm-6 col-xs-12">
                        <!-- Follow Us -->
                        <div class="widget my-quicklinks">
                           <h5 style="font-size: 14px; font-weight: bold; color: #ffffff">Quick Links</h5>
                           <ul >
                              <li><a href="<?php echo base_url('index.php/TermOfService') ?>">Terms of Service</a></li>
                              <li><a href="<?php echo base_url('index.php/Privacy') ?>">Privacy Policy</a></li>
                              <li><a href="<?php echo base_url('index.php/Refund') ?>">Refund & Cancel Policy</a></li>
                              <li><a href="<?php echo base_url('index.php/About') ?>">About Us</a></li>
                              <li><a href="<?php echo base_url('index.php/Contact') ?>">Contact Us</a></li>
                           </ul>
                        </div>
                        <!-- Follow Us End -->
                     </div>
                  </div>
               </div>
            </div>
           <center> © Copyright 2019. All Rights Reserved. <center>
         </footer>
         <!-- =-=-=-=-=-=-= FOOTER END =-=-=-=-=-=-= -->