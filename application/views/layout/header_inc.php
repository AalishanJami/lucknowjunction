<!DOCTYPE html>
<html lang="en">
   
<!-- Mirrored from templates.scriptsbundle.com/carspot/demos/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Nov 2019 08:20:08 GMT -->
<head>
      <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
      <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="skype_toolbar" content="skype_toolbar_parse_compatible"/>
        <meta name="author" content="https://www.lucknowjunction.com"/>
        <meta name="robots" content="all">
        <meta name="robots" content="index, follow"/>
        <meta name="YahooSeeker" content="index, follow"/>
        <meta name="msnbot" content="index, follow"/>
        <meta name="googlebot" content="index, follow"/>
        <meta name="allow-search" content="yes"/>
        <meta name="revisit-after" content="7 days"/>
        <meta name="distribution" content="Global"/>
        <meta name="rating" content="General"/>
        <meta name="copyright" content="https://www.lucknowjunction.com"/>
      <meta name="country" content="India (IN)"/>
      <meta name="subject" content="All Events in City"/>
      <meta name="language" content="English, Hindi">
      <meta property="og:site_name" content="All Events in City - Lucknow Junction"/>
        <meta property="og:type" content="Salon at Home"/>  
      <meta property="og:url" content="https://www.lucknowjunction.com/" />
      <meta property="og:site_name" content="lucknowjunction.com" />
        <meta property="article:author" content="https://www.facebook.com/LucknowJuntionIn"/>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/lucknowjunction.com\/","name":"Lucknow Junction - All Events in City | Discover Events Happening in Your City","potentialAction":{"@type":"SearchAction","target":"https:\/\/lucknowjunction.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<meta name="description" content="Explore All Events, Activities and Things to do in Lucknow. Find information of nearby upcoming events happening in your city, Discover parties, concerts, meets, shows, sports, club, reunion, Performance. Discover events in Lucknow"/><link rel="canonical" href="https://lucknowjunction.com/" />
<meta property="og:locale" content="en_US" /><meta property="og:type" content="website" />
<meta property="og:title" content="Lucknow Junction - All Events in City | Discover Events Happening in Your City" />
<meta property="og:description" content="Explore All Events, Activities and Things to do in Lucknow. Find information of nearby upcoming events happening in your city, Discover parties, concerts, meets, shows, sports, club, reunion, Performance. Discover events in Lucknow" />


      <title> <?php if(isset($events)){ echo $events[0]->event_name." | "; }?>Lucknow Junction - All Events in City | Discover Events Happening in Your City</title>
      <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
      <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.png" type="image/x-icon" />
      <!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
      <!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
      <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css" type="text/css">
      <!-- =-=-=-=-=-=-= Flat Icon =-=-=-=-=-=-= -->
      <link href="<?php echo base_url(); ?>assets/css/flaticon.css" rel="stylesheet">
      <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/et-line-fonts.css" type="text/css">
      <!-- =-=-=-=-=-=-= Menu Drop Down =-=-=-=-=-=-= -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/carspot-menu.css" type="text/css">
      <!-- =-=-=-=-=-=-= Animation =-=-=-=-=-=-= -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css">
      <!-- =-=-=-=-=-=-= Select Options =-=-=-=-=-=-= -->
      <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet" />
      <!-- =-=-=-=-=-=-= noUiSlider =-=-=-=-=-=-= -->
      <link href="<?php echo base_url(); ?>assets/css/nouislider.min.css" rel="stylesheet">
      <!-- =-=-=-=-=-=-= Listing Slider =-=-=-=-=-=-= -->
      <link href="<?php echo base_url(); ?>assets/css/slider.css" rel="stylesheet">
      <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.theme.css">
      <!-- =-=-=-=-=-=-= Check boxes =-=-=-=-=-=-= -->
      <link href="<?php echo base_url(); ?>assets/skins/minimal/minimal.css" rel="stylesheet">
      <!-- =-=-=-=-=-=-= PrettyPhoto =-=-=-=-=-=-= -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css" type="text/css" media="screen"/>
      <!-- =-=-=-=-=-=-= Responsive Media =-=-=-=-=-=-= -->
      <link href="<?php echo base_url(); ?>assets/css/responsive-media.css" rel="stylesheet">
      <!-- =-=-=-=-=-=-= Template Color =-=-=-=-=-=-= -->
      <link rel="stylesheet" id="color" href="<?php echo base_url(); ?>assets/css/colors/defualt.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600%7CSource+Sans+Pro:400,400i,600" rel="stylesheet">
      <!-- JavaScripts -->
      <script src="<?php echo base_url(); ?>assets/js/modernizr.js"></script>
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>

<!--       <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
      <script language='javascript'>
      $.get('https://ipinfo.io', function(response) {
      console.log(response.city, response.country);
document.cookie='user_city_name='+response.city;
}, 'jsonp')
</script> -->

<!-- =-=-=-=-=-=-= Background Rotator =-=-=-=-=-=-= -->

