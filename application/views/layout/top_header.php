<style>
  /* Shared */
  .loginBtn {
    box-sizing: border-box;
    position: relative;
    /* width: 13em;  - apply for fixed size */
    margin: 0.2em;
    padding: 0 15px 0 46px;
    border: none;
    text-align: left;
    line-height: 34px;
    white-space: nowrap;
    border-radius: 0.2em;
    font-size: 16px;
    color: #FFF;
  }
  .loginBtn:before {
    content: "";
    box-sizing: border-box;
    position: absolute;
    top: 0;
    left: 0;
    width: 34px;
    height: 100%;
  }
  .loginBtn:focus {
    outline: none;
  }
  .loginBtn:active {
    box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
  }


  /* Facebook */
  .loginBtn--facebook {
    background-color: #4C69BA;
    background-image: linear-gradient(#4C69BA, #3B55A0);
    /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
    text-shadow: 0 -1px 0 #354C8C;
  }
  .loginBtn--facebook:before {
    border-right: #364e92 1px solid;
    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
  }
  .loginBtn--facebook:hover,
  .loginBtn--facebook:focus {
    background-color: #5B7BD5;
    background-image: linear-gradient(#5B7BD5, #4864B1);
  }


  /* Google */
  .loginBtn--google {
    /*font-family: "Roboto", Roboto, arial, sans-serif;*/
    background: #DD4B39;
  }
  .loginBtn--google:before {
    border-right: #BB3F30 1px solid;
    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
  }
  .loginBtn--google:hover,
  .loginBtn--google:focus {
    background: #E74B37;
  }
  
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  }

.dropdown-content a {
  text-decoration: none;
  display: block;
}
.rr{
  margin-top: 35px;
}
.trr{
  margin-left: -3.5px;
}

.dropdown-content a:hover {color: white; background-color: #e52d27;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #e52d27;}
  @media (max-width: 767px) {
  .create_event {
    margin-top: -400px;
    margin-left: 220px;
  }
}
@media (max-width: 991px) {
  .create_event {
    margin-top: -400px;
    margin-left: 220px;
  }
}
@media (max-width: 575px) {
   .create_event {
    margin-top: -400px;
    margin-left: 220px;
  }
}
@media (max-width: 767px) {
  .login_fb {
    margin-top: -340px;
    margin-left: 320px;
    width: 25%;
    font-size: 11px;
    font-weight: bold;
  }
}
@media (max-width: 991px) {
  .login_fb {
    margin-top: -340px;
    margin-left: 320px;
    width: 25%;
    font-size: 11px;
    font-weight: bold;
  }
}
@media (max-width: 575px) {
   .login_fb {
    margin-top: -340px;
    margin-left: 320px;
    width: 25%;
    font-size: 11px;
    font-weight: bold;
  }
}
@media (max-width: 767px) {
  .logof {
    height: 75px;
    float: right;
    margin-top: 30px;
    margin-left: 250px;
  }
}
@media (max-width: 991px) {
  .logof {
    height: 75px;
    float: right;
    margin-top: 30px;
    margin-left: 250px;
  }
}
@media (max-width: 575px) {
   .logof {
    height: 75px;
    float: right;
    margin-top: 30px;
    margin-left: 250px;
  }
}
@media (max-width: 767px) {
  .menu1 {
    margin-top: -148px;
    height: 50px;
    float: left;
    width: 16%;
    margin-left: 100px; 
  }
}
@media (max-width: 991px) {
  .menu1 {
    margin-top: -148px;
    height: 50px;
    float: left;
    width: 16%;
    margin-left: 100px; 
  }
}
@media (max-width: 575px) {
   .menu1 {
    margin-top: -148px;
    height: 50px;
    float: left;
    width: 16%;
    margin-left: 100px; 
  }
}
@media (max-width: 767px) {
  .hidden {
    display: hidden;
  }
}
@media (max-width: 991px) {
  .hidden {
    display: hidden;
  }
}
@media (max-width: 575px) {
   .hidden {
    display: hidden;
  }
}
@media (max-width: 767px) {
  .drap {
    background: #FDFEFE;
    color: white;
    width: 540%;
    line-height: 2;
    position: absolute;
    top:39px;
    left: 0px;
    font-weight: bolder;
    text-align: left;
  }
}
@media (max-width: 991px) {
  .drap {
    background: #FDFEFE;
    color: white;
    width: 540%;
    line-height: 2;
    position: absolute;
    top:39px;
    left: 0px;
    font-weight: bolder;
    text-align: left;
  }
}
@media (max-width: 575px) {
   .drap {
    background: #FDFEFE;
    color: white;
    width: 540%;
    line-height: 2;
    position: absolute;
    top:39px;
    left: 0px;
    font-weight: bolder;
    text-align: left;
  }
}
</style>

<!-- Top Bar -->
<div class="header-top dark" style="height: 90px; border-bottom: 7px groove #e52d27;">
  <div class="container">
   <div class="row">
  <ul class="menu-logo pull-left">
                           <li>
                              <a href="<?php echo base_url('index.php/Home') ?>"><img class="logof" src="<?php echo base_url(); ?>assets/images/lucknow.png" alt="logo" height="70" style="margin-top: 5px;"> </a>
                           </li>
                        </ul>

    <!-- Header Top Right Social -->
    <div class="header-right col-md-6 col-sm-6 col-xs-12" style="float: right;">
     <div class="pull-right">

      <ul class="listnone" style="margin-top: 18px;">
       <?php if(!empty($this->session->userdata('id'))){ ?>
        <!-- <li class="hidden-xs hidden-sm"><a href="<?php echo base_url('index.php/admin/Signin') ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> Admin</a></li>      -->   
        <li class="dropdown">
          <a href="<?php echo base_url('index.php/Profile') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <?php if (!empty($this->session->userdata('site_url'))){ ?>
              <img class="img-circle resize" alt="" src="<?php echo $this->session->userdata('img'); ?>">
            <?php } else { ?>
              <img class="img-circle resize" alt="" src="<?php echo base_url(); ?>uploads/<?php echo $this->session->userdata('img'); ?>"> 
            <?php } ?>
            <span class="myname hidden-xs"> <?php echo $this->session->userdata('name'); ?> </span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
             <li><a href="<?php echo base_url('index.php/Profile') ?>">User Profile</a></li>
             <li><a href="<?php echo base_url('index.php/BecomeEvent') ?>">Create Event</a></li>
             <li><a href="<?php echo base_url('index.php/MyReviews') ?>">My Reviews</a></li>
             <li><a href="<?php echo base_url('index.php/EventBookingList') ?>">Event Booking List</a></li>
             <li><a href="<?php echo base_url('index.php/MyEvents') ?>">My Events</a></li>
             <li><a href="<?php echo base_url('index.php/Login/logout') ?>">Logout</a></li>

           </ul>
         </li>
         <li>
          <a href="<?php echo base_url('index.php/BecomeEvent') ?>" class="btn btn-theme">Create Event</a>
        </li>

      <?php } else {?>
        <button type="button" class="btn btn-info btn-md login_fb" data-toggle="modal" data-target="#myModal"><i style="font-size:14px" class="fa">&#xf09a;</i> <strong>/</strong> <i style="font-size:14px" class="fa">&#xf1a0;</i>   &nbsp; &nbsp;<strong>Login</strong></button>
        <li><a href="<?php echo base_url('index.php/Login') ?>"><i class="fa fa-sign-in"></i> Log in</a></li>
        <li class="hidden-xs hidden-sm"><a href="<?php echo base_url('index.php/Register') ?>"><i class="fa fa-unlock" aria-hidden="true"></i> Register</a></li>                          
     <!-- <li class="hidden-xs hidden-sm"><a href="<?php echo base_url('index.php/admin/Signin') ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> Admin</a></li>
       <li> -->
        <a href="<?php echo base_url('index.php/Login') ?>" class="btn btn-theme create_event">Create Event</a>
  <span class="btn btn-theme menu1 dropdown trr"><i class="fa fa-bars"></i>
  <span class="dropdown-content drap rr">
    <a href="<?php echo base_url('index.php/BecomeEvent') ?>" style="border-bottom: 1px solid #ECF0F1;">Create Event</a>
    <a href="<?php echo base_url('index.php/TopEvents') ?>" style="border-bottom: 1px solid #ECF0F1;">Top Events</a>
    <a href="<?php echo base_url('index.php/Login') ?>" style="border-bottom: 1px solid #ECF0F1;">Login</a>
    <a href="<?php echo base_url('index.php/About') ?>" style="border-bottom: 1px solid #ECF0F1;">About Us</a>
    <a href="<?php echo base_url('index.php/Contact') ?>" style="border-bottom: 1px solid #ECF0F1;">Contact Us</a>
  </span>
</span>
      </li>
    <?php } ?>
  </ul>
</div>
</div>

<div class="header-right col-md-6 col-sm-6 col-xs-12" style="float: center;">

  <ul class="listnone">                        
   <li class="hidden-xs hidden-sm">
    <!--   <a href="<?php echo base_url('index.php/admin/Signin') ?>"> -->
      <!-- <i class="fa fa-sign-in" aria-hidden="true"></i> -->
      <!-- <span id="top-head-location">Your location: <?php  if (isset($_COOKIE['currentcity'])) {echo $_COOKIE['currentcity'];} ?><span> -->

        <!--   </a> -->
      </li>
    </ul>
  </div>

</div>
</div>
</div>

<!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <br>
        <center><h3 style="color: #000"><strong>Quick Sign In</strong></h3></center>
        <br>
        <center><span>Join events, get recommendations based on your interest. Find where your friends are going.</span></center>
        <br>
        <center>
            <a href="<?php echo $this->session->userdata('facebook_auth')?>">
         <button class="loginBtn loginBtn--facebook" style="width: 100%">
          <center>
            Login with Facebook
          </center>
        </button>
      </a>
      </center>
 
      <CENTER><p><strong>OR</strong></p></CENTER>
  
      <center>
        <a href="<?php echo $this->session->userdata('google_auth')?>">
        <button class="loginBtn loginBtn--google" style="width: 100%; margin-top: -12px">
          <center>
            Login with Google
          </center>
        </button>
      </a>
      </center>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>

</div>
</div>
<!-- Top Bar End -->
