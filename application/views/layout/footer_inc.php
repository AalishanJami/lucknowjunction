<!-- Back To Top -->
      <a href="#0" class="cd-top">Top</a>
      <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
      <!-- Bootstrap Core Css  -->
      <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
      <!-- Jquery Easing -->
      <script src="<?php echo base_url(); ?>assets/js/easing.js"></script>
      <!-- Menu Hover  -->
      <script src="<?php echo base_url(); ?>assets/js/carspot-menu.js"></script>
      <!-- Jquery Appear Plugin -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.appear.min.js"></script>
      <!-- Numbers Animation   -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.countTo.js"></script>
      <!-- Jquery Select Options  -->
      <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
      <!-- noUiSlider -->
      <script src="<?php echo base_url(); ?>assets/js/nouislider.all.min.js"></script>
      <!-- Carousel Slider  -->
      <script src="<?php echo base_url(); ?>assets/js/carousel.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/slide.js"></script>
      <!-- Image Loaded  -->
      <script src="<?php echo base_url(); ?>assets/js/imagesloaded.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/isotope.min.js"></script>
      <!-- CheckBoxes  -->
      <script src="<?php echo base_url(); ?>assets/js/icheck.min.js"></script>
      <!-- Jquery Migration  -->
      <script src="<?php echo base_url(); ?>assets/js/jquery-migrate.min.js"></script>

      <!-- Style Switcher -->
      <script src="<?php echo base_url(); ?>assets/js/color-switcher.js"></script>
      <!-- PrettyPhoto -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js"></script>
      <!-- Wow Animation -->
      <script src="<?php echo base_url(); ?>assets/js/wow.js"></script>
      <!-- Template Core JS -->
      <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
   </body>

<!-- Mirrored from templates.scriptsbundle.com/carspot/demos/index-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Nov 2019 08:22:11 GMT -->
</html>

