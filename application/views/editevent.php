<style>
[type="date"] {
  background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;
}
[type="date"]::-webkit-inner-spin-button {
  display: none;
}
[type="date"]::-webkit-calendar-picker-indicator {
  opacity: 0;
}

input {
  border: 1px solid #c4c4c4;
  border-radius: 5px;
  background-color: #fff;
  padding: 3px 5px;
  box-shadow: inset 0 3px 6px rgba(0,0,0,0.1);
  width: 190px;
}
</style>

<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
<div class="page-header-area-2 gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="small-breadcrumb">
                    <div class="breadcrumb-link">
                        <ul>
                             <li><a href="<?php echo base_url();?>">Home Page</a></li>
                            <li><a class="active" href="#">Edit Event</a></li>
                        </ul>
                    </div>
                    <div class="header-page">
                        <h1>Edit Event</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
    <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
    <section class="section-padding no-top gray ">
        <!-- Main Container -->
        <div class="container">
            <!-- Row -->
            
                       <?php if ($this->session->flashdata('success')) { ?>
    <h3 style="font-size: 15px; color:green;float: right;" >
        <?php echo $this->session->flashdata('success'); ?>
    </h3>
<?php } ?>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <!-- end post-padding -->

                   
                    <div class="post-ad-form postdetails">
                    <?php echo form_open_multipart('MyEvents/update/'.$events[0]->event_id, array('id' => 'event', 'class'=> 'submit-form')); ?>
                        
                            <!-- Title  -->
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <label class="control-label">Event Name <span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" name="name" value="<?php echo $events[0]->event_name; ?>" 
                                        placeholder="Enter the name of your event" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                    <label class="control-label">Date <span style="color: red;">*</span></label>
                                     <input type="date" name="date"value="<?php echo $events[0]->event_date; ?>" 
>
                                  <!--   <input type="text" name="date" class="form-control" onfocus="(this.type='date')"
                                        placeholder="Enter date of event" required> -->
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                    <label class="control-label">Time</label>
                                    <input type="text"  name="time" required class="form-control"
                                    value="<?php echo $events[0]->event_time; ?>" 

                                        onfocus="(this.type='time')" placeholder="Enter time of event">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <label class="control-label">Category<span style="color: red;">*</span></label>
                                    <select name="category" form="event">
                                    <?php foreach ($category as $item){ 
                                            if($item->category_id == $events[0]->category_id){ ?> 
  <option value="<?php echo $item->category_id; ?>" selected><?php echo $item->category_name; ?></option>
                                            <?php }else { ?>
                                        <option value="<?php echo $item->category_id; ?>" ><?php echo $item->category_name; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <label class="control-label">Venue<span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" name="chief_guest"
                                     value="<?php echo $events[0]->event_chiefGuest; ?>" 
                                        placeholder="Enter Category of the event" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <label class="control-label">Country<span style="color: red;">*</span></label>
                                    <select name="country" form="event">
                                    <?php foreach ($country as $item){ 
                                               if($item->country_id == $events[0]->country_id){ ?> 
  <option value="<?php echo $item->country_id; ?>" selected><?php echo $item->country_name; ?></option>
                                            <?php }else { ?>
                                        <option value="<?php echo $item->country_id; ?>"><?php echo $item->country_name; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <label class="control-label">State<span style="color: red;">*</span></label>
                                    <select name="state" form="event">
                                    <?php foreach ($state as $item){ 
                                          if($item->state_id == $events[0]->state_id){ ?> 
  <option value="<?php echo $item->state_id; ?>" selected><?php echo $item->state_name; ?></option>
                                            <?php }else { ?>
                                        <option value="<?php echo $item->state_id; ?>"><?php echo $item->state_name; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <label class="control-label">City<span style="color: red;">*</span></label>
                                    <select name="city" form="event">
                                    <?php foreach ($city as $item){ 
                                           if($item->city_id == $events[0]->city_id){ ?> 
 <option value="<?php echo $item->city_id; ?>" selected><?php echo $item->city_name; ?></option>
                                            <?php }else { ?>
                                        <option value="<?php echo $item->city_id; ?>"><?php echo $item->city_name; ?></option>
                                        <?php } }?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <label class="control-label">Description<span style="color: red;">*</span></label>
                                    <textarea class="form-control" cols=20 row =9 name="description"    placeholder="description of event"
                                        required><?php echo $events[0]->event_description; ?></textarea>
                                </div>
                            </div>

                            <input type="submit" class="btn btn-theme" value="Update">
                            <?php form_close() ?>
                    </div>
                    <!-- end post-ad-form-->
                </div>
                <!-- end col -->

                <!-- Middle Content Area  End -->
                <!-- end col -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Main Container End -->
    </section>
    <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

    <script type='text/javascript'>


    function addFields() {
        // Number of inputs to create
        var number = document.getElementById("member").value;
        // Container <div> where dynamic content will be placed
        var container = document.getElementById("container");
        // Clear previous contents of the container
        while (container.hasChildNodes()) {
            container.removeChild(container.lastChild);
        }
        for (i = 0; i < number; i++) {
            // Append a node with a random text
            container.appendChild(document.createTextNode("Member " + (i + 1)));
            // Create an <input> element, set its type and name attributes
            var input = document.createElement("input");
            input.type = "text";
            input.name = "member" + i;
            container.appendChild(input);
            // Append a line break 
            container.appendChild(document.createElement("br"));
        }
    }

    function show(){
        document.getElementById('hideorshow').style.display = 'block';
    }

    function hide(){
        document.getElementById('hideorshow').style.display = 'none';
    }

 

    </script>