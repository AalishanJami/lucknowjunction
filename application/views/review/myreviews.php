<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= --><div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class=" breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Reviews</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Your Reviews</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top reviews gray ">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
                  <!-- Middle Content Area -->
                  <div class="col-md-8 col-xs-12 col-sm-12 news">
                      <!-- Middle Content Box -->
                  <div class="row">
                     <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="posts-masonry">
                           <?php foreach($items as $item){?>
                            
                           <div class="col-md-4 col-xs-12 col-sm-6">
                              <!-- Ad Box -->
                              <div class="short-description">
                                 
                                 <div class="short-description">
                                    <!-- Ad Title -->
                                    <h3><a title="" href="single-page-listing.html">Review: </a></h3>
                                 <span ><?php echo $item->event_review;?></span></span>
                                 <br><br>
                                  <span><strong>Date:</strong> &nbsp<?php echo strstr($item->review_date, ' ', true);?></span>
                                 </div>
                              </div>
                           </div>
                           <br><br>
                           <?php } ?>
                        </div>
                     </div>
                  </div>
               
                     
            
                     </div>
                  </div>
                  <!-- Right Sidebar -->
                  
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
