<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Terms of Services</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Terms of Services</h1>
                     </div>
                  </div>
               </div>
            </div>
      </div>
   </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <br><br>
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <ul>
                        <li>
                           <div>
                              <h2 style="font-weight: bold; color: black;">TERMS OF SERVICE AGREEMENT</h2><br>
                              <p>LAST REVISION: 09/11/2019</p>
                              <h5 style="font-weight: bold; color: red;">PLEASE READ THIS TERMS OF SERVICE AGREEMENT CAREFULLY. BY USING THIS WEBSITE OR ORDERING SERVICES FROM THIS WEBSITE YOU AGREE TO BE BOUND BY ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT.</h5><br>
                              <p style="text-align: justify;">This Terms of Service Agreement (the "Agreement") governs your use of this website, www.glammbeauty.com (the "Website"), Glamm Beauty – Salon at Home ("Business Name") offer of Services for purchase on this Website, or your purchase of services available on this Website. This Agreement includes, and incorporates by this reference, the policies and guidelines referenced below. Glamm Beauty – Salon at Home reserves the right to change or revise the terms and conditions of this Agreement at any time by posting any changes or a revised Agreement on this Website. Glamm Beauty – Salon at Home will alert you that changes or revisions have been made by indicating on the top of this Agreement the date it was last revised. The changed or revised Agreement will be effective immediately after it is posted on this Website. Your use of the Website following the posting any such changes or of a revised Agreement will constitute your acceptance of any such changes or revisions. Glamm Beauty – Salon at Home encourages you to review this Agreement whenever you visit the Website to make sure that you understand the terms and conditions governing use of the Website. This Agreement does not alter in any way the terms or conditions of any other written agreement you may have with Glamm Beauty – Salon at Home for other services or services. If you do not agree to this Agreement (including any referenced policies or guidelines), please immediately terminate your use of the Website. If you would like to print this Agreement, please click the print button on your browser toolbar.</p>
                           </div>
                        </li>
                        <li>
                           <div>
                              <h2 style="font-weight: bold; color: black;">Services</h2><br>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Terms of Offer.</strong> This Website offers for sale certain services (the "Services"). By placing an order for Services through this Website, you agree to the terms set forth in this Agreement.</p>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Customer Solicitation:</strong> Unless you notify our third party call center reps or direct Glamm Beauty – Salon at Home sales reps, while they are calling you, of your desire to opt out from further direct company communications and solicitations, you are agreeing to continue to receive further emails and call solicitations Glamm Beauty – Salon at Home and its designated in house or third party call team(s).</p>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Opt Out Procedure:</strong> We provide 3 easy ways to opt out of from future solicitations. 1. You may use the opt out link found in any email solicitation that you may receive. 2. You may also choose to opt out, via sending your email address to: info@glammbeauty.com</p>
                              <p style="text-align: justify;">You may send a written remove request to Indira Nagar, Lucknow, Uttar Pradesh.</p>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Proprietary Rights.</strong> Glamm Beauty – Salon at Home has proprietary rights and trade secrets in the Services. You may not copy, reproduce, resell or redistribute any Product manufactured and/or distributed by Glamm Beauty – Salon at Home. Glamm Beauty – Salon at Home also has rights to all trademarks and trade dress and specific layouts of this webpage, including calls to action, text placement, images and other information.</p>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Sales Tax.</strong> If you purchase any Services, you will be responsible for paying any applicable sales tax.</p>
                           </div>
                        </li>
                        <li>
                           <div>
                              <h2 style="font-weight: bold; color: black;">Website</h2><br>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Content; Intellectual Property; Third Party Links.</strong> In addition to making Services available, this Website also offers information and marketing materials. This Website also offers information, both directly and through indirect links to third-party websites, about nutritional and dietary supplements. Glamm Beauty – Salon at Home does not always create the information offered on this Website; instead the information is often gathered from other sources. To the extent that Glamm Beauty – Salon at Home does create the content on this Website, such content is protected by intellectual property laws of the India, foreign nations, and international bodies. Unauthorized use of the material may violate copyright, trademark, and/or other laws. You acknowledge that your use of the content on this Website is for personal, noncommercial use. Any links to third-party websites are provided solely as a convenience to you. Glamm Beauty – Salon at Home does not endorse the contents on any such third-party websites. Glamm Beauty – Salon at Home is not responsible for the content of or any damage that may result from your access to or reliance on these third-party websites. If you link to third-party websites, you do so at your own risk.</p>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Use of Website;</strong> Glamm Beauty – Salon at Home is not responsible for any damages resulting from use of this website by anyone. You will not use the Website for illegal purposes. You will (1) abide by all applicable local, state, national, and international laws and regulations in your use of the Website (including laws regarding intellectual property), (2) not interfere with or disrupt the use and enjoyment of the Website by other users, (3) not resell material on the Website, (4) not engage, directly or indirectly, in transmission of "spam", chain letters, junk mail or any other type of unsolicited communication, and (5) not defame, harass, abuse, or disrupt other users of the Website.</p>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">License.</strong> By using this Website, you are granted a limited, non-exclusive, non-transferable right to use the content and materials on the Website in connection with your normal, noncommercial, use of the Website. You may not copy, reproduce, transmit, distribute, or create derivative works of such content or information without express written authorization from Glamm Beauty – Salon at Home or the applicable third party (if third party content is at issue).</p>
                              <p style="text-align: justify;"><strong style="color: black; font-weight: bold;">Posting.</strong> By posting, storing, or transmitting any content on the Website, you hereby grant Glamm Beauty – Salon at Home a perpetual, worldwide, non-exclusive, royalty-free, assignable, right and license to use, copy, display, perform, create derivative works from, distribute, have distributed, transmit and assign such content in any form, in all media now known or hereinafter created, anywhere in the world. Glamm Beauty – Salon at Home does not have the ability to control the nature of the user-generated content offered through the Website. You are solely responsible for your interactions with other users of the Website and any content you post. Glamm Beauty – Salon at Home is not liable for any damage or harm resulting from any posts by or interactions between users. Glamm Beauty – Salon at Home reserves the right, but has no obligation, to monitor interactions between and among users of the Website and to remove any content Glamm Beauty – Salon at Home deems objectionable, in Glamm Beauty – Salon at Home's sole discretion. </p>
                           </div>
                        </li>
                        <li>
                           <div>
                              <h2 style="font-weight: bold; color: black;">DISCLAIMER OF WARRANTIES</h2><br>
                              <p style="text-align: justify;">YOUR USE OF THIS WEBSITE AND/OR SERVICES ARE AT YOUR SOLE RISK. THE WEBSITE AND SERVICES ARE OFFERED ON AN "AS IS" AND "AS AVAILABLE" BASIS. Glamm Beauty – Salon at Home EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT WITH RESPECT TO THE SERVICES OR WEBSITE CONTENT, OR ANY RELIANCE UPON OR USE OF THE WEBSITE CONTENT OR SERVICES. ("SERVICES" INCLUDE TRIAL SERVICES.)</p>
                              <p style="text-align: justify;">WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, Glamm Beauty – Salon at Home MAKES NO WARRANTY:
                              <li style="list-style-type: disc;">THAT THE INFORMATION PROVIDED ON THIS WEBSITE IS ACCURATE, RELIABLE, COMPLETE, OR TIMELY.</li>
                              <li style="list-style-type: disc;">THAT THE LINKS TO THIRD-PARTY WEBSITES ARE TO INFORMATION THAT IS ACCURATE, RELIABLE, COMPLETE, OR TIMELY.</li>
                              <li style="list-style-type: disc;">NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM THIS WEBSITE WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED HEREIN.</li>
                              <li style="list-style-type: disc;">AS TO THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICES OR THAT DEFECTS IN SERVICES WILL BE CORRECTED.</li>
                              <li style="list-style-type: disc;">REGARDING ANY SERVICES PURCHASED OR OBTAINED THROUGH THE WEBSITE.</li>
                              <li style="list-style-type: disc;">SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES, SO SOME OF THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU.</li>
                              </p>
                              
                           </div>
                        </li>
                        <li style="list-style-type: none;">
                           <div>
                              <h2 style="font-weight: bold; color: black;">LIMITATION OF LIABILITY</h2><br>
                              <p style="text-align: justify;">Glamm Beauty – Salon at Home ENTIRE LIABILITY, AND YOUR EXCLUSIVE REMEDY, IN LAW, IN EQUITY, OR OTHWERWISE, WITH RESPECT TO THE WEBSITE CONTENT AND SERVICES AND/OR FOR ANY BREACH OF THIS AGREEMENT IS SOLELY LIMITED TO THE AMOUNT YOU PAID, LESS SHIPPING AND HANDLING, FOR SERVICES PURCHASED VIA THE WEBSITE.</p>
                              <p style="text-align: justify;">Glamm Beauty – Salon at Home WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES IN CONNECTION WITH THIS AGREEMENT OR THE SERVICES IN ANY MANNER, INCLUDING LIABILITIES RESULTING FROM (1) THE USE OR THE INABILITY TO USE THE WEBSITE CONTENT OR SERVICES; (2) THE COST OF PROCURING SUBSTITUTE SERVICES OR CONTENT; (3) ANY SERVICE PURCHASED OR OBTAINED OR TRANSACTIONS ENTERED INTO THROUGH THE WEBSITE; OR (4) ANY LOST PROFITS YOU ALLEGE.</p>
                              <p style="text-align: justify;">SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.</p>
                              
                           </div>
                        </li>
                        <li style="list-style-type: none;">
                           <div>
                              <h2 style="font-weight: bold; color: black;">INDEMNIFICATION</h2><br>
                              <p style="text-align: justify;">You will release, indemnify, defend and hold harmless Glamm Beauty – Salon at Home and any of its contractors, agents, employees, officers, directors, shareholders, affiliates and assigns from all liabilities, claims, damages, costs and expenses, including reasonable attorneys' fees and expenses, of third parties relating to or arising out of (1) this Agreement or the breach of your warranties, representations and obligations under this Agreement; (2) the Website content or your use of the Website content; (3) the Services or your use of the Services (including Trial Services); (4) any intellectual property or other proprietary right of any person or entity; (5) your violation of any provision of this Agreement; or (6) any information or data you supplied to Glamm Beauty – Salon at Home. When Glamm Beauty – Salon at Home is threatened with suit or sued by a third party, Glamm Beauty – Salon at Home may seek written assurances from you concerning your promise to indemnify Glamm Beauty – Salon at Home; your failure to provide such assurances may be considered by Glamm Beauty – Salon at Home  to be a material breach of this Agreement. Glamm Beauty – Salon at Home will have the right to participate in any defense by you of a third-party claim related to your use of any of the Website content or Services, with counsel of Glamm Beauty – Salon at Home choice at its expense. Glamm Beauty – Salon at Home will reasonably cooperate in any defense by you of a third-party claim at your request and expense. You will have sole responsibility to defend Glamm Beauty – Salon at Home against any claim, but you must receive Glamm Beauty – Salon at Home prior written consent regarding any related settlement. The terms of this provision will survive any termination or cancellation of this Agreement or your use of the Website or Services.</p>
                           </div>
                        </li>
                        <li style="list-style-type: none;">
                           <div>
                              <h2 style="font-weight: bold; color: black;">Privacy</h2><br>
                              <p style="text-align: justify;">Glamm Beauty – Salon at Home believes strongly in protecting user privacy and providing you with notice of Glamm Beauty – Salon at Home's use of data. Please refer to Glamm Beauty – Salon at Home privacy policy, incorporated by reference herein, that is posted on the Website.</p>
                              
                           </div>
                        </li>
                        <li style="list-style-type: none;">
                           <div>
                              <h2 style="font-weight: bold; color: black;">AGREEMENT TO BE BOUND</h2><br>
                              <p style="text-align: justify;">By using this Website or ordering Services, you acknowledge that you have read and agree to be bound by this Agreement and all terms and conditions on this Website.</p>
                           </div>
                        </li>
                        <li style="list-style-type: none;">
                           <div>
                              <h4 style="font-weight: bold; color: black;">General:</h4><br>
                              <p style="text-align: justify; margin-top: -20px;"><strong style="font-weight: bold;color: black;">Force Majeure.</strong> Glamm Beauty – Salon at Home will not be deemed in default hereunder or held responsible for any cessation, interruption or delay in the performance of its obligations hereunder due to earthquake, flood, fire, storm, natural disaster, act of God, war, terrorism, armed conflict, labor strike, lockout, or boycott.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Cessation of Operation. </strong>Glamm Beauty – Salon at Home may at any time, in its sole discretion and without advance notice to you, cease operation of the Website and distribution of the Services.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Entire Agreement.</strong> This Agreement comprises the entire agreement between you and Glamm Beauty – Salon at Home and supersedes any prior agreements pertaining to the subject matter contained herein.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Effect of Waiver.</strong> The failure of Glamm Beauty – Salon at Home to exercise or enforce any right or provision of this Agreement will not constitute a waiver of such right or provision. If any provision of this Agreement is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions of this Agreement remain in full force and effect.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Governing Law;</strong> Indian This Website originates from the Lucknow, Uttar Pradesh. This Agreement will be governed by the laws of the State of Uttar Pradesh, India without regard to its conflict of law principles to the contrary.</p>
                              <p style="text-align: justify;">Neither you nor Glamm Beauty – Salon at Home will commence or prosecute any suit, proceeding or claim to enforce the provisions of this Agreement, to recover damages for breach of or default of this Agreement, or otherwise arising under or by reason of this Agreement, other than in courts located in State of Uttar Pradesh. By using this Website or ordering Services, you consent to the jurisdiction and venue of such courts in connection with any action, suit, proceeding or claim arising under or by reason of this Agreement. You hereby waive any right to trial by jury arising out of this Agreement and any related documents.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Statute of Limitation.</strong> You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Website or Services or this Agreement must be filed within one (1) year after such claim or cause of action arose or be forever barred.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Waiver of Class Action Rights.</strong> BY ENTERING INTO THIS AGREEMENT, YOU HEREBY IRREVOCABLY WAIVE ANY RIGHT YOU MAY HAVE TO JOIN CLAIMS WITH THOSE OF OTHER IN THE FORM OF A CLASS ACTION OR SIMILAR PROCEDURAL DEVICE. ANY CLAIMS ARISING OUT OF, RELATING TO, OR CONNECTION WITH THIS AGREEMENT MUST BE ASSERTED INDIVIDUALLY.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Termination.</strong> Glamm Beauty – Salon at Home reserves the right to terminate your access to the Website if it reasonably believes, in its sole discretion, that you have breached any of the terms and conditions of this Agreement. Following termination, you will not be permitted to use the Website and Glamm Beauty – Salon at Home may, in its sole discretion and without advance notice to you, cancel any outstanding orders for Services. If your access to the Website is terminated, Glamm Beauty – Salon at Home reserves the right to exercise whatever means it deems necessary to prevent unauthorized access of the Website. This Agreement will survive indefinitely unless and until Glamm Beauty – Salon at Home chooses, in its sole discretion and without advance to you, to terminate it.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Domestic Use.</strong> [Glamm Beauty – Salon at Home makes no representation that the Website or Services are appropriate or available for use in locations outside India. Users who access the Website from outside India do so at their own risk and initiative and must bear all responsibility for compliance with any applicable local laws.</p>
                              <p style="text-align: justify;"><strong style="font-weight: bold;color: black;">Assignment.</strong> You may not assign your rights and obligations under this Agreement to anyone. Glamm Beauty – Salon at Home may assign its rights and obligations under this Agreement in its sole discretion and without advance notice to you.</p><br>
                              <p style="text-align: justify; color: red; font-weight: bold; font-size: 17px;">BY USING THIS WEBSITE OR ORDERING SERVICES FROM THIS WEBSITE YOU AGREE 
TO BE BOUND BY ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT.
</p>
                           </div>
                        </li>
                       </ul>
                  </div>
                  
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->