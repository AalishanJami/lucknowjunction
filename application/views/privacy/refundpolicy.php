<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
      <div class="page-header-area-2 gray">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="small-breadcrumb">
                     <div class="breadcrumb-link">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home Page</a></li>
                           <li><a class="active" href="#">Refund & Cancelation Policy</a></li>
                        </ul>
                     </div>
                     <div class="header-page">
                        <h1>Refund & Cancelation Policy</h1>
                     </div>
                  </div>
               </div>
            </div>
      </div>
   </div>
      <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
      <br><br>
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding no-top">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <ul>
                        <li>
                           <div>
                              <h2 style="font-weight: bold; color: black;">Refund and Cancellation Policy</h2><br>
                              <p style="text-align: justify;">Our focus is complete customer satisfaction. In the event, if you are displeased with the services provided, we will refund back the money, provided the reasons are genuine and proved after investigation. Please read the fine prints of each deal before buying it, it provides all the details about the services or the product you purchase.</p>
                              <p style="text-align: justify;">In case of dissatisfaction from our services, clients have the liberty to cancel their projects and request a refund from us. Our Policy for the cancellation and refund will be as follows:</p>
                           </div>
                        </li>
                        <li>
                           <div>
                              <h2 style="font-weight: bold; color: black;">Cancellation Policy</h2><br>
                              <p style="text-align: justify;">For Cancellations please contact the us via contact us link. <a href="<?php echo base_url('index.php/Contact') ?>">Click Here</a> </p>
                              <p style="text-align: justify;">Requests received later than 3 business days prior to the end of the current service period will be treated as cancellation of services for the next service period.</p>
                           </div>
                        </li>
                        <li>
                           <div>
                              <h2 style="font-weight: bold; color: black;">Refund Policy</h2><br>
                              <p style="text-align: justify;">We will try our best to create the suitable design concepts for our clients.</p>
                              <p style="text-align: justify;">In case any client is not completely satisfied with our products we can provide a refund.</p>
                              <p style="text-align: justify;">If paid online, refunds will be issued to the original Payment Methods provided at the time of purchase and in case of payment gateway name payments refund will be made to the same account.</p>
                           </div>
                        </li>
                        </ul>
                  </div>
                  
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->