 <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
 <style>
    .rate {
        float: left;
        height: 46px;
        padding: 0 10px;
    }
    .rate:not(:checked) > input {
        position:absolute;
        top:-9999px;
    }
    .rate:not(:checked) > label {
        float:right;
        width:1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:30px;
        color:#ccc;
    }
    .rate:not(:checked) > label:before {
        content: '★ ';
    }
    .rate > input:checked ~ label {
        color: #ffc700;    
    }
    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;  
    }
    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }
</style>


<div class="page-header-area-2 gray">
 <div class="container">
     <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="small-breadcrumb">
                 <div class=" breadcrumb-link">
                     <ul>
                         <li><a href="<?php echo base_url();?>">Home Page</a></li>
                         <li><a class="active" href="#">Checkout</a></li>
                     </ul>
                 </div>
                 <div class="header-page">
                     <h1>Checkout</h1>
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
 <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
 <section class="section-padding no-top gray">
     <!-- Main Container -->
     <div class="container">
         <!-- Row -->
         <div class="row">
             <!-- Middle Content Area -->
            
                <!-- Middle Content Area  End -->
            </div>
            <!-- Row End -->
            <div class="row margin-top-40">
             <!-- Middle Content Area -->
             <div class="col-md-12 col-xs-12 col-sm-12">
                 <!-- Row -->
                 <div class="profile-section margin-bottom-20">
                     <div class="profile-tabs">
                      
                         <div class="tab-content">
                        
                                 
        		<div class="container">
        			<form action="<?php echo $action; ?>/_payment" method="post" id="payuForm" name="payuForm">
		                <input type="hidden" name="key" value="<?php echo $mkey; ?>" />
		                <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
		                <input type="hidden" name="txnid" value="<?php echo $tid; ?>" />
		                
		                <div class="form-group">
		                    <label class="control-label">Total Payable Amount</label>
		                    <input class="form-control" name="amount" value="<?php echo $amount; ?>"  readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Your Name</label>
		                    <input class="form-control" name="firstname" id="firstname" value="<?php echo $name; ?>" readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Email</label>
		                    <input class="form-control" name="email" id="email" value="<?php echo $mailid; ?>" readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Phone</label>
		                    <input class="form-control" name="phone" value="<?php echo $phoneno; ?>" readonly />
		                </div>
		                    <input type="hidden" class="form-control" name="productinfo" value="<?php echo $productinfo; ?>"readonly>

		                <div class="form-group">
		                    <input name="surl" value="<?php echo $sucess; ?>" size="64" type="hidden" />
		                    <input name="furl" value="<?php echo $failure; ?>" size="64" type="hidden" />  
		                    <!--for test environment comment  service provider   -->
		                    <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
		                    <input name="curl" value="<?php echo $cancel; ?> " type="hidden" />
		                </div>
		                <div class="form-group float-right">
		                	<input type="submit" value="Pay Now" class="btn btn-success" />
		                </div>
		            </form> 
        		</div>

                              </div>
                       
                          </div>
                      </div>
                  </div>
                  <!-- Row End -->
              </div>
              <!-- Middle Content Area  End -->
          </div>
      </div>
      <!-- Main Container End -->
  </section>
  <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->

  