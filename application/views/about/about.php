<!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
<div class="page-header-area-2 gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="small-breadcrumb">
                    <div class="breadcrumb-link">
                        <ul>
                             <li><a href="<?php echo base_url();?>">Home Page</a></li>
                            <li><a class="active" href="#">About</a></li>
                        </ul>
                    </div>
                    <div class="header-page">
                        <h1>About Us</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
    <!-- =-=-=-=-=-=-= About CarSpot =-=-=-=-=-=-= -->
    <section class="custom-padding about-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="title">
                        <h3>About <span class="heading-color">Lucknow Junction</span> </h3>
                    </div>
                    <div class="content">

                        <p>Lucknow Junction is a digital platform that serves you with the convenience of arranging all
                            the tickets, passes, bookings at single place. Whether it’s about booking some event in the
                            city or about getting passes for your favorite restaurant. The platform is totally helpful
                            to you.
                            Often, it’s not that easy to stay up to date with all the events happening around your city,
                            and, if one stays updated about the events, then it’s not that easy to get tickets, passes
                            for that, you have to move out to get them. Or suppose you want to book passes of
                            restaurants for your special ones, even then, you would have to move up to that restaurant
                            to get the passes. So, to make your life easier and add more fun to it, Lucknow Junction
                            helps you to get your tasks done being seated in the umbrella of your comfort.
                            Lucknow Junction is a futuristic digital platform that would, in the long run, provide you
                            with the convenience to book everything across Lucknow through this single platform. Isn’t
                            it amazing? Yes, be it phone recharge, metro tickets, hotel booking or any other thing you
                            can think of. Every booking would be possible here.
                        </p>
                    </div>
                </div>

            </div>
            <div class="title">
                <h3 style="margin-top: 15px">About Lucknow </h3>
            </div>
            <div class="content">
                Lucknow, the city of Nawabs resides alongside the holy river, Gomti. The city well known for its kebabs,
                is also home to beautiful artforms like Kathak and Chikankaari. The city has something to offer to all
                kinds of people, be it about food, shopping, history, heritage, art forms or any thing else. Due to such
                richness, the city has always attracted people from in and outside the country and of course, our
                celebrities too, which gives them an amazing opportunity to perform and show their talent and to
                Lucknowites, an opportunity to attend their shows, be it related to music, dance or comedy.
                Since, there is no shortage of talents in Lucknow, it has also become a major city to host talent hunt
                shows. So, why not just book a ticket and enjoy the bliss of these shows? Click Here (we can add link
                that directs to the page from where tickets can be booked)

            </div>
            <div class="title">
            <h3 style="margin-top: 15px">About Platform </h3>
            <h4
                style="color:red; font-style: italic;margin-top: 10px;font-weight: bold;font-size: 20px;text-align: center;">
                Our Company “La Event Rockers”</h4>
        </div>
        <div  class="content">
            La Event Rockers is one among the prominent event management companies in the capital of Uttar Pradesh,
            Lucknow. The company provides strategic planning, creative solutions and flawless production with the help
            of a highly professional team of experts who think and work in accordance to their clients’ needs. La Event
            Rockers works towards conceptualizing and executing corporate events, wedding planning, entertainment
            events, conferences & seminars, fashion shows, brand activation and product launches. From scratch to final
            execution, every tidbit is handled very delicately as it believes in creating experiences, as the event can
            be forgotten, but the experiences last lifetime.

        </div>
</div>
<h2 style="color:#ff6666; text-align: center; font-weight: bold; font-family: sans-serif;">How is Lucknow Junction
    Beneficial?</h2><center>
<ul style="list-style-type:circle">
    <li style="text-align: center;font-weight: bold;color: black;font-size: 20px;font-weight: bold; margin-top: 15px">
        Time Saving</li>
    <span style="margin-top: 20px; font-size:16px">Since, you don’t have to roam around to get tickets
        and passes, neither give your time in searching events happening around, thus, saving you time and
        efforts.</span>
    <li style="text-align:center; font-weight: bold;margin-top: 10px; color: black;font-size: 20px;font-weight: bold;">
        Convenient</li>
    <span style="margin-top: 20px;font-size:16px">SThrough this platform, you can book tickets, passes
        for events and restaurants of your choice, at your own place and at your convenience.</span>
    <li style=" text-align: center;font-weight: bold;color: black;font-size: 20px;font-weight: bold;margin-top: 10px">
        Updates</li>
    <span style="margin-top: 20px;font-size:16px">The platform, Lucknow Junction will keep you
        updating regarding events happening across the Lucknow city, thus, keeping you all informed.</span>
</ul></center>
        </div>

</section>
<!-- =-=-=-=-=-=-= About CarSpot End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Services Section  =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-=  Services Section End =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Testimonials =-=-=-=-=-=-= -->


<section style="margin-top: 35px" class="about-us">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 no-padding">
                <!-- service box 3 -->

                <!-- service box end -->
                <!-- service box 3 -->
                <div class="col-sm-6 col-md-6 col-xs-12 no-padding">
                    <div class="why-us border-box text-center">
                        <h5>Our mission</h5>
                        <p>To make bookings of anything and everything happening in Lucknow, possible at single
                            platform.</p>
                    </div>
                    <!-- end featured-item -->
                </div>
                <!-- service box end -->
                <!-- service box 3 -->
                <div class="col-sm-6 col-md-6 col-xs-12 no-padding">
                    <div class="why-us border-box text-center">
                        <h5>Our Vision</h5>
                        <p>To help people enjoy the best of Lucknow through making pre-arrangements at your comfort.</p>
                    </div>
                    <!-- end featured-item -->
                </div>
                <!-- service box end -->
            </div>
        </div>
    </div>
    <!-- end container -->
</section>
<div class="clearfix"></div>
<!-- =-=-=-=-=-=-= Statistics Counter =-=-=-=-=-=-= -->

<!-- /.funfacts -->
<!-- =-=-=-=-=-=-= Statistics Counter End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Clients  =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Clients  End =-=-=-=-=-=-= -->