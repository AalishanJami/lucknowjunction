<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Google API Configuration
| -------------------------------------------------------------------
| 
| To get API details you have to create a Google Project
| at Google API Console (https://console.developers.google.com)
| 
|  client_id         string   Your Google API Client ID.
|  client_secret     string   Your Google API Client secret.
|  redirect_uri      string   URL to redirect back to after login.
|  application_name  string   Your Google application name.
|  api_key           string   Developer key.
|  scopes            string   Specify scopes
*/
$config['google']['client_id']        = '491043002778-0mr4j1n272qo30o8s88n7mdp66c7ffaq.apps.googleusercontent.com';
$config['google']['client_secret']    = 'vZ5iO_NCAdCdLGCPGw3nBYdL';
$config['google']['redirect_uri']     = base_url('index.php/Login/oAuthGoogle');
$config['google']['application_name'] = 'Lucknowjunction';
$config['google']['api_key']          = '';
// $config['google']['scopes']           = array(
// "https://www.googleapis.com/auth/plus.login",
// "https://www.googleapis.com/auth/userinfo.email",
// "https://www.googleapis.com/auth/userinfo.profile",
// "https://www.googleapis.com/auth/plus.me"
// );