<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStrm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MProfile');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        //$this->load->database(); // load database
    }

    public function index()
    {

            $id = $this->session->userdata('id');
      $data['items'] = $this->MProfile->fetch($id);
            $this->load->view('layout/header_inc');
            $this->load->view('layout/top_header');
            $this->load->view('layout/nav');
            $this->load->view('profile/profile',$data);
            $this->load->view('layout/footer');
            $this->load->view('layout/footer_inc');
        }

    public function update($id){
         // configurations from upload library
            $arraypath = explode(".", strtolower($_FILES['userfile']['name']));
        $extension = end($arraypath);
        $image = time().mt_rand(1, 99).'.'.$extension;
        // configurations from upload library
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048000'; // max size in KB
        $config['max_width'] = '20000'; //max resolution width
        $config['max_height'] = '20000';  //max resolution height
        $config['file_name'] = $image;
      
        // load CI libarary called upload
        $this->load->library('upload', $config);
        // body of if clause will be executed when image uploading is failed
        if (!$this->upload->do_upload()) {
            $errors = array('error' => $this->upload->display_errors());
            // This image is uploaded by deafult if the selected image in not uploaded
            $image = 'no_image.png';
        } // body of else clause will be executed when image uploading is succeeded
        else {
            $data = array('upload_data' => $this->upload->data());
        }
    
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $fb = $this->input->post('fb');
            $insta = $this->input->post('insta');
           
                $data = array(
                    'user_name' => $name,
                    'user_email' => $email,
                    'user_contact' => $phone,
                    'user_fb' => $fb,     
                    'user_insta' => $insta,
                    'user_img' => $image,
                    'is_Active' => "1"
                );

            
            $this->MProfile->update($id,$data);
           
            redirect("Profile");
}
    }