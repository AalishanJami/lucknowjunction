<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mhome');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index($type, $id)
    {
        if($type == "category"){
            $data['all']=$this->Mhome->getSample("category_id", $id);
            $data['name'] = $this->Mhome->getCategoryName("category_id", $id);
        }
        else if($type == "country"){
            $data['all']=$this->Mhome->getSample("country_id", $id);
    }
    else if($type == "city"){
            $data['all']=$this->Mhome->getSample("city_id", $id);
    }
    $data['type'] = $type;
        $this->load->view('layout/header_inc');
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('event/customevents', $data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');
    }
}