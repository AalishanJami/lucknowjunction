<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class EventDetails extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Meventdetails');
        $this->load->model('MProfile');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('email');
        $this->load->database(); // load database
    }

    public function index($id)
    {

        if(!empty($this->session->userdata('id'))){
            $data['ratinguser'] = $this->Meventdetails->getRatingsUser($id, $this->session->userdata('id'));
        }
        if(!empty($this->session->userdata('id'))){
            $data['ratinguser'] = $this->Meventdetails->getRatingsUser($id, $this->session->userdata('id'));
        }
        $data['events'] = $this->Meventdetails->getDetails($id);
        $data['prices'] = $this->Meventdetails->getPrices($id);
        $data['bookings'] = $this->Meventdetails->getBookings($id);
        $data['ratings'] = $this->Meventdetails->getRatings($id);
        $data['reviews'] = $this->Meventdetails->getReviews($id);
        $data['adsq'] = $this->Meventdetails->getAdsSq();
        $data['adrec'] = $this->Meventdetails->getAdsRec();
        $this->load->view('layout/header_inc', $data);
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('event/eventdetails', $data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');
    }

    function addRating($event){
       if(empty($this->session->userdata('id'))){
        redirect('Login');
    }
    $id = $this->session->userdata('id');
    $rating = $this->input->post('rate');

    $data = array(
        'rating' => $rating,
        'event_id' => $event,
        'user_id' => $id
    );
    $this->Meventdetails->saveRating($data);
    redirect('EventDetails/index/'.$id);

}

function review($eventid){
   if(empty($this->session->userdata('id'))){
    redirect('Login');
}
$userid = $this->session->userdata('id');
$review = $this->input->post('review');

$data = array(
    'user_id' => $userid,
    'event_id' => $eventid,
    'event_review' => $review,
    'isActive'=> "1"
);
$this->Meventdetails->saveReview($data);
redirect('EventDetails/index/'.$eventid);
}

function booknow($id){
    if(empty($this->session->userdata('id'))){
        redirect('Login');
    }
    if(!empty($this->session->userdata('id'))){
        $data['ratinguser'] = $this->Meventdetails->getRatingsUser($id, $this->session->userdata('id'));
    }
    $data['events'] = $this->Meventdetails->getDetails($id);
    $data['prices'] = $this->Meventdetails->getPrices($id);
    $data['bookings'] = $this->Meventdetails->getBookings($id);
    $data['ratings'] = $this->Meventdetails->getRatings($id);
    $data['reviews'] = $this->Meventdetails->getReviews($id);
    $this->load->view('layout/header_inc');
    $this->load->view('layout/top_header');
    $this->load->view('layout/nav');
    $this->load->view('event/booknow', $data);
    $this->load->view('layout/footer');
    $this->load->view('layout/footer_inc');
}

function freeBooking(){
   if(empty($this->session->userdata('id'))){
    redirect('Login');
}
if(!empty($this->session->userdata('id'))){
    $randomNumber = rand(999999,999999999999999999); 
    $data['txnid'] = strval($randomNumber);
    $usid= $this->session->userdata('id');
    $count = $this->input->post('ticketcount');
    $eventid = $this->input->post('id');
    $insert = array(
        'user_id' => $usid,
        'event_id' => $eventid,
        'booking_status' => '1',
        'trxid' => $data['txnid']
    );

    $this->Meventdetails->savePayment($insert);
}
$randomNumber = rand(999999,999999999999999999); 
$data['txnid'] = strval($randomNumber);
$data['ticketcount'] = $count;
$data['event_details'] = $this->Meventdetails->getDetails($eventid);
$data['cust_details'] = $this->MProfile->fetch($usid);
$this->customer($data);   
}

function customer($data){
    $from_email = "info@lucknowjunction.com"; 
    $to_email = $data['cust_details'][0]->user_email;
    $this->email->from($from_email, 'Lucknowjunction'); 
    $this->email->to($to_email);
    $this->email->subject('Your Event has been booked!'); 
    $this->email->message('Thanks '.$data['cust_details'][0]->user_name.'
      Your Booking is Confirmed.
      The details for ticket are as follows:
      Booking ID: '.$data['txnid'].'
      Event Name:  '.$data['event_details'][0]->event_name.'
      Date: '.$data['event_details'][0]->event_date.'
      Time: '.$data['event_details'][0]->event_time.'
      Venue: '.$data['event_details'][0]->city_name.'
      Quantity : '.$data['ticketcount'].'
      Type: Free

      For any queries Contact us at:
      www.lucknowjunction.com
      support@lucknowjunction.com'); 

         //Send mail 
    if($this->email->send()) {
      $this->organizer($data);
  }
  else {
   echo $this->email->print_debugger();

}
}

function organizer($data){
    $from_email = "info@lucknowjunction.com"; 
    $data['get_registered_user'] = $this->MProfile->fetch($data['event_details'][0]->organizer_id);
    $to_email = $data['get_registered_user'][0]->user_email;

    $this->email->from($from_email, 'Lucknowjunction'); 
    $this->email->to($to_email);
    $this->email->subject('Your Event has been booked!'); 
    $this->email->message('Dear '.$data['cust_details'][0]->user_name.'
      Congratulations on New Booking!
      The details for ticket are as follows:
      Booking ID:  '.$data['txnid'].'
      Event Name:  '.$data['event_details'][0]->event_name.'
      Date: '.$data['event_details'][0]->event_date.'
      Time: '.$data['event_details'][0]->event_time.'
      Venue: '.$data['event_details'][0]->city_name.'
      Quantity : '.$data['ticketcount'].'
      Type: Free

      For any queries Contact us at:
      www.lucknowjunction.com
      support@lucknowjunction.com'); 

         //Send mail 
    if($this->email->send()) {
      $this->admin($data);
  }
  else {
   echo $this->email->print_debugger();

}
}

function admin($data){

    $from_email = "info@lucknowjunction.com"; 
    $to_email = "admin@lucknowjunction.com";
    $this->email->from($from_email, 'Lucknowjunction'); 
    $this->email->to($to_email);
    $this->email->subject('Your Event has been booked!'); 
    $this->email->message('New Booking!
      The details for ticket are as follows:
      Booking ID:  '.$data['txnid'].'
      Event Name:  '.$data['event_details'][0]->event_name.'
      Date: '.$data['event_details'][0]->event_date.'
      Time: '.$data['event_details'][0]->event_time.'
      Venue: '.$data['event_details'][0]->city_name.'
      Quantity : '.$data['ticketcount'].'
      Type: Free

      For any queries Contact us at:
      www.lucknowjunction.com
      support@lucknowjunction.com'); 

         //Send mail 
    if($this->email->send()) {
       redirect('EventDetails/index/'.$data['event_details'][0]->event_id);
        $this->session->set_flashdata('success', 'Booking created');
   }
   else {
       echo $this->email->print_debugger();

   }
}

function payment(){
 $userid = $this->session->userdata('id');
 $id = $this->input->post('event');
 $class = $this->input->post('class');
 $data['amount']= $class;

 if(!empty($this->session->userdata('id'))){
    $data['ratinguser'] = $this->Meventdetails->getRatingsUser($id, $this->session->userdata('id'));
}
$data['events'] = $this->Meventdetails->getDetails($id);
$data['prices'] = $this->Meventdetails->getPrices($id);
$data['bookings'] = $this->Meventdetails->getBookings($id);
$data['ratings'] = $this->Meventdetails->getRatings($id);
$data['reviews'] = $this->Meventdetails->getReviews($id);
$this->load->view('layout/header_inc');
$this->load->view('layout/top_header');
$this->load->view('layout/nav');
$this->load->view('event/payemnt', $data);
$this->load->view('layout/footer');
$this->load->view('layout/footer_inc');
}

public function check()
{

    $amount =  $this->input->post('amount');
    $product_info = $this->input->post('productinfo');
    $customer_name = $this->input->post('firstname');
    $customer_emial = $this->input->post('email');
    $customer_mobile = $this->input->post('phone');

            //payumoney details


            $MERCHANT_KEY = "RqB5H8hl"; //change  merchant with yours
            $SALT = "gBWMfzG3nn";  //change salt with yours 

            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            //optional udf values 
            $udf1 = '';
            $udf2 = '';
            $udf3 = '';
            $udf4 = '';
            $udf5 = '';
            
            $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
            $hash = strtolower(hash('sha512', $hashstring));

            $success = base_url() . 'index.php/Status';  
            $fail = base_url() . 'index.php/Status';
            $cancel = base_url() . 'index.php/Status';
            
            
            $data = array(
                'mkey' => $MERCHANT_KEY,
                'tid' => $txnid,
                'hash' => $hash,
                'amount' => $amount,           
                'name' => $customer_name,
                'productinfo' => $product_info,
                'mailid' => $customer_emial,
                'phoneno' => $customer_mobile,
                'action' => "https://sandboxsecure.payu.in/_payment", //for live change action  https://secure.payu.in
                'sucess' => $success,
                'failure' => $fail,
                'cancel' => $cancel            
            );

            $this->load->view('layout/header_inc');
            $this->load->view('layout/top_header');
            $this->load->view('layout/nav');
            $this->load->view('confirmation', $data);   
            $this->load->view('layout/footer');
            $this->load->view('layout/footer_inc');

        }


    }