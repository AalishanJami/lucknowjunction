<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Meventdetails');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        //$this->load->database(); // load database
    }

    public function index($id)
    {       
        $data['bookings'] = $this->Meventdetails->getBookings($id);
        $data['ratings'] = $this->Meventdetails->getRatings($id);
        $data['reviews'] = $this->Meventdetails->getReviews($id);
             $this->session->set_userdata('event_id', $id);
            $this->load->view('eventside/layout/header_inc');
            $this->load->view('eventside/layout/top_header');
            $this->load->view('eventside/layout/nav');
            $this->load->view('eventside/dashboard/dashboard', $data);
            $this->load->view('eventside/layout/footer_inc');
        }

    }