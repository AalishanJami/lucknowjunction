<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class UserBookings extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MUserBookings');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {       
            $id = $this->session->userdata('event_id');
            $data['items']=$this->MUserBookings->fetch($id);
            $this->load->view('eventside/layout/header_inc');
            $this->load->view('eventside/layout/top_header');
            $this->load->view('eventside/layout/nav');
            $this->load->view('eventside/userbookings/userbookings',$data);
            $this->load->view('eventside/layout/footer_inc');

    }

     public function active($id){

        $data = array(
            'booking_status'=> '1'
        );

        $this->MUserBookings->active($id, $data);
        redirect('eventside/UserBookings/index');
    }

    public function deactive($id){


        $data = array(
            'booking_status'=> '0'
        );
         $this->MUserBookings->active($id, $data);
        redirect('eventside/UserBookings/index');
    }

    }