<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class MyEvents extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mbecome');
        $this->load->model('Mmyevents');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('MEventProfile');
        $this->load->database(); // load database
    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $data['all'] = $this->Mmyevents->getEvents($id);
        $this->load->view('layout/header_inc');
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('event/myevents',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');
    }

    public function delete($id)
    {
         $this->Mmyevents->delete($id);
         redirect('MyEvents');
       
    }

     public function edit($id)
    {
        $data['country']=$this->Mbecome->getCountry();
        $data['city']=$this->Mbecome->getCity();
        $data['state']=$this->Mbecome->getState();
        $data['category']=$this->Mbecome->getCategory();
        $data['events'] = $this->MEventProfile->getDetails($id);
        $data['prices'] = $this->MEventProfile->getPrices($id);
        $this->load->view('layout/header_inc');
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('editevent', $data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');   
    }


    public function update($id)
    {

        $name = $this->input->post('name');
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $category = $this->input->post('category');
        $chief_guest = $this->input->post('chief_guest');
        $country = $this->input->post('country');
        $description = $this->input->post('description');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
       
            $data = array(
                'event_name' => $name,
                'event_date' => $date,
                'event_time' => $time,
                'category_id' => $category,
                'event_chiefGuest' => $chief_guest,     
                'country_id' => $country,
                'event_description' => $description,
                'city_id' => $city,
                'state_id' => $state
            );

        $this->Mmyevents->update($id, $data);
        redirect('MyEvents');
    }
}