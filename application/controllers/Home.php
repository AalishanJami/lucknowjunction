<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mhome');
        $this->load->model('Mdashboard');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
        $this->load->library('facebook');
        $this->load->library('google');
    }

    public function index()
    {     
        $this->session->set_userdata('facebook_auth', $this->facebook->login_url());
        $this->session->set_userdata('google_auth', $this->google->loginURL());

        // BY DEFAULT THE CITY NAME IS LUCKNOW
        
        

        // IF THERE IS ANY POSTED DATA FOR CITY,  CITY NAME WILL CHANGE TO THAT CITY NAME. SEARCH QUERY IS OKAY. YOU JUST HAVE TO POST CITY DATA WHEN USER SELECT IT. SAME FOR CATEGORY. BUT CATEGORY WONT HAVE ANY INITIALIZING VALUE
        if(!empty($this->input->post('city'))){
            $city = $this->input->post('city');
        }
        else {
            $city = 'Lucknow';
        }

        if(!empty($this->input->post('category'))){
            $category = $this->input->post('category');
        } else {
            $category ='0';
        }

        $data['city_selected'] = $city;
        $data['category_selected'] = $category;     

       
        $data['test'] = $city;
$data['results'] = 1;
$data['events_by_city'] = $this->Mhome->checkPricise($city);
        
        

        if ($category != "0" && !empty($this->input->post('city')) && $city != "0" ){
            $data['events'] = $this->Mhome->getCityandCat($data['events_by_city'][0]->city_id, $category);
             $total = count((array)$data['events']); 
             if($total <= 0){
                $data['results'] = 0;
            }
        }
        else if(empty($this->input->post('city')) && !empty($this->input->post('category'))) {
            $data['events'] = $this->Mhome->getEventbyCategory($category);
            $total = count((array)$data['events']); 
             if($total <= 0){
                $data['results'] = 0;
            }
        }
            else{
                 
                $total = count((array)$data['events_by_city']); 
if($total > 0){
                $data['events'] = $this->Mhome->getAllEventsByCity($data['events_by_city'][0]->city_id);
                $data['results'] = 1;
            } else {
                $data['results'] = 0;
            }
        }

            $data['users']=$this->Mdashboard->getUsers();
            $data['cities']=$this->Mdashboard->getCities();
            $data['city']=$this->Mhome->getCity();
            $data['eventcount']=$this->Mdashboard->getEvents();
            $data['category']=$this->Mhome->getCategory();
            $data['country']=$this->Mhome->getCountry();
            $data['cityname'] = $city;
            $data['eventsnewyear']= $this->Mhome->getEventbyNewYear();
            $data['eventsfd']= $this->Mhome->getEventbyFD();

            $this->load->view('layout/header_inc');
            $this->load->view('layout/top_header', $data);
            $this->load->view('layout/nav');
            $this->load->view('home/home',$data);
            $this->load->view('layout/footer');
            $this->load->view('layout/footer_inc');

        }
        public function search()
        {
            $category = $this->input->post('category');
            $city = $this->input->post('city');
            $name = $this->input->post('name');
            $cityname = $this->input->post('cityname');


            if($city == "90000"){
                $data['check'] = $this->Mhome->checkPricise($cityname);
                $total = count((array)$data['check']);
                if($total > 0){
                    $data['results'] = $this->Mhome->getCityPrecise($city);
                }
                else{
                    $data['results'] = 0;
                }
            } else if($category != "0"  && $city != "0" && $name != ""){
               $data['results'] = $this->Mhome->searchAll($category, $cityname, $name);
               $total = count((array)$data['results']);
               if($total > 0){
                $data['results'] = $this->Mhome->searchAll($category, $city, $name);
            }
            else{
               $data['results'] = 0;
           }
       }

       elseif($category == "0"  && $city == "0"){
        $data['results'] = $this->Mhome->searchName($name);
        $total = count((array)$data['results']);
        if($total > 0){
         $data['results'] = $this->Mhome->searchName($name);
     }
     else{
        $data['results'] = 0;
    }
}
elseif($category != "0"  && $city == "0"){
    $data['results'] = $this->Mhome->searchCategory($category, $name);
    $total = count((array)$data['results']);
    if($total > 0){
     $data['results'] = $this->Mhome->searchCategory($category, $name);
 }
 else{
    $data['results'] = 0;
}

}
elseif($category == "0"  && $city != "0"){
    $data['results'] = $this->Mhome->searchCity($city, $name);
    $total = count((array)$data['results']);
    if($total > 0){
     $data['results'] = $this->Mhome->searchCity($city, $name);
 }
 else{
    $data['results'] = 0;
}

}

elseif($category != "0"  && $city != "0" && $name ==""){
    $data['results'] = $this->Mhome->searchCat($city, $category);
    $total = count((array)$data['results']);
    if($total > 0){
     $data['results'] = $this->Mhome->searchCat($city, $category);
 }
 else{
    $data['results'] = 0;
}

}
else{
    echo "no condition";
}

$data['city']=$this->Mhome->getCity();
$data['category']=$this->Mhome->getCategory();
$data['country']=$this->Mhome->getCountry();

$this->load->view('layout/header_inc');
$this->load->view('layout/top_header');
$this->load->view('layout/nav');
$this->load->view('search/search',$data);
$this->load->view('layout/footer');
$this->load->view('layout/footer_inc');
}

public function logout(){
    $this->session->unset_userdata('id');
    redirect("Home");
}



}