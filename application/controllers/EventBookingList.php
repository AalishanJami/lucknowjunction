<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class EventBookingList extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Meventbookinglist');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        //$this->load->database(); // load database
    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $data['bookings']= $this->Meventbookinglist->getBookings($id);
    
        $this->load->view('layout/header_inc');
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('event/eventbooking', $data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');
    }

    }