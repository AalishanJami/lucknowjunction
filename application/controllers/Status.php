<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {
	public function __construct() {
    parent::__construct();
    $this->load->helper('url');   
    $this->load->model('Meventdetails');
    $this->load->model('MProfile');
    $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->library('pagination');
    $this->load->library('email');
    $this->load->database();   
  }

  public function index() {
   $status = $this->input->post('status');
   if (empty($status)) {
    redirect('Welcome');
  }

  $firstname = $this->input->post('firstname');
  $amount = $this->input->post('amount');
  $txnid = $this->input->post('txnid');
  $posted_hash = $this->input->post('hash');
  $key = $this->input->post('key');
  $productinfo = $this->input->post('productinfo');
  $email = $this->input->post('email');
        $salt = "dxmk9SZZ9y"; //  Your salt
        $add = $this->input->post('additionalCharges');
        if (isset($add)) {
          $additionalCharges = $this->input->post('additionalCharges');
          $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {

          $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $data['hash'] = hash("sha512", $retHashSeq);
        $data['amount'] = $amount;
        $data['txnid'] = $txnid;
        $data['posted_hash'] = $posted_hash;  
        $data['status'] = $status;
        $data['firstname'] = $firstname;
        $data['productinfo'] = $productinfo;
        $data['email'] = $email;
        $data['add'] = $add;

          $splitTokens = explode(",", $productinfo); // this is what "split" is in other languages, splits an spring by a SEP and retrieving an array
          $eventid = $splitTokens[0];
          $custid = $splitTokens[1];


          $insert = array(
            'trxid' => $txnid,
            'user_id' => $custid,
            'event_id' => $eventid,
            'amount_paid' => $amount,
            'booking_status' => '1',
          );

          $this->Meventdetails->savePayment($insert);

          $data['event_details'] = $this->Meventdetails->getDetails($eventid);
          $data['cust_details'] = $this->MProfile->fetch($custid);


          if($status == 'success'){

            $this->customer($data);   
            // $this->admin($data['cust_details']); 
            // $this->event($data['event_details']); 
          }
          else{
           $this->load->view('layout/header_inc');
           $this->load->view('layout/top_header');
           $this->load->view('layout/nav');
           $this->load->view('failure', $data); 
           $this->load->view('layout/footer');
           $this->load->view('layout/footer_inc');

         }

       }

       function customer($data){
        $from_email = "info@lucknowjunction.com"; 
        $to_email = $data['cust_details'][0]->user_email;
        $this->email->from($from_email, 'Lucknowjunction'); 
        $this->email->to($to_email);
        $this->email->subject('Your Event has been booked!'); 
        $this->email->message('Thanks '.$data['cust_details'][0]->user_name.'
          Your Booking is Confirmed.
          The details for ticket are as follows:
          Booking ID: '.$data['txnid'].'
          Event Name:  '.$data['event_details'][0]->event_name.'
          Date: '.$data['event_details'][0]->event_date.'
          Time: '.$data['event_details'][0]->event_time.'
          Venue: '.$data['event_details'][0]->city_name.'
          Quantity : 1
          Paid: : '.$data['amount'].'
          Payment Method: Card

          For any queries Contact us at:
          www.lucknowjunction.com
          support@lucknowjunction.com'); 

         //Send mail 
        if($this->email->send()) {
          $this->organizer($data);
        }
        else {
         echo $this->email->print_debugger();

       }
     }

     function organizer($data){
        $from_email = "info@lucknowjunction.com"; 
         $data['get_registered_user'] = $this->MProfile->fetch($data['event_details'][0]->organizer_id);
        $to_email = $data['get_registered_user'][0]->user_email;

        $this->email->from($from_email, 'Lucknowjunction'); 
        $this->email->to($to_email);
        $this->email->subject('Your Event has been booked!'); 
        $this->email->message('Dear '.$data['cust_details'][0]->user_name.'
          Congratulations on New Booking!
          The details for ticket are as follows:
          Booking ID:  '.$data['txnid'].'
          Event Name:  '.$data['event_details'][0]->event_name.'
          Date: '.$data['event_details'][0]->event_date.'
          Time: '.$data['event_details'][0]->event_time.'
          Venue: '.$data['event_details'][0]->city_name.'
          Quantity : 1
          Paid: : '.$data['amount'].'
          Payment Method: Card

          For any queries Contact us at:
          www.lucknowjunction.com
          support@lucknowjunction.com'); 

         //Send mail 
        if($this->email->send()) {
          $this->admin($data);
        }
        else {
         echo $this->email->print_debugger();

       }
     }

     function admin($data){
    
        $from_email = "info@lucknowjunction.com"; 
        $to_email = "admin@lucknowjunction.com";
        $this->email->from($from_email, 'Lucknowjunction'); 
        $this->email->to($to_email);
        $this->email->subject('Your Event has been booked!'); 
        $this->email->message('New Booking!
          The details for ticket are as follows:
          Booking ID:  '.$data['txnid'].'
          Event Name:  '.$data['event_details'][0]->event_name.'
          Date: '.$data['event_details'][0]->event_date.'
          Time: '.$data['event_details'][0]->event_time.'
          Venue: '.$data['event_details'][0]->city_name.'
          Quantity : 1
          Paid: : '.$data['amount'].'
          Payment Method: Card

          For any queries Contact us at:
          www.lucknowjunction.com
          support@lucknowjunction.com'); 

         //Send mail 
        if($this->email->send()) {
          $this->success($data);
        }
        else {
         echo $this->email->print_debugger();

       }
     }

     function success($data){
       $this->load->view('layout/header_inc');
       $this->load->view('layout/top_header');
       $this->load->view('layout/nav');
       $this->load->view('success', $data);
       $this->load->view('layout/footer');
       $this->load->view('layout/footer_inc');
     }
   }
