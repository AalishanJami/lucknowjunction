<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class UserFeedbackManagement extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Muserfeedbackmanagement');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
         if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
        $data['items']=$this->Muserfeedbackmanagement->fetch();
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/feedbackmanagement/feedbackmanagement', $data);
            $this->load->view('admin/layout/footer_inc');
        }

        public function delete($id){
            $this->Muserfeedbackmanagement->delete($id);
            redirect('admin/BookingManagement/index');
        }
    
        public function reply($id){
            $data['items']=$this->Muserfeedbackmanagement->fetch_user($id);
            $this->load->view('admin/layout/header_inc');
                $this->load->view('admin/layout/top_header');
                $this->load->view('admin/layout/nav');
                $this->load->view('admin/editform/reply',$data);
                $this->load->view('admin/layout/footer_inc');
        }

        public function add(){

            $reply = $this->input->post('reply');
            $feedbackid = $this->input->post('feedbackid');
            $userid = $this->input->post('user_id');
    
            $data = array(
                'replies_content' => $reply,
                'replies_feedbackid'=> $feedbackid,
                'replies_userid'=> $userid
            );
    
            $this->Muserfeedbackmanagement->insert($data);
            redirect('admin/UserFeedbackManagement');
        }

    }