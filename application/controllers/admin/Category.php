<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Categoriesmodel');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database(); // load database
    }

    public function index()
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
        $data['items']=$this->Categoriesmodel->fetch();
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/category/category',$data);

            $this->load->view('admin/layout/footer_inc');
        }

     public function add(){

        if($this->input->post('save'))
{
     $arraypath = explode(".", strtolower($_FILES['userfile']['name']));
        $extension = end($arraypath);
        $image = time().mt_rand(1, 99).'.'.$extension;
        // configurations from upload library
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048000'; // max size in KB
        $config['max_width'] = '20000'; //max resolution width
        $config['max_height'] = '20000';  //max resolution height
        $config['file_name'] = $image;
      
        // load CI libarary called upload
        $this->load->library('upload', $config);
        // body of if clause will be executed when image uploading is failed
        if (!$this->upload->do_upload()) {
            $errors = array('error' => $this->upload->display_errors());
            // This image is uploaded by deafult if the selected image in not uploaded
            $image = 'no_image.png';
        } // body of else clause will be executed when image uploading is succeeded
        else {
            $data = array('upload_data' => $this->upload->data());
        }

     $name = $this->input->post('name');
     $desc = $this->input->post('desc');
        $data = array(
            'category_name' => $name,
            'category_status'=> '1',
            'category_img' => $image,
            'category_desc' => $desc
        );

        $this->Categoriesmodel->insert($data);
        $this->session->set_flashdata('success', 'Category Created');
        redirect('admin/Category/index');
    }
    else{
        $this->session->set_flashdata('error', 'Somthing went wrong.');
        redirect('admin/Category/index');
    }
    }


    public function savedata()
    {
    
        //Check submit button 
        if($this->input->post('savecateg'))
        {
        //get form's data and store in local varable
        $n=$this->input->post('txt_categoryname');
     
    
        $this->Categoriesmodel->saverecords($n);  
        $this->session->set_flashdata('success', 'Category Created');      
        redirect("admin/Category");
        
        }
            else{
        $this->session->set_flashdata('error', 'Somthing went wrong.');
        redirect('registration');
    }
    }
     public function display()
     {
       $this->load->model('Categoriesmodel');
    // $data['category']=$this->Categoriesmodel->displayrecords();
     $this->load->view('admin/category/category',$data);
    
     }
     public function active($id){

        $data = array(
            'category_status'=> '1'
        );

        $this->Categoriesmodel->active($id, $data);
        redirect('admin/Category/index');
    }

    public function deactive($id){


        $data = array(
            'category_status'=> '0'
        );

        $this->Categoriesmodel->deactive($id, $data);
        redirect('admin/Category/index');
    }

    public function delete($id){
        $this->Categoriesmodel->delete($id);
        redirect('admin/Category/index');
    }

    public function update($id){
        $edit = $this->input->post('edit');
        $data = array(
            'category_name' => $edit
        );

        $this->Categoriesmodel->update($id, $data);
        redirect('admin/Category/index');
    }
    }