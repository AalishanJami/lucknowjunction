<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 
 * Time: 8:59 P
 */
class Country extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mcountry');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index() 
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
        $data['items']=$this->Mcountry->fetch();
        $this->load->view('admin/layout/header_inc');
        $this->load->view('admin/layout/top_header');
        $this->load->view('admin/layout/nav');
        $this->load->view('admin/location/country', $data);
        $this->load->view('admin/layout/footer_inc');
    }

    public function add(){

        $name = $this->input->post('name');

        $data = array(
            'country_name' => $name,
            'status'=> '1'
        );

        $this->Mcountry->insert($data);
        redirect('admin/location/Country/index');
    }

    public function active($id){

        $data = array(
            'status'=> '1'
        );

        $this->Mcountry->active($id, $data);
        redirect('admin/location/Country/index');
    }

    public function deactive($id){

        $data = array(
            'status'=> '0'
        );

        $this->Mcountry->deactive($id, $data);
        redirect('admin/location/Country/index');
    }

    public function delete($id){
        $this->Mcountry->delete($id);
        redirect('admin/location/Country/index');
    }

    public function update($id){
        $edit = $this->input->post('edit');
        $data = array(
            'country_name' => $edit
        );

        $this->Mcountry->update($id, $data);
        redirect('admin/location/Country/index');
    }



    }