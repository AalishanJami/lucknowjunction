<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class City extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mcity');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index() 
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
        $data['items']=$this->Mcity->fetch();
        $this->load->view('admin/layout/header_inc');
        $this->load->view('admin/layout/top_header');
        $this->load->view('admin/layout/nav');
        $this->load->view('admin/location/city', $data);
        $this->load->view('admin/layout/footer_inc');
    }

    public function add(){

        $name = $this->input->post('name');

        $data = array(
            'city_name' => $name,
            'city_status'=> '1'
        );

        $this->Mcity->insert($data);
        redirect('admin/location/City/index');
    }

    public function active($id){

        $data = array(
            'city_status'=> '1'
        );

        $this->Mcity->active($id, $data);
        redirect('admin/location/City/index');
    }

    public function deactive($id){

        $data = array(
            'city_status'=> '0'
        );

        $this->Mcity->deactive($id, $data);
        redirect('admin/location/City/index');
    }

    public function delete($id){
        $this->Mcity->delete($id);
        redirect('admin/location/City/index');
    }

    public function update($id){
        $edit = $this->input->post('edit');
        $data = array(
            'city_name' => $edit
        );

        $this->Mcity->update($id, $data);
        redirect('admin/location/City/index');
    }



    }