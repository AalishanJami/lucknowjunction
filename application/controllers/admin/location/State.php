<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class State extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mstate');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index() 
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
        $data['items']=$this->Mstate->fetch();
        $this->load->view('admin/layout/header_inc');
        $this->load->view('admin/layout/top_header');
        $this->load->view('admin/layout/nav');
        $this->load->view('admin/location/state', $data);
        $this->load->view('admin/layout/footer_inc');
    }

    public function add(){

        $name = $this->input->post('name');

        $data = array(
            'state_name' => $name,
            'state_status'=> '1'
        );

        $this->Mstate->insert($data);
        redirect('admin/location/State/index');
    }

    public function active($id){

        $data = array(
            'state_status'=> '1'
        );

        $this->Mstate->active($id, $data);
        redirect('admin/location/State/index');
    }

    public function deactive($id){

        $data = array(
            'state_status'=> '0'
        );

        $this->Mstate->deactive($id, $data);
        redirect('admin/location/State/index');
    }

    public function delete($id){
        $this->Mstate->delete($id);
        redirect('admin/location/State/index');
    }

    public function update($id){
        $edit = $this->input->post('edit');
        $data = array(
            'state_name' => $edit
        );

        $this->Mstate->update($id, $data);
        redirect('admin/location/State/index');
    }



    }