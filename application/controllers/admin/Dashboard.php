<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mdashboard');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
   $data['events']=$this->Mdashboard->getEvents();
   $data['users']=$this->Mdashboard->getUsers();
   $data['reviews']=$this->Mdashboard->getReviews();
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/dashboard/dashboard', $data);
            $this->load->view('admin/layout/footer_inc');
    }

    function logout(){
        $this->session->unset_userdata('admin_name');
        redirect('admin/Signin');
    }

    }