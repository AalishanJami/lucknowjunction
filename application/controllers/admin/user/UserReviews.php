<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * Use: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class UserReviews extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Muserreviews');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
        $data['items']=$this->Muserreviews->fetch();
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/usermanage/userreviews', $data);
            $this->load->view('admin/layout/footer_inc');
        }

        public function active($id){

            $data = array(
                'isActive'=> '1'
            );
    
            $this->Muserreviews->active($id, $data);
            redirect('admin/user/UserReviews/index');
        }
    
        public function deactive($id){
    
            $data = array(
                'isActive'=> '0'
            );
    
            $this->Muserreviews->deactive($id, $data);
            redirect('admin/user/UserReviews/index');
        }
    
        public function delete($id){
            $this->Muserreviews->delete($id);
            redirect('admin/user/UserReviews/index');
        }
    
        // public function loadedit($id){
        //     $data['items']=$this->Muserreviews->fetch_user($id);
        //     $this->load->view('admin/layout/header_inc');
        //         $this->load->view('admin/layout/top_header');
        //         $this->load->view('admin/layout/nav');
        //         $this->load->view('admin/usermanage/edit_user', $data);
        //         $this->load->view('admin/layout/footer_inc');
        // }
    
        // public function update($id){
        //     $name = $this->input->post('name');
        //     $email = $this->input->post('email');
        //     $phone = $this->input->post('phone');
        //     $fb = $this->input->post('fb');
        //     $insta = $this->input->post('insta');
        //     $data = array(
        //         'user_name' => $name,
        //         'user_email' => $email,
        //         'user_contact' => $phone,
        //         'user_fb' => $fb,
        //         'user_insta' => $insta
        //     );
    
        //     $this->Muserreviews->update($id, $data);
        //     redirect('admin/user/UserReviews/index');
        // }

    }