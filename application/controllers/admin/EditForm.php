<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class EditForm extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('MHome');
        $this->load->model('Eventsmodel');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
    $data['items']=$this->Eventsmodel->displayrecords();
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/editform/editform');
            $this->load->view('admin/layout/footer_inc');
        }

    public function loadedit($id){
        $data['items']=$this->Eventsmodel->fetch_user($id);
        $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/editform/editform', $data);
            $this->load->view('admin/layout/footer_inc');
    }

    public function update($id){
        $name = $this->input->post('eventname');
        $location = $this->input->post('eventlocation');
        $guest = $this->input->post('chiefguest');
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $description = $this->input->post('description');

        $data = array(
            'event_name' => $name,
            'location' => $location,
            'event_chiefGuest' => $guest,
            'event_date' => $date,
            'event_time' => $time,
            'event_description' => $description
        );

        $this->Eventsmodel->update($id, $data);
        redirect('admin/eventmanagement/EventManagement');
    }

    public function sold($id){

        $data = array(
            'is_sold' => "1",
        );

        $this->Eventsmodel->sold($id, $data);
        redirect('admin/eventmanagement/EventManagement');
    }

    public function unsold($id){

        $data = array(
            'is_sold' => "0",
        );

        $this->Eventsmodel->unsold($id, $data);
        redirect('admin/eventmanagement/EventManagement');
    }

    public function delete($id){
        $this->Eventsmodel->delete($id);
        redirect('admin/eventmanagement/EventManagement/index');
    }

    

    
}