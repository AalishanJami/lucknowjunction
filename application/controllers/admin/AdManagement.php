<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class AdManagement extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('MHome');

         $this->load->model('Madmanagement');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
            $data['items']=$this->Madmanagement->fetch();
         
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/admanagement/admanagement',$data);
            $this->load->view('admin/layout/footer_inc');
        }

    public function active($id){

        $data = array(
            'Ad_isActive'=> '1'
        );

        $this->Madmanagement->active($id, $data);
        redirect('admin/AdManagement/index');
    }

    public function deactive($id){

        $data = array(
            'Ad_isActive'=> '0'
        );

        $this->Madmanagement->deactive($id, $data);
        redirect('admin/AdManagement/index');
    }

    public function delete($id){
        $this->Madmanagement->delete($id);
        redirect('admin/AdManagement/index');
    }

    public function update($id){
        $edit = $this->input->post('edit');
        $data = array(
            'ad_name' => $edit
        );

        $this->Madmanagement->update($id, $data);
        redirect('admin/AdManagement/index');
    }

    public function add(){   
            $arraypath = explode(".", strtolower($_FILES['userfile']['name']));
        $extension = end($arraypath);
        $image = time().mt_rand(1, 99).'.'.$extension;
        // configurations from upload library
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048000'; // max size in KB
        $config['max_width'] = '20000'; //max resolution width
        $config['max_height'] = '20000';  //max resolution height
        $config['file_name'] = $image;
      
        // load CI libarary called upload
        $this->load->library('upload', $config);
        // body of if clause will be executed when image uploading is failed
        if (!$this->upload->do_upload()) {
            $errors = array('error' => $this->upload->display_errors());
            // This image is uploaded by deafult if the selected image in not uploaded
            $image = 'no_image.png';
        } // body of else clause will be executed when image uploading is succeeded
        else {
            $data = array('upload_data' => $this->upload->data());
        }
    $name = $this->input->post('name');
    $type = $this->input->post('loc');
        
        $data = array(
            'ad_name' => $name,
            'Ad_isActive' =>'1',
            'ad_img' => $image,
            'ad_type' => $type
        );


        $this->Madmanagement->insert($data);
    
        redirect('admin/AdManagement/index');
}


    }