<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class BookingManagement extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mbookingmanagement');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
        $data['items']=$this->Mbookingmanagement->fetch();
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/bookingmanagement/bookingmanagement', $data);
            $this->load->view('admin/layout/footer_inc');
        }

        public function active($id){

            $data = array(
                'booking_status'=> '1'
            );
    
            $this->Mbookingmanagement->active($id, $data);
            redirect('admin/BookingManagement/index');
        }
    
        public function deactive($id){
    
            $data = array(
                'booking_status'=> '0'
            );
    
            $this->Mbookingmanagement->deactive($id, $data);
            redirect('admin/BookingManagement/index');
        }
    
        public function delete($id){
            $this->Mbookingmanagement->delete($id);
            redirect('admin/BookingManagement/index');
        }
    
        // public function loadedit($id){
        //     $data['items']=$this->Mbookingmanagement->fetch_user($id);
        //     $this->load->view('admin/layout/header_inc');
        //         $this->load->view('admin/layout/top_header');
        //         $this->load->view('admin/layout/nav');
        //         $this->load->view('admin/usermanage/edit_user', $data);
        //         $this->load->view('admin/layout/footer_inc');
        // }
    
        // public function update($id){
        //     $name = $this->input->post('name');
        //     $email = $this->input->post('email');
        //     $phone = $this->input->post('phone');
        //     $fb = $this->input->post('fb');
        //     $insta = $this->input->post('insta');
        //     $data = array(
        //         'user_name' => $name,
        //         'user_email' => $email,
        //         'user_contact' => $phone,
        //         'user_fb' => $fb,
        //         'user_insta' => $insta
        //     );
    
        //     $this->Mbookingmanagement->update($id, $data);
        //     redirect('admin/user/UserReviews/index');
        // }

    }