<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Changepass extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mchangepass');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }

            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/settings/changepassword');
            $this->load->view('admin/layout/footer_inc');
        }

       function change(){
        $old = $this->input->post('old');
        $new = $this->input->post('new');
        $username = $this->session->userdata('admin_username');
        $data['count'] = $this->Mchangepass->fetch($old);
        var_dump($old, $new);

        if($data['count'] == 0){
            redirect('admin/settings/Changepass?auth=no');
        }
        else {
            $pass = array(
                'password'=> $new
            );
    
            $this->Mchangepass->change($pass, $username);
            redirect('admin/Dashboard');
        }
        }
    }