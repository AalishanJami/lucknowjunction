<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class EventManagement extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('MHome');
         $this->load->model('Eventsmodel');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
        if(empty($this->session->userdata('admin_name'))){
            redirect("admin/Signin");
        }
         $data['items']=$this->Eventsmodel->displayrecords();
         $this->load->view('admin/event/event',$data);
            $this->load->view('admin/layout/header_inc');
            $this->load->view('admin/layout/top_header');
            $this->load->view('admin/layout/nav');
            $this->load->view('admin/layout/footer_inc');
        }

    public function display()
     {
   //  $data['events']=$this->Eventsmodel->displayrecords();
     $this->load->view('admin/event/event',$data);
    
     }
     public function active($id){

        $data = array(
            'event_status'=> '1'
        );

        $this->Eventsmodel->active($id, $data);
        redirect('admin/eventmanagement/EventManagement/index');
    }

    public function deactive($id){


        $data = array(
            'event_status'=> '0'
        );

        $this->Eventsmodel->deactive($id, $data);
        redirect('admin/eventmanagement/EventManagement/index');
    }

    public function delete($id){
        $this->Eventsmodel->delete($id);
        redirect('admin/eventmanagement/EventManagement/index');
    }

    public function update($id){
        $edit = $this->input->post('edit');
        $data = array(
            'country_name' => $edit
        );

        $this->MCountry->update($id, $data);
        redirect('admin/location/Country/index');
    }

    }