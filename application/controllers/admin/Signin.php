<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Signin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mlogin');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
        if(!empty($this->session->userdata('admin_name'))){
            redirect("admin/Dashboard");
        }
        else{
        $this->load->view('admin/profile/signin');
        }
    }

    public function auth()
    {
        
            $email = $this->input->post('username');
            $password = $this->input->post('password');
            $this->data['user'] = $this->Mlogin->adminAuth($email);
            if (count($this->data['user']) > 0){
                foreach ($this->data['user'] as $users) {
                    $username = $users->username;
                    $name = $users->name;                
                    $passworddb = $users->password;
                }
    
                // echo $password;
                // echo $passworddb;
        
                if ($passworddb == $password) {
                    $this->session->set_userdata('admin_name', $name);
                    $this->session->set_userdata('admin_username', $username);
                    
                    redirect("admin/Dashboard");
    
                } else {
         
                    redirect('admin/Signin?auth=no');
                }
            } 
            else {
         
                redirect('admin/Signin?auth=no');
            }
        
    }

    }