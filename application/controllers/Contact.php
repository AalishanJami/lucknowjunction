<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Tie: 8:59 PM
 */
class Contact extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mcontact');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {

            $this->load->view('layout/header_inc');
            $this->load->view('layout/top_header');
            $this->load->view('layout/nav');
            $this->load->view('contact/contact');
            $this->load->view('layout/footer');
            $this->load->view('layout/footer_inc');
        }

    public function insert(){

        $message = $this->input->post('message');
        $id = $this->session->userdata('id');


        $data = array(
            'feedback_msg' => $message,
            'user_id'=> $id
        );

        

        $this->Mcontact->insert($data);
        var_dump($data);
        // redirect('Home');
    }
    public function replies(){
          $id = $this->session->userdata('id');
      $data['items'] = $this->Mcontact->fetch($id);
      
      $this->load->view('layout/header_inc');
      $this->load->view('layout/top_header');
      $this->load->view('layout/nav');
      $this->load->view('contact/contactreplies', $data);
      $this->load->view('layout/footer');
      $this->load->view('layout/footer_inc');

     }
     
    }