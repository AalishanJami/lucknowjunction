<?php

    class Eventsc extends CI_Controller{
       
        public function __construct()
    {
        parent::__construct();
        $this->load->database();
         $this->load->model('Eventsm');
         $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');

    }
        function index()
       
    {
    	  $this->load->helper('url');

        //load model
        $this->load->model('Eventsm');

        // Get 
        $edit = $this->input->get('edit'); 

        if(!isset($edit)){
            // get data
            $data['response'] = $this->Eventsm->getEventsList();
            $data['view'] = 1;

            // load view
            $this->load->view('event/eventsv',$data);
        }else{

            // Check submit button POST or not
            if($this->input->post('submit') != NULL ){
                // POST data
                $postData = $this->input->post();

                //load model
                $this->load->model('Eventsm');

                // Update record
                $this->Eventsm->updateEvent($postData,$edit);

                // Redirect page
                redirect('eventsc/');

            }else{

                // get data by id
                $data['response'] = $this->Eventsm->getEventById($edit);
                $data['view'] = 2;

                // load view
                $this->load->view('event/eventsv',$data);

            }
        }
    }

            // Function to Delete selected record from database.
     function deletedata($id)
	{
	
	if($this->Eventsm->deleterecords($id)){
	echo "ok";
	}
	// redirect("Eventsc/index");
	}

        }
