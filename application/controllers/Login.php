<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mlogin');
        $this->load->model('Mregister');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
        $this->load->library('email');
        $this->load->library('facebook');
        $this->load->library('google');
    }

    public function index()
    {   
        $this->session->set_userdata('facebook_auth', $this->facebook->login_url());
        $this->session->set_userdata('google_auth', $this->google->loginURL());
        $data['fb_auth_url'] = $this->facebook->login_url();
        $data['google_auth_url'] = $this->google->loginURL();
        $this->load->view('layout/header_inc');
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('profile/login', $data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');
    }

    function oAuthGoogle(){
    // Redirect to profile page if the user already logged in
        // if($this->session->userdata('loggedIn') == true){
        //     redirect('user_authentication/profile/');
        // }

        if(isset($_GET['code'])){

            // Authenticate user with google
            if($this->google->getAuthenticate($_GET['code'])){

                // Get user info from google
                $gpInfo = $this->google->getUserInfo();
                
                // Preparing data for database insertion
                $userData['oauth_provider'] = 'google';
                $userData['oauth_uid']      = $gpInfo['id'];
                $userData['first_name']     = $gpInfo['given_name'];
                $userData['last_name']      = $gpInfo['family_name'];
                $userData['email']          = $gpInfo['email'];
                $userData['gender']         = !empty($gpInfo['gender'])?$gpInfo['gender']:'';
                $userData['locale']         = !empty($gpInfo['locale'])?$gpInfo['locale']:'';
                $userData['link']           = !empty($gpInfo['link'])?$gpInfo['link']:'';
                $userData['picture']        = !empty($gpInfo['picture'])?$gpInfo['picture']:'';

                $data['user_data'] = $this->Mlogin->emailExists($userData['email']); 
                if (count($data['user_data']) > 0){
                    foreach ($data['user_data'] as $users) {
                        $name = $users->user_name;
                        $email = $users->user_email;
                        $user_fb = $users->user_fb;
                        $user_contact = $users->user_contact;
                        $user_id = $users->user_id;
                        $user_img = $users->user_img;
                        $user_insta = $users->user_insta;
                        $passworddb = $users->user_password;
                        $site_url = $users->site_url;
                    }
                    $this->session->set_userdata('name', $name);
                    $this->session->set_userdata('email', $email);
                    $this->session->set_userdata('fb', $user_fb);
                    $this->session->set_userdata('contact', $user_contact);
                    $this->session->set_userdata('id', $user_id);
                    $this->session->set_userdata('img', $user_img);
                    $this->session->set_userdata('insta', $user_insta);
                    $this->session->set_userdata('site_url', $site_url);

                    redirect("Home");

                } 
                else {

                   $data = array(
                    'user_name' => $userData['first_name'],
                    'user_email' =>  $userData['email'],
                    'user_img' =>  $userData['picture'],
                    'is_Active' => "1",
                    'site_url' => 'gl'
                );

                   $usrid = $this->Mregister->insertFb($data);
                   $this->session->set_userdata('id', $usrid);
                   $this->session->set_userdata('name', $userData['first_name']);
                   $this->session->set_userdata('email', $userData['email']);
                   $this->session->set_userdata('img', $userData['picture']);
                   $this->session->set_userdata('site_url', 'gl');

                   redirect("Home");
               }
       } 

   }
}

   function oAuthFacebook(){
         // Check if user is logged in
    if($this->facebook->is_authenticated()){
            // Get user facebook profile details
        $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,picture');
            // Preparing data for database insertion
        $userData['oauth_provider'] = 'facebook';
        $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
        $userData['first_name']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
        $userData['last_name']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
        $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
        $userData['picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            // Insert or update user data
        $data['user_data'] = $this->Mlogin->emailExists($userData['email']); 

        if (count($data['user_data']) > 0){
            foreach ($data['user_data'] as $users) {
                $name = $users->user_name;
                $email = $users->user_email;
                $user_fb = $users->user_fb;
                $user_contact = $users->user_contact;
                $user_id = $users->user_id;
                $user_img = $users->user_img;
                $user_insta = $users->user_insta;
                $passworddb = $users->user_password;
                $site_url = $users->site_url;
            }
            $this->session->set_userdata('name', $name);
            $this->session->set_userdata('email', $email);
            $this->session->set_userdata('fb', $user_fb);
            $this->session->set_userdata('contact', $user_contact);
            $this->session->set_userdata('id', $user_id);
            $this->session->set_userdata('img', $user_img);
            $this->session->set_userdata('insta', $user_insta);
            $this->session->set_userdata('site_url', $site_url);

            redirect("Home");

        } 
        else {

           $data = array(
            'user_name' => $userData['first_name'],
            'user_email' =>  $userData['email'],
            'user_img' =>  $userData['picture'],
            'is_Active' => "1",
            'site_url' => 'fb'
        );

           $usrid = $this->Mregister->insertFb($data);
           $this->session->set_userdata('id', $usrid);
           $this->session->set_userdata('name', $userData['first_name']);
           $this->session->set_userdata('email', $userData['email']);
           $this->session->set_userdata('img', $userData['picture']);
           $this->session->set_userdata('site_url', 'fb');

           redirect("Home");
       }}
}
public function logout() {
        // Remove local Facebook session
    $this->facebook->destroy_session();
    $this->google->revokeToken();
    $this->session->unset_userdata('id');
        // Remove user data from session
    $this->session->unset_userdata('email');
        // Redirect to login page
    $this->session->sess_destroy();
    redirect('Login');
}

public function auth()
{
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $this->data['user'] = $this->Mlogin->emailExists($email);
    if (count($this->data['user']) > 0){
        foreach ($this->data['user'] as $users) {
            $name = $users->user_name;
            $email = $users->user_email;
            $user_fb = $users->user_fb;
            $user_contact = $users->user_contact;
            $user_id = $users->user_id;
            $user_img = $users->user_img;
            $user_insta = $users->user_insta;
            $passworddb = $users->user_password;
        }

        if (password_verify($password, $passworddb)) {
            $this->session->set_userdata('name', $name);
            $this->session->set_userdata('email', $email);
            $this->session->set_userdata('fb', $user_fb);
            $this->session->set_userdata('contact', $user_contact);
            $this->session->set_userdata('id', $user_id);
            $this->session->set_userdata('img', $user_img);
            $this->session->set_userdata('insta', $user_insta);


            redirect("Home");

        } else {
            $this->session->set_flashdata("error_login","Incorrect Pasword"); 
            redirect('Login');
        }
    } 
    else {
        $this->session->set_flashdata("error_login","Incorrect Email"); 
        redirect('Login');
    }
}
function forgotPassword(){
    $this->load->view('layout/header_inc');
    $this->load->view('layout/top_header');
    $this->load->view('layout/nav');
    $this->load->view('profile/forgotpassword');
    $this->load->view('layout/footer');
    $this->load->view('layout/footer_inc');
}


function recover(){

    $from_email = "aalishanjami@intellirobs.com"; 
    $to_email = $this->input->post('email'); 

    $strone =  mt_rand(1, 99);
    $strtwo =  mt_rand(1, 99);
    $strthree =  mt_rand(1, 99);
    $strfour =  mt_rand(1, 99);
    $strfive =  mt_rand(1, 99);
    $strsix =  mt_rand(1, 99);
    $strseven =  mt_rand(1, 99);
    $streight =  mt_rand(1, 99);
    $strnine =  mt_rand(1, 99);
    $strten =  mt_rand(1, 99);

    $token = $strone.$strtwo.$strthree.$strfour.$strfive.$strsix.$strseven.$streight.$strnine.$strten;


    $data = array(
        'token' => $token,
        'email' => $to_email,
    );

    $this->Mlogin->addRecoveryToken($data);

    $this->email->from($from_email, 'Aalishan Jami'); 
    $this->email->to($to_email);
    $this->email->subject('Password Recovery: Lucknowjunction'); 
    $this->email->message('Below is the link to your password reset request. Click to proceed: '.base_url().'index.php/Login/validaterecoverpassword/'.$to_email.'/'.$token); 

         //Send mail 
    if($this->email->send()) {
        $this->session->set_flashdata("success_email","Email sent successfully"); 
            // echo "ok";
    }
    else {
        $this->session->set_flashdata("error_email","Error processing your email, please try again later"); 
            // echo " not ok";

    }
    $this->load->view('layout/header_inc');
    $this->load->view('layout/top_header');
    $this->load->view('layout/nav');
    $this->load->view('profile/forgotpassword');
    $this->load->view('layout/footer');
    $this->load->view('layout/footer_inc');

        // echo $this->email->print_debugger();


}

function validaterecoverpassword($email, $token){
    $email_exists = $this->Mlogin->validaterecoverpassword($email);
    if ($email_exists == 0) {
        $this->session->set_flashdata("error_validate","Token expired, please apply again for a new password");
        redirect('Login/recover');
    }
    else{
        $validate = $this->Mlogin->validaterecoverpasswordtoken($token);
        if($validate == 0){
            redirect('Login/recover', 'refresh');
            $this->session->set_flashdata("error_validate","Token expired, please apply again for a new password"); 

        }else{
         $this->session->set_userdata('reset_email', $email);
         $this->load->view('layout/header_inc');
         $this->load->view('layout/top_header');
         $this->load->view('layout/nav');
         $this->load->view('profile/reset');
         $this->load->view('layout/footer');
         $this->load->view('layout/footer_inc');
     }
 }
}

function change(){

    $password = $this->input->post('password');
    $password_hash = password_hash($password, PASSWORD_BCRYPT);
    $email = $this->session->userdata('reset_email');

    $data = array(
        'user_password' => $password_hash,
    );

    $this->Mlogin->change($email, $data);

    redirect("Login?change=yes");  

}
}