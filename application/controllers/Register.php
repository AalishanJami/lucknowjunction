<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalshan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mregister');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
            $this->load->view('layout/header_inc');
            $this->load->view('layout/top_header');
            $this->load->view('layout/nav');
            $this->load->view('profile/register');
            $this->load->view('layout/footer');
            $this->load->view('layout/footer_inc');
        }

    
        public function add()
        {
            
            // configurations from upload library
            if ($this->input->post('submit')) {
                # code...
                echo "hi";
            $arraypath = explode(".", strtolower($_FILES['userfile']['name']));
        $extension = end($arraypath);
        $image = time().mt_rand(1, 99).'.'.$extension;
        // configurations from upload library
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048000'; // max size in KB
        $config['max_width'] = '20000'; //max resolution width
        $config['max_height'] = '20000';  //max resolution height
        $config['file_name'] = $image;
      
        // load CI libarary called upload
        $this->load->library('upload', $config);
        // body of if clause will be executed when image uploading is failed
        if (!$this->upload->do_upload()) {
            $errors = array('error' => $this->upload->display_errors());
            // This image is uploaded by deafult if the selected image in not uploaded
            $image = 'no_image.png';
        } // body of else clause will be executed when image uploading is succeeded
        else {
            $data = array('upload_data' => $this->upload->data());
        }
    
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $password_hash = password_hash($password, PASSWORD_BCRYPT);
            $phone = $this->input->post('phone');
            $fb = $this->input->post('fb');
            $insta = $this->input->post('insta');
           
                $data = array(
                    'user_name' => $name,
                    'user_email' => $email,
                    'user_password' => $password_hash,
                    'user_contact' => $phone,
                    'user_fb' => $fb,     
                    'user_insta' => $insta,
                    'user_img' => $image,
                    'is_Active' => "1"
                );
            
            $this->Mregister->insert($data);
            
            redirect("Login");
        }
    }
}