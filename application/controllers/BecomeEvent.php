<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class BecomeEvent extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mbecome');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->database(); // load database
    }

    public function index()
    {
         if(empty($this->session->userdata('id'))){
            redirect('Login');
        }
        $data['country']=$this->Mbecome->getCountry();
        $data['city']=$this->Mbecome->getCity();
        $data['state']=$this->Mbecome->getState();
        $data['category']=$this->Mbecome->getCategory();
        $this->load->view('layout/header_inc');
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('event/becomeevent', $data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');
    }

    public function add()
    {
         if(empty($this->session->userdata('id'))){
            redirect('Login');
        }
        $arraypath = explode(".", strtolower($_FILES['userfile']['name']));
        $extension = end($arraypath);
        $image = time().mt_rand(1, 99).'.'.$extension;
        // configurations from upload library
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048000'; // max size in KB
        $config['max_width'] = '20000'; //max resolution width
        $config['max_height'] = '20000';  //max resolution height
        $config['file_name'] = $image;
      
        // load CI libarary called upload
        $this->load->library('upload', $config);
        // body of if clause will be executed when image uploading is failed
        if (!$this->upload->do_upload()) {
            $errors = array('error' => $this->upload->display_errors());
            // This image is uploaded by deafult if the selected image in not uploaded
            $image = 'no_image.png';
        } // body of else clause will be executed when image uploading is succeeded
        else {
            $data = array('upload_data' => $this->upload->data());
        }

        $name = $this->input->post('name');
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $category = $this->input->post('category');
        $chief_guest = $this->input->post('chief_guest');
        $country = $this->input->post('country');
        $description = $this->input->post('description');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $price = $this->input->post('price');
        $type = $this->input->post('type');
        $user =  $this->session->userdata('id');

        if($type == "free"){
            $is_pricing = "0";
            $price = "0";
        }
        else{
            $is_pricing = "1";
        }
       
            $data = array(
                'event_name' => $name,
                'event_date' => $date,
                'event_time' => $time,
                'category_id' => $category,
                'event_chiefGuest' => $chief_guest,     
                'country_id' => $country,
                'event_description' => $description,
                'event_status' => "1",
                'event_photo' => $image,
                'city_id' => $city,
                'state_id' => $state,
                'organizer_id' => $user,
                'event_price' => $price,
                'is_pricing' => $is_pricing
            );
        $id = $this->Mbecome->insert($data);
        if($type == "free"){
            $this->session->set_flashdata('success', 'Event Created');
            redirect('BecomeEvent');
        }
        else{
            $this->addPrices($id);
        }
        
       
        // redirect("MBecomeEvent");
    }

    public function addPrices($id){
         if(empty($this->session->userdata('id'))){
            redirect('Login');
        }
        $data['event']=$this->Mbecome->getEvent($id);
    
        $this->load->view('layout/header_inc');
        $this->load->view('layout/top_header');
        $this->load->view('layout/nav');
        $this->load->view('event/becomeeventprice', $data);
        $this->load->view('layout/footer');
        $this->load->view('layout/footer_inc');
    }

    public function addClasses(){
         if(empty($this->session->userdata('id'))){
            redirect('Login');
        }
        $data = array();
        $total = $this->input->post('total');
        $eventid = $this->input->post('event_id');

        for($i=0; $i < $total; $i++){
            $name = $this->input->post('name['.$i.']');
            $price = $this->input->post('price['.$i.']');
            $data = array(
                'class_name'=> $name,
                'class_price' => $price,
                'event_id' => $eventid
            );
            $this->db->insert('eventspriceclasses',$data);
        }

         $initprice = $this->input->post('price[0]');
         $datas =array(
            'base_price' => $initprice
         );
         $this->Mbecome->udpatePrice($eventid, $datas);
        $this->session->set_flashdata('success', 'Event Created');
       redirect('BecomeEvent');
    }


    }