<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Aalishan
 * Date: 7/12/2019
 * Time: 8:59 PM
 */
class RegisterC extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('RegisterM');
        $this->load->library('form_validation');
        $this->load->library('session');

        $this->load->database(); // load database
    }

    public function index()
    {
            $this->load->view('Auth/register');
    }

    public function add()
    {
        // configurations from upload library
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048000'; // max size in KB
        $config['max_width'] = '20000'; //max resolution width
        $config['max_height'] = '20000';  //max resolution height
        // load CI libarary called upload
        $this->load->library('upload', $config);
        // body of if clause will be executed when image uploading is failed
        if (!$this->upload->do_upload()) {
            $errors = array('error' => $this->upload->display_errors());
            // This image is uploaded by deafult if the selected image in not uploaded
            $image = 'no_image.png';
        } // body of else clause will be executed when image uploading is succeeded
        else {
            $data = array('upload_data' => $this->upload->data());
            $image = $_FILES['userfile']['name'];  //name must be userfile
        }

        $name = $this->input->post('inputName');
        $email = $this->input->post('inputEmail');
        $password = $this->input->post('inputPassword');
        $password_hash = password_hash($password, PASSWORD_BCRYPT);
        $phone = $this->input->post('inputPhone');
        $address = $this->input->post('inputAddress');

        if (null!== $this->input->post('saloon')){
            
            $data = array(
                'name' => $name,
                'email' => $email,
                'password' => $password_hash,
                'address' => $address,
                'phone_no' => $phone,     
                'is_admin' => true,
                'img' => $image
            );
        }
        else {
            $data = array(
                'name' => $name,
                'email' => $email,
                'password' => $password_hash,
                'address' => $address,
                'phone_no' => $phone,    
                'is_admin' => false,
                'img' => $image
            );
        }

        // echo $data;
       
        //    echo $name, $email, $password, $address, $phone, $city;
        $this->MRegister->insert($data);
        //  $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        // $this->session->set_userdata('url', $actual_link);
        redirect("index.php/LoginC");
    }


}

    