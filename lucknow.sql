-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2019 at 07:47 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lucknow`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `about_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `site_url` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `site_url`, `username`, `password`) VALUES
(1, 'Admin', 'www.intellirobs.com/admin', 'admin', 'password@00');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `ad_id` int(11) NOT NULL,
  `ad_name` varchar(255) DEFAULT NULL,
  `Ad_isActive` tinyint(1) DEFAULT NULL,
  `ad_img` varchar(255) DEFAULT NULL,
  `ad_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`ad_id`, `ad_name`, `Ad_isActive`, `ad_img`, `ad_type`) VALUES
(3, 'One Adv', 1, '157492458189.jpg', '2');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_status` varchar(255) DEFAULT NULL,
  `category_img` varchar(255) DEFAULT NULL,
  `category_desc` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_status`, `category_img`, `category_desc`) VALUES
(11, 'Business', '1', 'business.jpg', 'Networking is the key to a successful business. Attend business events like tech fairs, trade shows, sales & marketing summit, start-up or entrepreneurship conference, will help you grow your business network. An event is also an opportunity. Explore business and networking events near you  to find an opportunity for the development of your business.'),
(12, 'Concerts', '1', 'concerts.jpg', 'Music to ears is like a soul to life. Escape the monotonous life and find pieces of you by attending live music events, festivals and concerts. From classical music to jazz to pop to heavy metal to EDM to rock concerts, it is a much complex classification lineup, yet a religion that unites all. Love to groove on the thrilling number of your favorite artists?'),
(13, 'Exhibitions', '1', 'exibitions.jpg', 'Trade shows, expos, tech fairs are known to have benefitted traders with their product launches and improving their brand’s image. There is a lot to learn from such expos in from an attendee\'s perspective. One can make a lot of new connections within the industry itself and pitch about their ideas or products through such happening exhibitions. Look out for all the relevant trade fairs and exhibitions near you.'),
(14, 'Foods & Drinks', '1', 'foods.jpg', 'If you are a food stylist, food blogger, gourmet chef, caffeine junkie or just a keen foodie, you definitely are more worried about your salad being finely chopped than your appearance. To keep up with your food cravings there are these street food fairs, wine tasting events and food festivals. For all the foodies who are avid learners; there are cooking workshops, baking workshops, cake making classes and a lot more. You can also explore various food truck festivals happening.'),
(15, 'Meetups', '1', 'meetups.jpg', 'This informal meetings called meetups are organized all round the year. Meetups are organized for distinctive genres and suiting varied interests of the event goers. There are singles meetup for all the single men and ladies seeking for connection. There are entrepreneurs meetup, badminton meetup, startup meetups, coffee meetups and many more meetups that are lined up for you. You can meet people that share the same interest as you do, people that belong to your local community through the meetup groups.Life can never be boring if you learn to party hard. Don\'t just wait for weekends, celebrate every accomplishment or even your existence. Check out the Peshawar nightlife scene with pool parties, discotheques, DJ Nights, pub crawls, trance festivals and many more. You will be surprised to find many happy hours booze and best DJs playing your favorite EDM at weekend party venues near you.'),
(16, 'Parties', '1', 'parties.jpg', 'Life can never be boring if you learn to party hard. Don\'t just wait for weekends, celebrate every accomplishment or even your existence. Check out the nightlife scene with pool parties, discotheques, DJ Nights, pub crawls, trance festivals and many more. You will be surprised to find many happy hours booze and best DJs playing your favorite EDM at weekend party venues near you.'),
(17, 'Sports', '1', 'sports.jpg', 'Sports is about bettering ourselves. It\'s more about self-improvement, beating one\'s own record every day every moment. It helps us to gain the sportsmanship attitude not just towards game but towards life. While every sport follows different rules, participating in sports activities will help your brain take decisions faster. Participate in the sports events like a football tournament, cricket, basketball, rugby/soccer, etc to helps to build your stamina.\r\n\r\nExercising and playing sports increase the flow of oxygen in the body. Get up, start running or join the next marathon, triathlon or cycling events.'),
(18, 'Health & Wellness', '1', 'health.jpg', 'If there is anything that will stay with you until the end of your life, it is your body. Treat your body like a temple to your soul and it will never leave you till the last breath. Hit a gym or enroll for yoga classes. Perform strength and flexibility exercises. Heal your soul by learning the art of breathing techniques like pranayama or gong. Physical and Mental Health are interrelated and what could be better than getting to learn self-defense techniques, meditation, pilates, yoga'),
(22, 'New Year', '1', '157658161484.jpg', 'New Year in cities are celebrated with fun, here are the events being held for the purpose');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `city_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_status`) VALUES
(8, 'Lucknow', '1'),
(9, 'Allahabad', '1'),
(10, 'Kanpur', '1');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_name`, `status`, `country_id`) VALUES
('India', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eventbookinglist`
--

CREATE TABLE `eventbookinglist` (
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `event_bookinglistId` int(11) NOT NULL,
  `booking_status` varchar(255) DEFAULT NULL,
  `trxid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eventbookinglist`
--

INSERT INTO `eventbookinglist` (`user_id`, `event_id`, `event_bookinglistId`, `booking_status`, `trxid`) VALUES
(1, 1, 1, '1', '3d546d1721ced08672ae'),
(1, 1, 4, '1', '1cd464e0de146f6b2f08'),
(12, 1, 5, '1', '3ea43a4414db4c97d13f'),
(12, 1, 6, '1', '3ea43a4414db4c97d13f'),
(12, 1, 7, '1', '3ea43a4414db4c97d13f');

-- --------------------------------------------------------

--
-- Table structure for table `eventratings`
--

CREATE TABLE `eventratings` (
  `eventrating_id` int(11) NOT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eventratings`
--

INSERT INTO `eventratings` (`eventrating_id`, `rating`, `user_id`, `event_id`) VALUES
(1, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `eventreviews`
--

CREATE TABLE `eventreviews` (
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `event_review` varchar(255) DEFAULT NULL,
  `review_id` int(11) NOT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `review_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eventreviews`
--

INSERT INTO `eventreviews` (`user_id`, `event_id`, `event_review`, `review_id`, `isActive`, `review_date`) VALUES
(1, 1, 'Good work', 1, 1, '2019-11-21 17:03:33'),
(1, 1, 'Keep it up', 2, 1, '2019-11-21 17:04:04'),
(1, 1, 'love to be part of this', 3, 1, '2019-11-21 17:04:28'),
(1, 1, 'hope to see you soon', 4, 1, '2019-11-21 17:05:59'),
(1, 1, 'we are in love', 5, 1, '2019-11-21 17:07:34'),
(1, 1, 'can\'t wait', 6, 1, '2019-11-21 17:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `event_chiefGuest` varchar(255) DEFAULT NULL,
  `event_date` varchar(255) DEFAULT NULL,
  `event_time` varchar(255) DEFAULT NULL,
  `event_price` varchar(255) DEFAULT NULL,
  `event_status` varchar(255) DEFAULT NULL,
  `organizer_id` int(11) DEFAULT NULL,
  `rejection_message` varchar(255) DEFAULT NULL,
  `event_photo` varchar(255) DEFAULT NULL,
  `event_description` longtext DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `country_id` varchar(255) DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `is_pricing` varchar(255) DEFAULT NULL,
  `is_sold` varchar(255) NOT NULL DEFAULT '0',
  `base_price` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `event_name`, `event_chiefGuest`, `event_date`, `event_time`, `event_price`, `event_status`, `organizer_id`, `rejection_message`, `event_photo`, `event_description`, `category_id`, `country_id`, `city_id`, `state_id`, `is_pricing`, `is_sold`, `base_price`) VALUES
(1, 'Session on \"Success & The Meaning of Life\"', 'Chairman of the University', '2019-01-01', '02:01', '3', '1', 1, NULL, '6fece96e394fb6cbf8ee9af5d65c34c861d1d893897518fb3f4d3db8eb53f6dc-rimg-w526-h295-gmir.jpg', ' Session on \"Success & The Meaning of Life\"\r\nWe invite our students to attend the grand session on “Success and the Meaning of life” delivered by our honorable guest Mr Salman Asif Siddiqui.\r\n\r\nSalman Asif Siddiqui is the founder and director of Educational Resource Development Centre (ERDC) – an institute of training and material development in Pakistan. Salman is an advocate of natural learning. Salman has received his education in Pakistan and specialized in Educational Psychology. He has travelled widely and studied the strengths and limitations of some of the best education systems of the world. His academic pursuits spread across psychology, education, sociology, religion, philosophy, literature, and science. During his extensive experience of over 24 years in the field of education, he has served the roles of a teacher, trainer, counselor, author and editor. Salman Asif Siddiqui has over forty children’s books and teachers’ courses to his credit as author or editor. His books for children are taught in schools in Pakistan and abroad. He has conducted hundreds of training workshops for teachers, trainers, school heads, parents and students all over the country in association with several public and private institutions. He has spoken to educational forums in Pakistan, Europe and Middle East and worked for a number of leading national and international organizations.', '11', '1', '8', '5', '1', '0', '1000'),
(2, 'One Day Training (Monitoring & Evaluation)', 'CCV', '2019-02-08', '14:00', '0', '1', 1, NULL, 'ecf5a9a714478822b25bcbdcce2a78c733b8a074c9378df04847000f10c94032-rimg-w504-h315-gmir.jpg', ' One Day Training (Monitoring & Evaluation)\r\nOn Day Training on Monitoring & Evaluation (Social & Hard Components of Project)\r\n\r\n2 to 5 PM\r\nSaturday\r\n23-11-19\r\n\r\nRegistration Charges Rs. 3500/-\r\nTrainer Profile\r\n19+ Years of Experience\r\nAn analytical, enthusiastic professional enjoyed the glory of success & failures, rewards & punishments,\r\nachievements & surrenders, with sound track record, well groomed and for communities development through best use of learned & practiced techniques in organizing\r\nand mobilizing as well as in coordination in line with the set objectives of the projects successfully by\r\nproducing excellent results in stipulated time frame.\r\nWorking experience with Communities, NGOs, INGOs Local Government, Town Municipal Administration, Stakeholders Governments Agencies / Depts.\r\nself motivated & having the mission to serve the needy.\r\nProfessionally trained on using developer side of social media tools, digital marketing and research based integrations.\r\n', '12', '1', '8', '5', '0', '0', NULL),
(3, 'Doomsday', 'CSV', '2019-09-23', '16:00', '2', '1', 1, NULL, 'f2c6b913113628201cb14e712d58ab846c97ecd6e4fd1d3f47dd23758fd5fc4b-rimg-w526-h294-gmir.jpg', ' Doomsday\r\nNUCES Pwr. ACM Student Chapter is planning to make your weekend #lit by not just #gaming but other thrilling stuff as well. Yes, you\'ve got it right – Doomsday is gonna be ‘LIT’.\r\n\r\nThis event will serve as a refresher for all those students who are bored of tiresome and tedious study routine and want some entertainment.\r\n\r\nThe exciting part is that the event is not only for #gamers but #nongamers will get the chance to have the time of their lives too.\r\n\r\nThe event will consist of 4 categories consisting of #Hardcore #E_gaming, #Indoor-#Gaming, #Kids #Modules and #Female #modules.\r\n\r\nThe winning teams/players of each category will be awarded with cash prizes and Winning Shields.\r\n\r\nOutsiders are allowed. The entry fee for the non-gamers is Rs.200 and for gamers who have participated will have free entry.\r\n\r\nThe details of the modules are given.\r\n1. Hardcore E-Gaming\r\nIn the 1st module, there will be Hardcore E-gaming. There is a total of seven games in hardcore e-gaming module.\r\n\r\n2. Indoor-Gaming\r\nIn Indoor Gaming we have 3 modules which consist of Ludo, Cards and Chess.\r\n\r\n3. Kids Modules\r\nKiddos should have some fun too, no? So, for them we have organized ‘Little Artist’ and ‘Spelling Bee’.\r\n\r\n4. Female Modules\r\nBy keeping in mind the females we have arranged modules for them separately which include Henna Competition, Singing Competition and Painting Competition.\r\n\r\n\r\n**THE BEST PART**:\r\nThis year our theme is Halloween and what comes to your mind from ‘Halloween’? TRICK OR TREAT OBVIOUSLY!\r\nWe will have a stall set up just for this where you will either go for trick or treat, your call! FUN, RIGHT? See you there!\r\n\r\nEvent Details:\r\nDay1 - 23rd November, 2019 (Saturday)\r\nDay2 - 24th November, 2019 (Sunday)\r\nTimings: 9AM - 7PM\r\n\r\n\r\nThe structure of fee and prize money will be updated on the event page soon.\r\n\r\n', '12', '1', '8', '5', '1', '0', '1000'),
(4, 'GBG Peshawar BIZ FEST 2019', 'CSV', '2109-03-02', '18:00', '0', '1', 1, NULL, 'e5cb25f0-fec1-11e9-93ce-5793d59b27ec-rimg-w819-h280-gmir.jpg', ' GBG Peshawar BIZ FEST 2019\r\n*Drumrolls*\r\n\r\nGBG Peshawar is bringing you the most happening conference in town!\r\nThe annual business conference by GBG Peshawar, the BIZ FEST is happening on the 24th of November, 2019.\r\n\r\nRegister yourselves to learn from the industry experts and get valuable insights for expanding and developing your businesses through Google tools and technology.\r\n\r\nBlock your calendars for the 24th of November, 2019!\r\nTime: 10am - 6pm\r\n\r\nRegister yourselves through the following link:\r\nhttps://forms.gle/X2w7JKKSozew6Rn88', '18', '1', '8', '5', '0', '0', NULL),
(5, 'Food Mob', 'Trump', '2019-02-04', '23:00', '1', '1', 1, NULL, '3c01e7005dca15068e4c54eec054f95d549c6c89c438e36ad5d14c20899498a7-rimg-w526-h275-gmir.jpg', ' Food Mob\r\nHey Peshawar!\r\nGet ready for full day FOOD MOB With our social media partner Peshawar food diaries at Shelton\'s Rezidor.\r\nEnjoy 30% Flat OFF on Entire New Menu.\r\n\r\nTimings: (Lunch/Dinner) 11am till 12am\r\n\r\nFor Pre-Booking:\r\nLandline (091) 5701201-5\r\nWhatsApp (0324) 7394367\r\nF&B Manager (0345) 8595553', '14', '1', '8', '5', '1', '0', '1000'),
(6, 'Parenting Challenges of the 21st Century', 'Trump', '2019-01-01', '15:03', '0', '1', 1, NULL, 'a02eacc133f36558853bbd67af58cc467ee526a9ac85089d8d6bd367169718b0-rimg-w526-h295-gmir.jpg', ' \'Parenting Challenges of the 21st Century\' is a program by Salman Asif Siddiqui (Educational Psychologist, Parent Counselor & Director ERDC), that will guide parents and teachers in preparing their children to dealing with the issues and challenges of the 21st Century.\r\n\r\nEntry to this program is free, but prior registration is mandatory.\r\nRegister now at: https://tinyurl.com/Parenting23Nov\r\n\r\nSCHEDULE:\r\nSaturday, 23rd November, 2019\r\n11:00 AM to 1:00 PM\r\n\r\nINTENDED AUDIENCE:\r\nAll parents, teachers, educationists and interested individuals from all walks of life.\r\n\r\nVENUE:\r\nMubarak Banquet Hall-B,\r\nFakhr-e-Alam Road, Sadar Peshawar.\r\n\r\nPROGRAM PROTOCOL:\r\n- The program is FREE but registration is mandatory for attending.\r\n- Program discusses a perspective for elders, and people above 18 years are allowed only.\r\n- For note-taking, please bring a notebook and a pen, and arrive a few minutes before the program.\r\n\r\nREGISTRATIONS:\r\nPlease register yourself at: https://tinyurl.com/Parenting23Nov\r\n\r\nFor any further details, please contact us at:\r\n0335-2444834\r\n0321-2034483\r\n0334-3856304\r\n\r\n\r\nYou may also like the following events from Educational Resource Development Centre (ERDC):\r\n\r\nNext Sunday, 1st December, 11:00 am, Parents as Counselors | Islamabad in Islamabad\r\nNext month, 4th December, 03:00 pm, 7 Magical Activities to Prepare Children for Reading in Karachi\r\nNext month, 7th December, 09:00 am, ERDC Workshop: The Art of Public Speaking in Karachi', '13', '1', '8', '5', '0', '0', NULL),
(19, 'Session on \"Success & The Meaning of Life\"', 'Chairman of the University', '2019-01-01', '02:01', '3', '1', 1, NULL, '6fece96e394fb6cbf8ee9af5d65c34c861d1d893897518fb3f4d3db8eb53f6dc-rimg-w526-h295-gmir.jpg', ' Session on \"Success & The Meaning of Life\"\r\nWe invite our students to attend the grand session on “Success and the Meaning of life” delivered by our honorable guest Mr Salman Asif Siddiqui.\r\n\r\nSalman Asif Siddiqui is the founder and director of Educational Resource Development Centre (ERDC) – an institute of training and material development in Pakistan. Salman is an advocate of natural learning. Salman has received his education in Pakistan and specialized in Educational Psychology. He has travelled widely and studied the strengths and limitations of some of the best education systems of the world. His academic pursuits spread across psychology, education, sociology, religion, philosophy, literature, and science. During his extensive experience of over 24 years in the field of education, he has served the roles of a teacher, trainer, counselor, author and editor. Salman Asif Siddiqui has over forty children’s books and teachers’ courses to his credit as author or editor. His books for children are taught in schools in Pakistan and abroad. He has conducted hundreds of training workshops for teachers, trainers, school heads, parents and students all over the country in association with several public and private institutions. He has spoken to educational forums in Pakistan, Europe and Middle East and worked for a number of leading national and international organizations.', '11', '1', '8', '5', '1', '0', '1000'),
(20, 'One Day Training (Monitoring & Evaluation)', 'CCV', '2019-02-08', '14:00', '0', '1', 1, NULL, 'ecf5a9a714478822b25bcbdcce2a78c733b8a074c9378df04847000f10c94032-rimg-w504-h315-gmir.jpg', ' One Day Training (Monitoring & Evaluation)\r\nOn Day Training on Monitoring & Evaluation (Social & Hard Components of Project)\r\n\r\n2 to 5 PM\r\nSaturday\r\n23-11-19\r\n\r\nRegistration Charges Rs. 3500/-\r\nTrainer Profile\r\n19+ Years of Experience\r\nAn analytical, enthusiastic professional enjoyed the glory of success & failures, rewards & punishments,\r\nachievements & surrenders, with sound track record, well groomed and for communities development through best use of learned & practiced techniques in organizing\r\nand mobilizing as well as in coordination in line with the set objectives of the projects successfully by\r\nproducing excellent results in stipulated time frame.\r\nWorking experience with Communities, NGOs, INGOs Local Government, Town Municipal Administration, Stakeholders Governments Agencies / Depts.\r\nself motivated & having the mission to serve the needy.\r\nProfessionally trained on using developer side of social media tools, digital marketing and research based integrations.\r\n', '12', '1', '8', '5', '0', '0', NULL),
(21, 'Doomsday', 'CSV', '2019-09-23', '16:00', '2', '1', 1, NULL, 'f2c6b913113628201cb14e712d58ab846c97ecd6e4fd1d3f47dd23758fd5fc4b-rimg-w526-h294-gmir.jpg', ' Doomsday\r\nNUCES Pwr. ACM Student Chapter is planning to make your weekend #lit by not just #gaming but other thrilling stuff as well. Yes, you\'ve got it right – Doomsday is gonna be ‘LIT’.\r\n\r\nThis event will serve as a refresher for all those students who are bored of tiresome and tedious study routine and want some entertainment.\r\n\r\nThe exciting part is that the event is not only for #gamers but #nongamers will get the chance to have the time of their lives too.\r\n\r\nThe event will consist of 4 categories consisting of #Hardcore #E_gaming, #Indoor-#Gaming, #Kids #Modules and #Female #modules.\r\n\r\nThe winning teams/players of each category will be awarded with cash prizes and Winning Shields.\r\n\r\nOutsiders are allowed. The entry fee for the non-gamers is Rs.200 and for gamers who have participated will have free entry.\r\n\r\nThe details of the modules are given.\r\n1. Hardcore E-Gaming\r\nIn the 1st module, there will be Hardcore E-gaming. There is a total of seven games in hardcore e-gaming module.\r\n\r\n2. Indoor-Gaming\r\nIn Indoor Gaming we have 3 modules which consist of Ludo, Cards and Chess.\r\n\r\n3. Kids Modules\r\nKiddos should have some fun too, no? So, for them we have organized ‘Little Artist’ and ‘Spelling Bee’.\r\n\r\n4. Female Modules\r\nBy keeping in mind the females we have arranged modules for them separately which include Henna Competition, Singing Competition and Painting Competition.\r\n\r\n\r\n**THE BEST PART**:\r\nThis year our theme is Halloween and what comes to your mind from ‘Halloween’? TRICK OR TREAT OBVIOUSLY!\r\nWe will have a stall set up just for this where you will either go for trick or treat, your call! FUN, RIGHT? See you there!\r\n\r\nEvent Details:\r\nDay1 - 23rd November, 2019 (Saturday)\r\nDay2 - 24th November, 2019 (Sunday)\r\nTimings: 9AM - 7PM\r\n\r\n\r\nThe structure of fee and prize money will be updated on the event page soon.\r\n\r\n', '12', '1', '8', '5', '1', '0', '1000'),
(22, 'GBG Peshawar BIZ FEST 2019', 'CSV', '2109-03-02', '18:00', '0', '1', 1, NULL, 'e5cb25f0-fec1-11e9-93ce-5793d59b27ec-rimg-w819-h280-gmir.jpg', ' GBG Peshawar BIZ FEST 2019\r\n*Drumrolls*\r\n\r\nGBG Peshawar is bringing you the most happening conference in town!\r\nThe annual business conference by GBG Peshawar, the BIZ FEST is happening on the 24th of November, 2019.\r\n\r\nRegister yourselves to learn from the industry experts and get valuable insights for expanding and developing your businesses through Google tools and technology.\r\n\r\nBlock your calendars for the 24th of November, 2019!\r\nTime: 10am - 6pm\r\n\r\nRegister yourselves through the following link:\r\nhttps://forms.gle/X2w7JKKSozew6Rn88', '18', '1', '8', '5', '0', '0', NULL),
(23, 'Food Mob', 'Trump', '2019-02-04', '23:00', '1', '1', 1, NULL, '3c01e7005dca15068e4c54eec054f95d549c6c89c438e36ad5d14c20899498a7-rimg-w526-h275-gmir.jpg', ' Food Mob\r\nHey Peshawar!\r\nGet ready for full day FOOD MOB With our social media partner Peshawar food diaries at Shelton\'s Rezidor.\r\nEnjoy 30% Flat OFF on Entire New Menu.\r\n\r\nTimings: (Lunch/Dinner) 11am till 12am\r\n\r\nFor Pre-Booking:\r\nLandline (091) 5701201-5\r\nWhatsApp (0324) 7394367\r\nF&B Manager (0345) 8595553', '14', '1', '8', '5', '1', '0', '1000'),
(24, 'Parenting Challenges of the 21st Century', 'Trump', '2019-01-01', '15:03', '0', '1', 1, NULL, 'a02eacc133f36558853bbd67af58cc467ee526a9ac85089d8d6bd367169718b0-rimg-w526-h295-gmir.jpg', ' \'Parenting Challenges of the 21st Century\' is a program by Salman Asif Siddiqui (Educational Psychologist, Parent Counselor & Director ERDC), that will guide parents and teachers in preparing their children to dealing with the issues and challenges of the 21st Century.\r\n\r\nEntry to this program is free, but prior registration is mandatory.\r\nRegister now at: https://tinyurl.com/Parenting23Nov\r\n\r\nSCHEDULE:\r\nSaturday, 23rd November, 2019\r\n11:00 AM to 1:00 PM\r\n\r\nINTENDED AUDIENCE:\r\nAll parents, teachers, educationists and interested individuals from all walks of life.\r\n\r\nVENUE:\r\nMubarak Banquet Hall-B,\r\nFakhr-e-Alam Road, Sadar Peshawar.\r\n\r\nPROGRAM PROTOCOL:\r\n- The program is FREE but registration is mandatory for attending.\r\n- Program discusses a perspective for elders, and people above 18 years are allowed only.\r\n- For note-taking, please bring a notebook and a pen, and arrive a few minutes before the program.\r\n\r\nREGISTRATIONS:\r\nPlease register yourself at: https://tinyurl.com/Parenting23Nov\r\n\r\nFor any further details, please contact us at:\r\n0335-2444834\r\n0321-2034483\r\n0334-3856304\r\n\r\n\r\nYou may also like the following events from Educational Resource Development Centre (ERDC):\r\n\r\nNext Sunday, 1st December, 11:00 am, Parents as Counselors | Islamabad in Islamabad\r\nNext month, 4th December, 03:00 pm, 7 Magical Activities to Prepare Children for Reading in Karachi\r\nNext month, 7th December, 09:00 am, ERDC Workshop: The Art of Public Speaking in Karachi', '13', '1', '8', '5', '0', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eventspriceclasses`
--

CREATE TABLE `eventspriceclasses` (
  `eventclass_id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `class_price` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eventspriceclasses`
--

INSERT INTO `eventspriceclasses` (`eventclass_id`, `event_id`, `class_name`, `class_price`) VALUES
(1, 1, 'Basic', '1000'),
(2, 1, 'Standard', '2000'),
(3, 1, 'Premium', '3000'),
(4, 3, 'Standard', '1000'),
(5, 3, 'Premium', '2000'),
(6, 5, 'Standard', '1000'),
(7, 16, 'Standard', '1000'),
(8, 17, 'st', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `feedback_msg` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_replies`
--

CREATE TABLE `feedback_replies` (
  `feedback_replies_id` int(11) NOT NULL,
  `replies_content` varchar(255) DEFAULT NULL,
  `replies_feedbackid` int(11) DEFAULT NULL,
  `replies_userid` int(11) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forgotpassword`
--

CREATE TABLE `forgotpassword` (
  `id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forgotpassword`
--

INSERT INTO `forgotpassword` (`id`, `token`, `email`, `created_at`) VALUES
(1, '23071728710598829', 'aalishanj1@gmail.com', '2019-11-23 09:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `search_data`
--

CREATE TABLE `search_data` (
  `id` int(11) NOT NULL,
  `query` varchar(255) DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `service_img` varchar(255) DEFAULT NULL,
  `service_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `state_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_name`, `state_status`) VALUES
(5, 'Uttar Pradesh', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_contact` varchar(255) DEFAULT NULL,
  `user_img` varchar(255) DEFAULT NULL,
  `banner_img` varchar(255) DEFAULT NULL,
  `user_fb` varchar(255) DEFAULT NULL,
  `user_insta` varchar(255) DEFAULT NULL,
  `site_url` varchar(255) DEFAULT NULL,
  `admin_mail` varchar(255) DEFAULT NULL,
  `is_Active` tinyint(1) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_email`, `user_password`, `user_contact`, `user_img`, `banner_img`, `user_fb`, `user_insta`, `site_url`, `admin_mail`, `is_Active`, `user_name`) VALUES
(1, 'user@gmail.com', '$2y$10$BV2c3qpYtSd/oRJN2e80E.UC5oOCE9rxIumwUUv8RyG9.cHwUuqaW', '03348229276', 'download.jpg', NULL, 'www.facebook.com/aali.jami', 'instagram.com/aalishanjami', NULL, NULL, 1, 'Aalishan Jami'),
(12, 'aalishanj1@gmail.com', '$2y$10$1UmpNq8p42sa8bi0L974new.pnyDx4L7MKcEdNPLMZbKGltfI8wvm', '03348229276', '157536446691.png', NULL, '', '', NULL, NULL, 1, 'Aalishan Jami'),
(13, 'testtest@gmail.com', '$2y$10$QPWWy1kqHesth/wmNKVXv.Nd9Z4bx8ULe9Cmm91RpfHmibul.An.O', '03348229276', '157536452432.png', NULL, '', '', NULL, NULL, 1, 'Aalishan Jami');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`about_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `eventbookinglist`
--
ALTER TABLE `eventbookinglist`
  ADD PRIMARY KEY (`event_bookinglistId`);

--
-- Indexes for table `eventratings`
--
ALTER TABLE `eventratings`
  ADD PRIMARY KEY (`eventrating_id`);

--
-- Indexes for table `eventreviews`
--
ALTER TABLE `eventreviews`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `eventspriceclasses`
--
ALTER TABLE `eventspriceclasses`
  ADD PRIMARY KEY (`eventclass_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `feedback_replies`
--
ALTER TABLE `feedback_replies`
  ADD PRIMARY KEY (`feedback_replies_id`);

--
-- Indexes for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search_data`
--
ALTER TABLE `search_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `about_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `eventbookinglist`
--
ALTER TABLE `eventbookinglist`
  MODIFY `event_bookinglistId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `eventratings`
--
ALTER TABLE `eventratings`
  MODIFY `eventrating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `eventreviews`
--
ALTER TABLE `eventreviews`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `eventspriceclasses`
--
ALTER TABLE `eventspriceclasses`
  MODIFY `eventclass_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_replies`
--
ALTER TABLE `feedback_replies`
  MODIFY `feedback_replies_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `search_data`
--
ALTER TABLE `search_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
